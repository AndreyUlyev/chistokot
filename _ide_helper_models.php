<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Article
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article orderBy(string $string)
 * @mixin \Eloquent
 * @property string $content
 * @property string $image
 * @property string|null $image_caption
 * @property int $category_id
 * @property-read \App\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereImageCaption($value)
 * @property int $is_promotion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereIsPromotion($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProductsArticle[] $productsArticles
 * @property-read int|null $products_articles_count
 */
	class Article extends \Eloquent {}
}

namespace App{
/**
 * App\Basket
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand where($value, $value)
 * @mixin \Eloquent
 * @property int $product_id
 * @property int $quantity
 * @property int $user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereUserId($value)
 */
	class Basket extends \Eloquent {}
}

namespace App{
/**
 * App\Brand
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand orderBy(string $string)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @property-read int|null $products_count
 */
	class Brand extends \Eloquent {}
}

namespace App{
/**
 * App\Category
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category update($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category orderBy($value)
 * @mixin \Eloquent
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category wherePosition($value)
 * @property string|null $description
 * @property int $is_on_main
 * @property string $slug
 * @property string $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIsOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 */
	class Category extends \Eloquent {}
}

namespace App{
/**
 * App\Comment
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $title
 * @property string|null $name
 * @property int $rating
 * @property string $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
 * @mixin \Eloquent
 * @property int $product_id
 * @property int $approved
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereProductId($value)
 */
	class Comment extends \Eloquent {}
}

namespace App{
/**
 * App\DeliveryType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class DeliveryType extends \Eloquent {}
}

namespace App{
/**
 * App\Like
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $user_ip
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereUserIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like create($value)
 * @mixin \Eloquent
 * @property int $product_id
 * @property-read \App\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereProductId($value)
 */
	class Like extends \Eloquent {}
}

namespace App{
/**
 * App\Order
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category update($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category orderBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category where($value, $value)
 * @mixin \Eloquent
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category wherePosition($value)
 * @property string $phone
 * @property string $address
 * @property int $delivery_type_id
 * @property int $user_id
 * @property int|null $promocode_id
 * @property int $payment_type_id
 * @property string|null $completion_date
 * @property-read \App\PaymentType $payment_type
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCompletionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDeliveryTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePaymentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePromocodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 * @property-read \App\DeliveryType $delivery_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @property-read int|null $products_count
 */
	class Order extends \Eloquent {}
}

namespace App{
/**
 * App\OrdersProducts
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts query()
 * @mixin \Eloquent
 */
	class OrdersProducts extends \Eloquent {}
}

namespace App{
/**
 * App\PaymentType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentType orderBy(string $string)
 * @mixin \Eloquent
 */
	class PaymentType extends \Eloquent {}
}

namespace App{
/**
 * App\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Permission extends \Eloquent {}
}

namespace App{
/**
 * App\Product
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property float $price
 * @property float|null $price_discount
 * @property int $quantity
 * @property int $category_id
 * @property int $brand_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePriceDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product where($value, $value)
 * @method static paginate(int $productsInPage)
 * @mixin \Eloquent
 * @property int $is_popular
 * @property string $slug
 * @property string|null $composition
 * @property string|null $about_delivery
 * @property string|null $guarantees
 * @property string|null $where_apply
 * @property string|null $maintenance
 * @property string|null $country
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProductsImage[] $productImages
 * @property-read int|null $product_images_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereAboutDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereGuarantees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereIsPopular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMaintenance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereWhereApply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product filterOrderBy($order)
 */
	class Product extends \Eloquent {}
}

namespace App{
/**
 * App\ProductsArticle
 *
 * @property int $id
 * @property int $article_id
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class ProductsArticles extends \Eloquent {}
}

namespace App{
/**
 * App\ProductsImage
 *
 * @property int $id
 * @property string $link
 * @property int $id_product
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage create($value)
 * @mixin \Eloquent
 * @property int $product_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereProductId($value)
 */
	class ProductsImages extends \Eloquent {}
}

namespace App{
/**
 * App\Promocode
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode get()
 * @mixin \Eloquent
 * @property float $discount
 * @property int|null $category_id
 * @property int|null $product_id
 * @property int|null $brand_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereProductId($value)
 */
	class Promocode extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\Slider
 *
 * @property int $id
 * @property string|null $description
 * @property string|null $link
 * @property string $image
 * @property int|null $sorting_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereSortingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slider create($value)
 * @mixin \Eloquent
 */
	class Slider extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User orWherePermissionIs($permission = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User orWhereRoleIs($role = '', $team = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePermissionIs($permission = '', $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleIs($role = '', $team = null, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User paginate($value)
 * @mixin \Eloquent
 * @property string $second_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSecondName($value)
 */
	class User extends \Eloquent {}
}


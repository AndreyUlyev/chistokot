<?php

namespace App\Console\Commands;

use App\Brand;
use App\Category;
use App\Product;
use Dobro\MoySkladApi\Facades\CustomEntity;
use Dobro\MoySkladApi\Facades\Product as SkladProduct;
use Dobro\MoySkladApi\Facades\ProductFolder;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MergeMoySklad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:load_from_moy_sklad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load brands, products and categories from moy sklad.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->mergeCategories();
        $this->mergeBrands();
        $this->mergeProducts();
    }

    /**
     * Связывание категорий
     *
     * @throws \Exception
     */
    private function mergeCategories()
    {
        $merge_categories = [];
        foreach (ProductFolder::get() as $category) {
            $id = Category::updateOrCreate([
                "moy_sklad_id" => $category->id
            ], [
                "name" => $category->name,
                "description" => $category->description,
                "slug" => Str::slug($category->name)
            ])->id;
            $merge_categories[] = $id;
        }

        Category::whereNotIn("id", $merge_categories)->delete();
    }

    /**
     * Выгрузка брендов с моего склада
     *
     * @throws \Exception
     */
    private function mergeBrands()
    {
        $merge_brands = [];
        $brands_sklad = CustomEntity::getEntities(config("moy_sklad.brand.customEntityMetaId"));
        foreach ($brands_sklad as $brand) {
            $id = Brand::updateOrCreate([
                "moy_sklad_id" => $brand->moy_sklad_id
            ], [
                "name" => $brand->name
            ])->id;
            $merge_brands[] = $id;
        }
        Brand::whereNotIn("id", $merge_brands)->delete();
    }

    /**
     * Выгрузка продуктов с моего склада
     *
     * @throws \Exception
     */
    private function mergeProducts()
    {
        $merge_products = [];
        foreach (SkladProduct::get() as $product) {
            $links = explode("/", $product->productFolder->meta->href);
            $category_sklad_id = end($links);

            $links = explode("/", $product->attributes[0]->value->meta->href);
            $brand_sklad_id = end($links);

            $id = Product::updateOrCreate([
                "moy_sklad_id" => $product->id
            ], array_merge([
                "name" => $product->name,
                "description" => $product->description,
                "price" => $product->salePrices[0]->value / 100,
                "price_discount" => $product->salePrices[1]->value / 100 ?: null,
                "slug" => Str::slug($product->name),
                "category_id" => Category::select("id")->where("moy_sklad_id", $category_sklad_id)->first()->id,
                "brand_id" => Brand::select("id")->where("moy_sklad_id", $brand_sklad_id)->first()->id,
            ], $this->getAttributesSkladProduct($product->attributes)))->id;
            $merge_products[] = $id;
        }
        Product::whereNotIn("id", $merge_products)->delete();
    }

    /**
     * Связывание атрубутов сайта и МойСклад
     *
     * @param $attributes
     * @return array
     */
    private function getAttributesSkladProduct($attributes)
    {
        $data = [
            "quantity" => 1,
            "composition" => null,
            "about_delivery" => null,
            "guarantees" => null,
            "where_apply" => null,
            "maintenance" => null,
            "meta_description" => null,
            "meta_title" => null,
            "meta_keywords" => null
        ];

        foreach ($attributes as $attribute) {
            switch ($attribute->meta->href) {
                case config("moy_sklad.quantity.meta.href"):
                    $data["quantity"] = $attribute->value;
                    break;
                case config("moy_sklad.composition.meta.href"):
                    $data["composition"] = $attribute->value;
                    break;
                case config("moy_sklad.about_delivery.meta.href"):
                    $data["about_delivery"] = $attribute->value;
                    break;
                case config("moy_sklad.guarantees.meta.href"):
                    $data["guarantees"] = $attribute->value;
                    break;
                case config("moy_sklad.where_apply.meta.href"):
                    $data["where_apply"] = $attribute->value;
                    break;
                case config("moy_sklad.maintenance.meta.href"):
                    $data["maintenance"] = $attribute->value;
                    break;
                case config("moy_sklad.descrition.meta.href"):
                    $data["meta_description"] = $attribute->value;
                    break;
                case config("moy_sklad.title.meta.href"):
                    $data["meta_title"] = $attribute->value;
                    break;
                case config("moy_sklad.keywords.meta.href"):
                    $data["meta_keywords"] = $attribute->value;
                    break;
            }
        }
        return $data;
    }
}

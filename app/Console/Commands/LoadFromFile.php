<?php

namespace App\Console\Commands;

use App\Category;
use App\Product;
use App\ProductsImage;
use http\Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

use Dobro\MoySkladApi\Facades\Product as ProductSklad;
use SebastianBergmann\CodeCoverage\Report\PHP;


class LoadFromFile extends Command
{
    private $Counter = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:load_from_file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
//        Category::whereNotNull('id')->delete();
//        \File::deleteDirectory('public/uploads/standard');
//        \File::deleteDirectory('public/uploads/mini');
//        \File::makeDirectory('public/uploads/standard');
//        \File::makeDirectory('public/uploads/mini');

        for ($i=0; $i<10; $i++) {
            Product::orderBy("id", "desc")->first()->delete();
        }


        $categories = json_decode(file_get_contents("public/base2.json"), true)["categories"];
        foreach ($categories as $category) {
            $category_name = strip_tags($category["name"]);
            $category_image = $this->getImageByLink($category["image"]);

            $category_s = Category::firstOrCreate([
                'name' => $category_name
            ], [
                'description' => 'Описание категории',
                'slug' => Str::slug($category_name, "_"),
                'image' => $this->loadImages($category_image, 900, null),
                'moy_sklad_id' => null
            ]);

            $this->parseProducts($category["products"], $category_s->id);
            echo "Категория - " . $category_name . PHP_EOL;
        }
    }

    /**
     * Интеграция продуктов на сайт
     *
     * @param $products
     * @param $category_id
     * @param bool $parent_product
     * @throws \Exception
     */
    private function parseProducts($products, $category_id, $parent_product = false)
    {
        foreach ($products as $product) {
            $color = $product["color"] ?? ($parent_product["color"] ?? null);
            $size = $product["size"] ?? ($parent_product["size"] ?? null);
            if ($size == false) {
                $size = null;
            }

            $product_s = Product::firstOrCreate([
                'name' => strip_tags($product["name"]),
                'color' => $color,
                'size' => $size,
            ], [
                'description' => strip_tags($product["description"] ?? $parent_product["description"]),
                'price' => $product["price"],
                'quantity' => '1',
                'category_id' => $category_id,
                'brand_id' => 1,
                'slug' => Str::slug($product["name"], "_"),
                'slug_size' => $size != null ? Str::slug($size, "_") : null,
                'slug_color' => $color != null ? Str::slug($color, "_") : null,
                'composition' => $product["composition"] ?? ($parent_product["composition"] ?? false),
                'about_delivery' => $product["about_delivery"] ?? ($parent_product["composition"] ?? false),
                'guarantees' => $product["guarantees"] ?? ($parent_product["composition"] ?? false),
                'where_apply' => $product["where_apply"] ?? ($parent_product["composition"] ?? false),
                'maintenance' => $product["maintenance"] ?? ($parent_product["composition"] ?? false),
                'moy_sklad_id' => null,
                'country' => 'ru',
                'meta_title' => $product["meta_title"] ?? ($parent_product["meta_title"] ?? false),
                'meta_keywords' => $product["meta_keywords"] ?? ($parent_product["meta_keywords"] ?? false),
                'meta_description' => $product["meta_description"] ?? ($parent_product["meta_description"] ?? false),
                'product_id' => $parent_product->id ?? null
            ]);
            $product_s["images"] = $product["images"] ?? $parent_product["images"];

            if ($product_s->wasRecentlyCreated == true) {
                foreach (($product["images"] ?? $parent_product["images"]) as $link) {
                    $image = $this->getImageByLink($link);
                    ProductsImage::create([
                        'link' => $this->loadImages($image, 900, null),
                        'product_id' => $product_s->id
                    ]);
                }
                echo ($this->Counter++) . ". Продукт добавлен: " . strip_tags($product["name"]) . PHP_EOL;
            } else {
                echo ($this->Counter++) . ". Продукт обновлен: " . strip_tags($product["name"]) . PHP_EOL;
            }

            if (isset($product["mods"]) && count($product["mods"])) {
                foreach ($product["mods"] as $mod) {
                    if (isset($mod["colors"]) && count($mod["colors"])) {
                        foreach ($mod["colors"] as $color) {
                            $this->parseProducts([[
                                "name" => $mod["name"],
                                "color" => $color["color"],
                                "price" => $color["price"]
                            ]], $category_id, $product_s);
                        }
                    } else {
                        $this->parseProducts($mod, $category_id, $product_s);
                    }
                }
            }
        }
    }

    /**
     * Получение изображение по ссылке
     *
     * @param $link
     * @return false|resource|null
     * @throws \Exception
     */
    private function getImageByLink($link)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $link);
            try {
                $image = imagecreatefromstring($response->getBody()->getContents());
                return $image;
            } catch (\Exception $e) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Загрузка изображений
     *
     * @param $image
     * @param $width - ширина товара
     * @param $height - высота товара
     * @return array
     */
    private function loadImages($image, $width, $height)
    {
        $image_path = $this->createDirImage();
        $image = Image::make($image)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $image->save($image_path["standard"]["path"]);

        $image = Image::make($image)->resize(350, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $image->save($image_path["mini"]["path"]);
        return $image_path["link"];
    }

    /**
     * Создание директории для сохранения изображения
     *
     * @return array
     */
    private function createDirImage()
    {
        $path_random = Str::lower(Str::random(75));
        $random = Str::substr($path_random, 0, 2);
        $types = [
            "standard" => null,
            "mini" => null
        ];

        foreach ($types as $key => $type) {
            $path = "public/uploads/" . $key . "/" . $random;
            if (!file_exists($path)) {
                mkdir($path);
            }
            $path .= "/" . Str::substr($path_random, 2, 2);
            if (!file_exists($path)) {
                mkdir($path);
            }
            $path .= '/' . Str::substr($path_random, 4) . '.jpg';
            $types[$key]["path"] = $path;
            $types["link"] = str_replace("public/uploads/" . $key . "/", "", $path);
        }

        return $types;
    }
}

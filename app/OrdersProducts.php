<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OrdersProducts
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereUpdatedAt($value)
 */
class OrdersProducts extends Model
{
    protected $guarded = [];
}

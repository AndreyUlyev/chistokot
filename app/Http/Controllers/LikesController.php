<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesController extends SiteController
{
    private $CountProductsInPage = 2;

    /**
     * Get list of likes products.
     */
    public function index()
    {
        if (Auth::check()) {
            $likes = Like::where("user_id", Auth::user()->id)->paginate($this->CountProductsInPage);
        } else {
            $likes = Like::where("user_ip", \Request::ip())->paginate($this->CountProductsInPage);
        }

        return $this->renderOutput("products.likes", [
            "likes" => $likes
        ]);
    }

    /**
     * Set like for product.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            Like::firstOrCreate([
                "user_id" => Auth::user()->id,
                "product_id" => $request->product_id
            ]);
        } else {
            Like::firstOrCreate([
                "user_ip" => $request->ip(),
                "product_id" => $request->product_id
            ]);
        }

        return response()->json(["success" => true], 200);
    }

    /**
     * Remove like from product.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        if (Auth::check()) {
            Like::where("user_id", Auth::user()->id)
                ->where("product_id", $request->product_id)
                ->delete();
        } else {
            Like::where("user_ip", $request->ip())
                ->where("product_id", $request->product_id)
                ->delete();
        }

        return response()->json(["success" => true], 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class PaymentsController
 *
 * @package App\Http\Controllers
 */
class PaymentsController extends SiteController
{
    /**
     * Successful purchase
     *
     * @param Request $request
     * @return Factory|View
     */
    public function success(Request $request)
    {
        $payment = Payment::where("sber_order_id", $request->orderId);
        $payment->update([
            "success" => true
        ]);
        $payment = $payment->first();
        $order = $payment->orders->first();

        return $this->renderOutput('baskets.success', [
            'total_price' => $payment->total_price,
            'order_number' => $order->id,
            'payment_type' => config("payments.types." . (int)$order->payment_type_id . ".name"),
            'address' => $order->address,
            'is_payment' => true
        ]);
    }

    /**
     * Error buying
     *
     * @return string
     */
    public function error()
    {
        return "Ошибка";
    }
}

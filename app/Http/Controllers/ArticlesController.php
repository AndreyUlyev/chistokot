<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ArticlesController
 *
 * @package App\Http\Controllers
 */
class ArticlesController extends SiteController
{
    /**
     * Get list articles.
     *
     * @return Factory|View
     */
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->get();

        return $this->renderOutput('articles.list', [
            'articles' => $articles,
        ]);
    }

    /**
     * Show articles.
     *
     * @param $articleId
     * @return Factory|RedirectResponse|View
     */
    public function show($articleId)
    {
        $article = Article::whereId($articleId)->first();
        if ($article == null) {
            return redirect()->route("home");
        }
        return $this->renderOutput('articles.page', [
            'article' => $article
        ]);
    }
}

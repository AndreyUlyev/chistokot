<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Product;
use App\Slider;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Illuminate\View\View;

class MainPageController extends SiteController
{
    /**
     * Build main page.
     *
     * @return Factory|View
     */
    public function main()
    {
        $sliders = Slider::get()->sortBy('sorting_id');
        $categories = Category::get();

        $categoriesInfo = Category::where('is_on_main', true)->get();

        $promotions = Article::where("is_promotion", true)
            ->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();

        return $this->renderOutput('pages.main_page', [
            'sliders' => $sliders,
            'categories' => $categories,
            'categories_info' => $categoriesInfo,
            "promotions" => $promotions
        ]);
    }

    /**
     * Get list of popular products.
     */
    public function popularProducts()
    {
        $popularProducts = Product::where('products.is_popular', true)
            ->with("productImages")
            ->get();

        foreach ($popularProducts as &$product) {
            $product->desc = Str::words($product->description, 8);
        }

        return $popularProducts;
    }

    /**
     * Get products for popular categories.
     *
     * @param $categoryId
     * @return Product[]|Builder[]|Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function productsCategory($categoryId)
    {
        return Product::where("category_id", $categoryId)
            ->take(8)
            ->with("productImages")
            ->where("product_id", null)
            ->get();
    }
}

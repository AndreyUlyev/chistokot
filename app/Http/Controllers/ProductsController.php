<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\DeliveryType;
use App\Product;
use Illuminate\Http\Request;
use \Monarobase\CountryList\CountryListFacade as Countries;
use App\Like;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProductsController
 *
 * @package App\Http\Controllers
 */
class ProductsController extends SiteController
{
    private $countSentencesInShortDescription = 15;

    /**
     * Products page.
     *
     * @param Request $request
     * @param $slug - slug of product
     * @param bool $slugSize - slug of size product
     * @param bool $slugColor - slug of color product
     * @return string
     */
    public function show(Request $request, $slug, $slugSize = false, $slugColor = false)
    {
        $product = Product::where("slug", $slug)
            ->when($slugSize != 'default' && $slugSize != false, function ($q) use ($slugSize) {
                return $q->where('slug_size', $slugSize);
            })
            ->when($slugColor != false, function ($q) use ($slugColor) {
                return $q->where('slug_color', $slugColor);
            })
            ->first();

        $productId = $product->id;
        $parentProduct = $product->product_id;

        if ($parentProduct != 0) {
            $productsGroup = Product::where(function ($query) use ($productId, $parentProduct) {
                $query->where('id', $parentProduct)
                    ->where("id", "!=", $productId);
            })->orWhere(function ($query) use ($productId, $parentProduct) {
                $query->where('product_id', $parentProduct)
                    ->where("id", "!=", $productId);
            })->get();
        } else {
            $productsGroup = Product::where("product_id", $productId)->get();
        }

        if ($product == null) {
            return redirect()->route("home");
        }

        if (Auth::check()) {
            $like = Like::where("product_id", $product->id)
                ->where("user_id", Auth::user()->id)
                ->count() ? 1 : 0;
        } else {
            $like = Like::where("product_id", $product->id)
                ->where("user_ip", $request->ip())
                ->count() ? 1 : 0;
        }

        $product->discount = null;
        if ($product->price_discount != null) {
            $product->discount = ceil(100 - ($product->price_discount / $product->price) * 100);
        }

        $product->short_description = $this->getShortDescription($product->description);
        $product->country_name = Countries::getOne($product->country, 'ru');
        $category = Category::where("id", $product->category_id)->first();

        $ip = $request->ip();
        $checkLike = Like::where(function ($query) use ($productId) {
            $query->where('user_id', Auth::user()->id ?? null)
                ->where("product_id", $productId);
        })->orWhere(function ($query) use ($productId, $ip) {
            $query->where('user_ip', $ip)
                ->where("product_id", $productId);
        })->exists();

        return $this->renderOutput("products.page", [
            "product" => $product,
            "like" => $like,
            'products' => $this->withBuy(),
            "meta_title" => $product->meta_title,
            "meta_description" => $product->meta_description,
            "meta_keywords" => $product->meta_keywords,
            "delivery_types" => DeliveryType::orderBy("id", "desc")->get(),
            "payment_types" => config("payments.types"),
            "category" => $category,
            "active_like" => $checkLike,
            "products_group" => $productsGroup
        ]);
    }

    /**
     * Get short decription of product.
     *
     * @param $description
     * @return string
     */
    private function getShortDescription($description)
    {
        $sentences = array_slice(explode(".", $description), 0, $this->countSentencesInShortDescription);
        return implode(".", $sentences) . ".";
    }
}

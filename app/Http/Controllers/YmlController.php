<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * Class YmlController
 *
 * @package App\Http\Controllers
 */
class YmlController extends Controller
{
    /**
     * Return yml catalog with products
     *
     * @return Application|ResponseFactory|Response
     */
    public function index()
    {
        $yml = [];
        $yml["shop"]["name"] = "Chistokot";
        $yml["shop"]["company"] = "Белый кот";
        $yml["shop"]["url"] = config("app.url");
        $yml["shop"]["currencies"]["currency"]["_attributes"]["id"] = "RUR";
        $yml["shop"]["currencies"]["currency"]["_attributes"]["rate"] = "1";

        $categories = Category::get();
        foreach ($categories as $category) {
            $yml["shop"]["categories"]["category"][] = [
                "_value" => $category->name,
                "_attributes" => ["id" => $category->id]
            ];
        }

        $products = Product::with("productImages")->get();
        foreach ($products as $product) {
            $ymlProduct = [];
            $ymlProduct["name"] = $product->name;
            $ymlProduct["url"] = route("products.page", ["slug" => $product->slug]);
            $ymlProduct["_attributes"] = ["id" => $product->id];
            $ymlProduct["currencyId"] = "RUR";
            $ymlProduct["categoryId"] = $product->category_id;
            $ymlProduct["picture"] = asset($product->productImages[0]->link);
            $ymlProduct["description"] = strip_tags($product->description);
            $ymlProduct["price"] = ($product->price_discount != null) ? $product->price_discount : $product->price;

            if ($product->price_discount != null) {
                $ymlProduct["oldprice"] = $product->price;
            }

            $params = [
                "Состав" => $product->composition,
                "О доставке" => $product->about_delivery,
                "Гарантии" => $product->guarantees,
                "Применение" => $product->where_apply
            ];
            foreach ($params as $key => $param) {
                if ($param != 0) {
                    $ymlProduct["param"][] = [
                        "_attributes" => ["name" => $key],
                        "_value" => strip_tags($param)
                    ];
                }
            }

            $yml["shop"]["offers"]["offer"][] = $ymlProduct;
        }

        $r = ArrayToXml::convert($yml, [
            'rootElementName' => 'yml_catalog',
            '_attributes' => [
                'date' => Carbon::now()->format('Y-m-d H:i'),
            ],
        ], true, 'UTF-8');
        return response($r, 200, [
            'Content-Type' => 'application/xml'
        ]);
    }
}

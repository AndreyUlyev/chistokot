<?php

namespace App\Http\Controllers;

use App\Http\Controllers\SiteController;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers
 */
class UsersController extends SiteController
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        return $this->renderOutput('pages.profile_editing');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        User::whereId($id)->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route("admin.users.index")->withSuccess("Пользователь успешно изменен");
    }
}

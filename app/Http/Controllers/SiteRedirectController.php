<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class SiteRedirectController
 *
 * @package App\Http\Controllers
 */
class SiteRedirectController extends Controller
{
    /**
     * Method for redirecting products from old URLs.
     *
     * @param $slug
     * @return RedirectResponse
     */
    public function redirect($slug)
    {
        $product = Product::select("id")->where("slug", $slug)->count();
        if ($product) {
            return redirect()->route("products.page", ["slug" => $slug]);
        } else {
            $category = Category::select("id")->where("slug", $slug)->count();
            if ($category) {
                return redirect()->route("categories.page", ["slug" => $slug]);
            } else {
                return redirect()->route("home");
            }
        }
    }
}

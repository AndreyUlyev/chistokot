<?php

namespace App\Http\Controllers;

use App\Article;
use App\Basket;
use App\Category;
use App\Mail\NotificationMail;
use App\Product;
use App\Promocode;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\ImageManagerStatic as Image;
use MoySklad\MoySklad;

/**
 * Class SiteController
 *
 * @package App\Http\Controllers
 */
class SiteController extends Controller
{
    /**
     * Get footer.
     *
     * @return array
     */
    public function footer()
    {
        return [];
    }


    /**
     *  Get menu items.
     *
     * @return array - массив пунтов меню
     */
    private function menu()
    {
        $categories = Category::orderBy("position")->get();
        $popularCategories = Category::where("is_on_main", true)
            ->limit(4)
            ->orderBy("position", "desc")
            ->get();
        return [
            "categories" => $categories,
            "popular_categories" => $popularCategories
        ];
    }

    /**
     * Rendering layout.
     *
     * @param $view
     * @param $vars
     * @return Factory|View
     */
    protected function renderOutput($view, $vars = [])
    {
        return view($view)->with($vars)->with([
            'menu' => $this->menu(),
            'footer' => $this->footer(),
            'countProductsInBasket' => $this->countProductsInBasket()
        ]);
    }

    /**
     * Rendering admin layout.
     *
     * @param $view
     * @param array $vars
     * @return Factory|View
     */
    protected function renderOutputAdmin($view, $vars = [])
    {
        $menu = config("admin_menu");
        return view($view)->with($vars)->withMenu($menu);
    }

    /**
     * Upload images.
     *
     * @param $images
     * @param $width
     * @param $height
     * @return array
     */
    protected function loadImages($images, $width, $height)
    {
        $paths = [];
        foreach ($images as $imageB64) {
            $imagePath = $this->createDirImage();
            $image = Image::make($imageB64)->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $watermark = $this->insertWatermark($image);
            $image->insert($watermark, 'center');
            $image->save($imagePath["standard"]["path"]);

            $image = Image::make($image)->resize(350, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image->save($imagePath["mini"]["path"]);
            $paths[] = $imagePath["link"];
        }

        return $paths;
    }

    /**
     * Set watermark for image.
     *
     * @param $img
     * @return \Intervention\Image\Image
     */
    private function insertWatermark($img)
    {
        $watermark = Image::make('uploads/watermark.png');
        $resizePercentage = 10;
        $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100), 2);
        $watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        return $watermark;
    }

    /**
     * Create path foolders for save image.
     *
     * @return array
     */
    private function createDirImage()
    {
        $pathRandom = Str::lower(Str::random(75));
        $random = Str::substr($pathRandom, 0, 2);
        $formats = [
            "standard" => null,
            "mini" => null
        ];

        foreach ($formats as $key => $format) {
            $path = "uploads/" . $key . "/" . $random;
            if (!file_exists($path)) {
                mkdir($path);
            }
            $path .= "/" . Str::substr($pathRandom, 2, 2);
            if (!file_exists($path)) {
                mkdir($path);
            }
            $path .= '/' . Str::substr($pathRandom, 4) . '.jpg';
            $formats[$key]["path"] = $path;
            $formats["link"] = str_replace("uploads/" . $key . "/", "", $path);
        }

        return $formats;
    }

    /**
     *  Get full price products in basket.
     *
     * @param $userProducts
     * @param bool $promocode
     * @return int
     */
    public static function totalPrice($userProducts, $promocode = false)
    {
        $totalPrice = 0;
        foreach ($userProducts as $userProduct) {
            $discount = 0;
            if ($promocode != false) {
                $discount = Promocode::select("discount")
                        ->where("category_id", $userProduct->category_id)
                        ->orWhere("product_id", $userProduct->id)
                        ->orWhere("brand_id", $userProduct->brand_id)
                        ->first()["discount"] / 100;
            }

            if ($userProduct->price_discount != null) {
                $totalPrice += $userProduct->quantity *
                    ($userProduct->price_discount - $userProduct->price_discount * $discount);
            } else {
                $totalPrice += $userProduct->quantity * ($userProduct->price - $userProduct->price * $discount);
            }
        }

        return $totalPrice;
    }

    /**
     * Get count products in basket.
     *
     * @return int
     */
    private function countProductsInBasket()
    {
        $basket = session()->get('basket');
        if (Auth::check()) {
            return Basket::where("user_id", Auth::user()->id)->count();
        } elseif ($basket) {
            return count($basket);
        } else {
            return 0;
        }
    }

    /**
     * Products of the section " with this often buy".
     *
     * @return Collection
     */
    protected function withBuy()
    {
        return Product::inRandomOrder()->take(8)->get();
    }
}

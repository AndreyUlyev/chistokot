<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class SearchController
 *
 * @package App\Http\Controllers
 */
class SearchController extends SiteController
{
    private $itemsInPage = 15;

    /**
     * Main page of seaching
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $term = $request->search;
        if (empty($term)) {
            return $this->renderOutput('search.list')->withError("Заполните поле поиска");
        }

        $items = Product::where('name', 'LIKE', '%' . $term . '%')
            ->orWhere('description', 'LIKE', '%' . $term . '%')
            ->paginate($this->itemsInPage);

        foreach ($items as &$item) {
            $item->description = Str::limit($item->description, 30);
            $item->route = route('products.page', ["slug" => $item->slug]);
        }

        if ($items->count() > 0) {
            $data = ["products" => $items];
        } else {
            $data = ["error" => "Искомая комбинация слов нигде не встречается."];
        }

        return $this->renderOutput('search.list', $data);
    }
}

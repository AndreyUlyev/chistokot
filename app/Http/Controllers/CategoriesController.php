<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Comment;
use App\CommentsSiteLikes;
use App\Modification;
use App\Product;
use App\Slider;
use DB;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class CategoriesController
 *
 * @package App\Http\Controllers
 */
class CategoriesController extends SiteController
{
    private $countProductsInPage = 27;
    private $categoriesInPage = 15;
    private $slidersInPage = 10;

    /**
     * Get list of categories.
     *
     * @return Factory|View
     */
    public function index()
    {
        $categories = Category::orderBy("position", "desc")->orderBy("name");
        $sliders = Slider::paginate($this->slidersInPage);
        return $this->renderOutput('category.list', [
            'all_categories' => $categories->get(),
            'categories' => $categories->paginate($this->categoriesInPage),
            "sliders" => $sliders
        ]);
    }

    /**
     * Get data for filters.
     *
     * @param $slug
     * @return array
     */
    public function filters($slug)
    {
        $countProducts = Product::selectRaw("COUNT(products.id)")
            ->whereColumn("category_id", "categories.id")
            ->latest()
            ->getQuery();

        $data = [];
        $data["categories"] = Category::select(["categories.name", "categories.slug"])
            ->selectSub($countProducts, 'count_product')
            ->groupBy('categories.name')
            ->orderBy("position", "desc")
            ->orderBy("name")
            ->get();

        $priceRange = Product::selectRaw("LEAST(MIN(price), MIN(IFNULL(price_discount, price))) as min_price, GREATEST(MAX(price), MAX(IFNULL(price_discount, price))) as max_price")
            ->where("category_id", function ($query) use ($slug) {
                $query->select("id")
                    ->from("categories")
                    ->where('slug', $slug);
            })
            ->first();
        $data["price"]["max"] = $priceRange->max_price;
        $data["price"]["min"] = $priceRange->min_price;
        return $data;
    }

    /**
     * Get products by category.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function getProducts(Request $request)
    {
        $filters = json_decode($request->filters);
        $slug = $request->slug;

        $avgRating = Comment::selectRaw('AVG(rating)')->whereColumn("product_id", "id");
        $countComments = Comment::selectRaw('COUNT(id)')->whereColumn("product_id", "products.id");

        $products = Product::select([
            "products.*",
            "product_modifications.is_main",
            "modifications.main_modification_id",
            "modifications.id as modification_id"
        ])
            ->leftJoin('product_modifications', 'products.id', '=', 'product_modifications.product_id')
            ->leftJoin('modifications', 'modifications.id', '=', 'product_modifications.modification_id')
            ->where("category_id", function ($query) use ($slug) {
                $query->select('id')
                    ->from(with(new Category())->getTable())
                    ->where('slug', $slug);
            })
            ->where(function ($query) {
                return $query->WhereNull("product_modifications.is_main")
                    ->orWhere("product_modifications.is_main", 1);
            })
            ->when($filters->price_range != null, function ($query) use ($filters) {
                return $query->whereRaw("LEAST(price, IFNULL(price_discount, price)) >= " . $filters->price_range[0]
                    . " AND LEAST(price, IFNULL(price_discount, price)) <= " . $filters->price_range[1]);
            })
            ->when($filters->only_discount === true, function ($query) use ($filters) {
                return $query->where("price_discount", "!=", null);
            })
            ->selectSub($avgRating, 'avg_rating')
            ->selectSub($countComments, 'count_comments')
//            ->filterOrderBy($filters->order->type)
            ->with("productImages")
            ->get();

        foreach ($products as &$product) {
            if ($product->main_modification_id == null) {
                $product->modifications = null;
                continue;
            }

            $product->modifications = Modification::select([
                "id",
                "name",
                "modification_id",
                "color_id",
                "type_modification_id"
            ])
                ->fromRaw("modifications, (select @pv := {$product->main_modification_id}) initialisation")
                ->where(function ($query) {
                    $query->whereRaw("find_in_set(modification_id, @pv)")
                        ->whereRaw("@pv := concat(@pv, ',', id)");
                })
                ->orWhereRaw("id = @pv")
                ->with("color")
                ->with("type")
                ->with("products.productImages")
                ->orderBy("type_modification_id")
                ->get();
            $product->current = null;
        }

        return $products;
    }

    /**
     * Get category page.
     *
     * @param Request $request
     * @param $slug
     * @return Factory|RedirectResponse|View
     */
    public function show(Request $request, $slug)
    {
        $category = Category::where("slug", $slug)->first();
        if ($category == null) {
            return redirect()->route("home");
        }
        return $this->renderOutput("category.page", [
            "category" => $category
        ]);
    }
}

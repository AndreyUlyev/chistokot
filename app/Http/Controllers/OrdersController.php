<?php

namespace App\Http\Controllers;

use App\Basket;
use App\Mail\AdminNotificationMail;
use App\Mail\NewOrder;
use App\Order;
use App\OrdersProducts;
use App\Payment;
use App\Product;
use App\Promocode;
use Exception;
use GuzzleHttp\Client;
use http\Env\Response;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class OrdersController
 *
 * @package App\Http\Controllers
 */
class OrdersController extends SiteController
{
    public function __construct()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Factory|JsonResponse|View
     * @throws Exception
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'second_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'delivery_type_id' => 'required',
            'payment_type_id' => 'required',
            'agree' => 'required|in:1'
        ], [
            'second_name.required' => 'Поле "Фамилия" обязательно для заполнения.',
            'agree.in' => 'Примите условия публичной оферты.',
        ]);

        $promocode = false;
        if ($request->input("promocode", false) != false) {
            $promocode = Promocode::where('name', $request->promocode)->count();
            if (!$promocode) {
                return response()->json(["success" => false, "mess" => "Указанный промокод не существует."], 400);
            }
        }

        if ($validator->fails()) {
            return response()->json(["success" => false, "mess" => $validator->errors()->first()], 400);
        }

        if (Auth::check()) {
            $userProducts = Product::select([
                "baskets.quantity",
                "products.name",
                "products.slug",
                "products.id",
                "products.price",
                "products.price_discount",
                "products.category_id",
                "products.brand_id"
            ])
                ->join('baskets', 'products.id', '=', 'baskets.product_id')
                ->where('baskets.user_id', Auth::user()->id)
                ->get();
        } else {
            $orderProducts = session()->get('basket');
            if ($orderProducts == null) {
                return response()->json(["success" => false, "mess" => "Корзина пуста"], 400);
            }
            $userProducts = Product::select([
                "products.name",
                "products.id",
                "products.price",
                "products.price_discount",
                "products.slug",
                "products.category_id",
                "products.brand_id"
            ])->whereIn('id', array_keys($orderProducts))->get();
            foreach ($userProducts as &$userProduct) {
                $userProduct->quantity = $orderProducts[$userProduct->id];
            }
        }

        if (Auth::check()) {
            Basket::where('user_id', Auth::user()->id)->delete();
        } else {
            session()->forget('basket');
        }

        return $this->doOrder($request, $userProducts);
    }

    /**
     * Build order.
     *
     * @param Request $request
     * @param bool $userProducts
     * @return JsonResponse|RedirectResponse|Redirector
     */
    private function doOrder(Request $request, $userProducts = false)
    {
        $totalPrice = $this->totalPrice($userProducts, $request->input("promocode", false));

        $orderId = Order::create([
            'phone' => $request->phone,
            'address' => $request->address,
            'delivery_type_id' => $request->delivery_type_id,
            'user_id' => Auth::user()->id ?? null,
            'promocode_id' => $request->promocode_id,
            'payment_type_id' => $request->payment_type_id,
            'total_price' => $totalPrice,
            'name' => $request->name,
            'second_name' => $request->second_name,
            'moy_sklad_id' => null
        ])->id;

        Mail::to("mail@chistokot.ru")->queue(new NewOrder([
            "id" => $orderId,
            "price" => $totalPrice,
            "payment_type" => config("payments.types." . (int)$request->payment_type_id . ".name"),
            "email_user" => $request->input("email", null),
            "phone_user" => $request->phone,
            "fio_user" => $request->name . " " . $request->second_name,
            "products" => $userProducts
        ]));

        $this->saveBoughtProducts($userProducts, $orderId);

        if (config("payments.types." . (int)$request->payment_type_id . ".is_bank") == true) {
            return $this->onlinePayment($orderId, $totalPrice);
        }

        return response()->json([
            'total_price' => $totalPrice,
            'order_number' => $orderId,
            'payment_type' => config("payments.types." . (int)$request->payment_type_id . ".name"),
            'address' => $request->address,
        ]);
    }

    /**
     * By product one click.
     *
     * @param Request $request
     * @return Factory|JsonResponse|View
     * @throws Exception
     */
    public function byOneClick(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'integer|required',
            'quantity' => 'integer|required',
            'name' => 'required',
            'phone' => 'required',
            'delivery_type_id' => 'required',
            'payment_type_id' => 'required',
            'agree' => 'required|in:1'
        ], [
            'payment_type_id.required' => 'Выберите тип оплаты.',
            'agree.in' => 'Примите условия публичной оферты.'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "mess" => $validator->errors()->first()], 400);
        }

        $userProducts = Product::where("id", $request->product_id)->get();
        $userProducts[0]->quantity = $request->quantity;

        return $this->doOrder($request, $userProducts);
    }

    /**
     * Buy product by sberbank online.
     *
     * @param $orderId
     * @param $totalPrice
     * @return Application|JsonResponse|RedirectResponse|Redirector
     */
    private function onlinePayment($orderId, $totalPrice)
    {
        $vars = [];
        $vars['userName'] = config("payments.settings.login");
        $vars['password'] = config("payments.settings.password");

        $vars['orderNumber'] = $orderId;
        $vars['amount'] = $totalPrice * 100;
        $vars['returnUrl'] = route("payments.success");
        $vars['failUrl'] = route("payments.error");
        $vars['description'] = 'Заказ №' . $orderId;

        $client = new Client();
        $response = $client->request('GET', 'https://3dsec.sberbank.ru/payment/rest/register.do', [
            'query' => $vars
        ]);
        $data = json_decode($response->getBody()->getContents());
        Payment::create([
            "sber_order_id" => $data->orderId,
            "total_price" => $totalPrice,
            "order_id" => $orderId
        ]);

        if (\Illuminate\Support\Facades\Request::ajax()) {
            return response()->json(["redirect" => $data->formUrl], 200);
        } else {
            return redirect($data->formUrl);
        }
    }

    /**
     * Preservation of purchased products.
     *
     * @param $userProducts
     * @param $orderId
     */
    private function saveBoughtProducts($userProducts, $orderId)
    {
        foreach ($userProducts as $userProduct) {
            OrdersProducts::create([
                'order_id' => $orderId,
                'product_id' => $userProduct->id,
                'quantity' => $userProduct->quantity
            ]);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Basket;
use App\DeliveryType;
use App\PaymentType;
use App\Product;
use App\ProductsImage;
use App\Promocode;
use App\User;
use Exception;
use Illuminate\Contracts\Session\Session;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class BasketsController
 *
 * @package App\Http\Controllers
 */
class BasketsController extends SiteController
{
    /**
     * Return view basket for vue.
     *
     * @return Factory|View
     */
    public function index()
    {
        return $this->renderOutput('baskets.list');
    }

    /**
     * Get data products in basket.
     *
     * @return array
     */
    public function getData()
    {
        $products = session()->get('basket');
        $deliveryTypes = DeliveryType::orderBy("id", "desc")->get();
        $paymentTypes = config("payments.types");

        if (Auth::check()) {
            $userProducts = Product::select([
                "baskets.quantity",
                "products.name",
                "products.id",
                "products.price",
                "products.price_discount",
                "products.quantity as max_quantity"
            ])
                ->join('baskets', 'products.id', '=', 'baskets.product_id')
                ->where('baskets.user_id', Auth::user()->id)
                ->with("productImages")
                ->get();
        } elseif ($products) {
            $userProducts = Product::select([
                "products.name",
                "products.id",
                "products.price",
                "products.price_discount",
                "products.quantity as max_quantity"
            ])
                ->whereIn('id', array_keys($products))
                ->with("productImages")
                ->get();
            foreach ($userProducts as &$userProduct) {
                $userProduct->quantity = $products[$userProduct->id];
            }
        } else {
            $userProducts = [];
        }
        return [
            'products' => $userProducts,
            'delivery_types' => $deliveryTypes,
            'payment_types' => $paymentTypes,
            'user' => Auth::user() ?? false
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|integer|min:0',
            'product_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "mess" => $validator->errors()->all()], 400);
        }

        $product = Product::select(["quantity", "id", "price_discount", "price"])
            ->whereId($request->product_id)
            ->first();
        if ($product->quantity === 0) {
            return response()->json(["success" => false, "mess" => "Выбранный продукт закончился"], 400);
        }

        $data = [
            "success" => true,
            "mess" => "Товар добавлен в корзину",
            "new_product" => false
        ];

        if (Auth::check()) {
            $basket = Basket::updateOrCreate(
                [
                    'user_id' => Auth::user()->id,
                    'product_id' => $product->id
                ],
                [
                    'quantity' => \DB::raw('quantity + ' . $request->quantity)
                ]
            );

            if ($basket->wasRecentlyCreated) {
                $data["new_product"] = true;
            }
        } else {
            $basket = session()->get('basket');
            if (!$basket) {
                $basket = [
                    $product->id => $request->quantity
                ];
                $data["new_product"] = true;
            } elseif (isset($basket[$product->id])) {
                $basket[$product->id] += $request->quantity;
            } else {
                $data["new_product"] = true;
                $basket[$product->id] = $request->quantity;
            }
            session()->put('basket', $basket);
        }

        $data["price"] = $this->totalPrice([
            (object)[
                "price_discount" => $product->price_discount,
                "price" => $product->price,
                "quantity" => $request->quantity
            ]
        ]);

        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $productId
     * @return array
     */
    public function update(Request $request, $productId)
    {
        if (Auth::check()) {
            if ($request->inc === true) {
                Basket::where("product_id", $productId)->where('user_id', Auth::user()->id)->increment('quantity');
            } else {
                Basket::where("product_id", $productId)->where('user_id', Auth::user()->id)->decrement('quantity');
            }
        } else {
            $basket = session()->get('basket');
            if ($request->inc === true) {
                $basket[$productId]++;
            } else {
                $basket[$productId]--;
            }
            session()->put('basket', $basket);
        }

        return ["success" => true];
    }

    /**
     * Remove product from basket.
     *
     * @param null $productId
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($productId = null)
    {
        if ($productId == null) {
            if (Auth::check()) {
                Basket::where('user_id', Auth::user()->id)->delete();
            } else {
                session()->forget('basket');
            }
            $data["success"] = true;
            $data["mess"] = ["Корзина успешно очищена"];
            return response()->json($data, 200);
        } else {
            if (Auth::check()) {
                Basket::where('product_id', $productId)->where("user_id", Auth::user()->id)->delete();
            } else {
                session()->forget('basket.' . $productId);
            }
            $data["success"] = true;
            $data["mess"] = ["Товар успешно удален"];
            return response()->json($data, 200);
        }
    }
}

<?php

namespace App\Http\Controllers;

/**
 * Class StaticPagesController
 *
 * @package App\Http\Controllers
 */
class StaticPagesController extends SiteController
{
    public function contact()
    {
        return $this->renderOutput('pages.contact');
    }

    public function shipping()
    {
        return $this->renderOutput('pages.shipping');
    }

    public function gifts()
    {
        return $this->renderOutput('pages.gifts');
    }

    public function priceList()
    {
        return $this->renderOutput('pages.price_list');
    }

    public function cooperation()
    {
        return $this->renderOutput('pages.cooperation');
    }

    public function jobOffers()
    {
        return $this->renderOutput('pages.job_offers');
    }

    public function purchaseRules()
    {
        return $this->renderOutput('pages.purchase_rules');
    }

    public function admin()
    {
        return $this->renderOutputAdmin('layouts.admin_app');
    }
}

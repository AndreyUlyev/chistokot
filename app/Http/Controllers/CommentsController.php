<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Validator;

class CommentsController extends Controller
{
    private $commentsInPage = 1;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $comments = Comment::where('product_id', $request->product_id)
            ->where("approved", true)
            ->paginate($this->commentsInPage);
        return [
            "comments" => $comments,
            "check_auth" => Auth::check()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'rating' => 'required|integer|between:1,5',
            'comment' => 'required',
            'user_id' => 'required_without:name',
            'name' => 'required_without:user_id',
            'product_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "mess" => "Заполните обязательные поля"], 400);
        }

        $comment = [
            "title" => $request->title,
            "rating" => $request->rating,
            "comment" => $request->comment,
            "product_id" => $request->product_id
        ];
        if (Auth::check()) {
            $comment["user_id"] = Auth::user()->id;
        } else {
            $comment["name"] = $request->name;
        }

        Comment::create($comment);

        return response()->json([
            'mess' => 'Комментарий успешно добавлен и находится на модерации!',
            'success' => true
        ]);
    }
}

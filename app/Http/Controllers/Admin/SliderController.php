<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\SiteController;
use App\Slider;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class SliderController
 *
 * @package App\Http\Controllers\Admin
 */
class SliderController extends SiteController
{
    private $slidersInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $sliders = Slider::paginate($this->slidersInPage);
        return $this->renderOutputAdmin("admin.slider.list", [
            "sliders" => $sliders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return $this->renderOutputAdmin("admin.slider.form", [
            'route' => route("admin.slider.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'link' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            $data["success"] = false;
            $data["mess"] = $validator->errors()->first();
            return response()->json($data, 400);
        }

        $userUploadedImg = $this->loadImages(json_decode($request->images), null, 1200);

        if (sizeof($userUploadedImg) > 1) {
            $data["success"] = false;
            $data["mess"] = "Допустимое количество изображений - не более одного";
            return response()->json($data, 400);
        }

        Slider::create([
            'description' => $request->description,
            'link' => $request->link,
            'image' => $userUploadedImg[0],
            'name' => $request->name
        ]);

        return response()->json(['mess' => "Слайдер успешно добавлен", 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return $this->renderOutputAdmin("admin.slider.form", [
            "slider" => $slider,
            "route" => route("admin.slider.update", ["id_slider" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'link' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            $data["success"] = false;
            $data["mess"] = $validator->errors()->all();
            return response()->json($data, 400);
        }

        $images = json_decode($request->images);

        $userUploadedImg = $this->loadImages($images, null, 1200);

        if (sizeof($userUploadedImg) > 1) {
            $data["success"] = false;
            $data["mess"] = "Допустимое количество изображений - не более одного";
            return response()->json($data, 400);
        }

        Slider::whereId($id)->update(
            [
                'description' => $request->description,
                'link' => $request->link,
                'image' => $userUploadedImg[0],
                'name' => $request->name
            ]
        );

        return response()->json(['mess' => "Слайдер успешно изменен", 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        Slider::whereId($id)->delete();
        return redirect()->route("admin.slider.index")->withSuccess("Cлайд успешно удален");
    }
}

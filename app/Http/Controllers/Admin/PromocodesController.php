<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Http\Controllers\SiteController;
use App\Product;
use App\Promocode;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PromocodesController extends SiteController
{
    private $promocodesInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $promocodes = Promocode::paginate($this->promocodesInPage);

        return $this->renderOutputAdmin("admin.promocodes.list", [
            "promocodes" => $promocodes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categoriesInfo = Category::all();
        $brandsInfo = Brand::all();
        $productsInfo = Product::all();

        return $this->renderOutputAdmin('admin.promocodes.form', [
            'categories' => $categoriesInfo,
            'products' => $productsInfo,
            'brands' => $brandsInfo,
            'route' => route('admin.promocodes.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'discount' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        Promocode::create([
           'name' => $request->name,
           'discount' => $request->discount,
           'category_id' => $request->category_id,
           'product_id' => $request->product_id,
           'brand_id' => $request->brand_id
        ]);

        return redirect()->route("admin.promocodes.index")->withSuccess("Промокод успешно добавлен");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $promocode = Promocode::find($id);
        $categoriesInfo = Category::get();
        $brandsInfo = Brand::get();
        $productsInfo = Product::get();

        return $this->renderOutputAdmin("admin.promocodes.form", [
            "promocode" => $promocode,
            'categories' => $categoriesInfo,
            'brands' => $brandsInfo,
            'products' => $productsInfo,
            "route" => route("admin.promocodes.update", ["id_promocode" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'discount' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        Promocode::whereId($id)->update([
            'name' => $request->name,
            'discount' => $request->discount,
            'category_id' => $request->category_id,
            'product_id' => $request->product_id,
            'brand_id' => $request->brand_id
        ]);

        return redirect()->route("admin.promocodes.index")->withSuccess("Промокод успешно изменен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Promocode $id
     * @return Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        Promocode::whereId($id)->delete();
        return redirect()->route("admin.promocodes.index")->withSuccess("Промокод успешно удален");
    }
}

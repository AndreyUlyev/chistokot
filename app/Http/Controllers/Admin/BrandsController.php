<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Controllers\SiteController;
use Dobro\MoySkladApi\Facades\CustomEntity;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class BrandsController
 *
 * @package App\Http\Controllers\Admin
 */
class BrandsController extends SiteController
{
    private $brandsInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $brands = Brand::orderBy("name")->paginate($this->brandsInPage);
//        return $this->renderOutputAdmin("admin.brands.list", [
//            "brands" => $brands
//        ]);
        return $brands;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return $this->renderOutputAdmin("admin.brands.form", [
            "route" => route("admin.brands.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        try {
            $categorySklad = CustomEntity::createValue([
                "name" => $request->name
            ], config("moy_sklad.brand.id"))->id;
        } catch (\ErrorException $e) {
            $categorySklad = false;
        }

        Brand::create([
            'name' => $request->name,
            'moy_sklad_id' => $categorySklad
        ]);

        return redirect()->route("admin.brands.index")->withSuccess("Бренд успешно добавлен");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        return $this->renderOutputAdmin("admin.brands.form", [
            "brand" => $brand,
            "route" => route("admin.brands.update", ["id_brand" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|void
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        Brand::whereId($id)->update([
            'name' => $request->name
        ]);

        CustomEntity::updateValue([
            "name" => $request->name
        ], config("moy_sklad.brand.id"), $request->brand_sklad_id);

        return redirect()->route("admin.brands.index")->withSuccess("Бренд успешно изменен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $moySkladBrand = Brand::select("moy_sklad_id")->whereId($id)->first();
        try {
            CustomEntity::deleteValue(config("moy_sklad.brand.id"), $moySkladBrand->moy_sklad_id);
        } catch (\ErrorException $e) {
        }
        Brand::whereId($id)->delete();
        return redirect()->route("admin.brands.index")->withSuccess("Бренд успешно удален");
    }
}

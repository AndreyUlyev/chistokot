<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Http\Controllers\SiteController;
use App\Modification;
use App\Product;
use App\ProductsImage;
use Dobro\MoySkladApi\Facades\CustomEntity;
use Dobro\MoySkladApi\Facades\ProductFolder;
use Dobro\MoySkladApi\Facades\Mods;
use ErrorException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;
use \Monarobase\CountryList\CountryListFacade as Countries;
use \Dobro\MoySkladApi\Facades\Product as ProductSklad;

/**
 * Class ProductsController
 *
 * @package App\Http\Controllers\Admin
 */
class ProductsController extends SiteController
{
    private $productsInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $products = Product::orderBy("name")
            ->leftJoin('product_modifications', 'products.id', '=', 'product_modifications.product_id')
            ->where(function ($query) {
                return $query->WhereNull("product_modifications.is_main")
                ->orWhere("product_modifications.is_main", 1);
            })
            ->paginate($this->productsInPage);
        return $this->renderOutputAdmin("admin.products.list", [
            "products" => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $brands = Brand::orderBy("name")->get();
        $categories = Category::orderBy("name")->get();
        $modifications = Modification::get();

        return $this->renderOutputAdmin("admin.products.form", [
            "route" => route("admin.products.store"),
            "brands" => $brands,
            "categories" => $categories,
            "countries" => Countries::getList('ru'),
            "modifications" => $modifications
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'price_discount' => 'numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required',
            'brand_id' => 'required',
            'is_popular' => 'boolean',
            'images' => 'required|min:1'
        ]);

        if ($validator->fails()) {
            $data["success"] = false;
            $data["mess"] = $validator->errors()->all();
            return response()->json($data, 400);
        }

        $productsImages = json_decode($request->images);
        if (!count((array)$productsImages)) {
            $data["success"] = false;
            $data["mess"] = ["Добавьте изображения товаров"];
            return response()->json($data, 400);
        }

        try {
//            if ($request->input("parent_product_id", false)) {
//                $skladProductId = $this->moySkladCreateUpdateModification($request->parent_product_id, $request);
//            } else {
//                $skladProductId = $this->moySkladCreateUpdateProduct(
//                    $request->brand_id,
//                    $request->category_id,
//                    $request
//                );
//            }
            $skladProductId = null;
        } catch (ErrorException $e) {
            $skladProductId = null;
        }

        $productId = Product::create([
            "name" => $request->name,
            "description" => $request->description,
            "price" => $request->price,
            "price_discount" => $request->input("price_discount", null) ?: null,
            "quantity" => $request->quantity,
            "category_id" => $request->category_id,
            "brand_id" => $request->brand_id,
            "is_popular" => $request->input("is_popular", false),
            "slug" => Str::slug($request->name, "_"),
            "composition" => $request->composition,
            "about_delivery" => $request->about_delivery,
            "guarantees" => $request->guarantees,
            "where_apply" => $request->where_apply,
            "maintenance" => $request->maintenance,
            "country" => $request->country,
            "moy_sklad_id" => $skladProductId,
            "meta_description" => $request->meta_description,
            "meta_title" => $request->meta_title,
            "meta_keywords" => $request->meta_keywords,
//            "size" => $request->mod == false ? null : $request->mod,
//            "color" => $request->color == false ? null : $request->color,
//            "slug_size" => $request->input("mod", false) != false ? Str::slug($request->mod) : null,
//            "slug_color" => $request->input("color", false) != false ? Str::slug($request->color) : null,
//            "product_id" => $request->input("parent_product_id", null)
        ])->id;

        $userUploadedImg = $this->loadImages($productsImages, 800, null);

        foreach ($userUploadedImg as $img) {
            ProductsImage::create([
                "link" => $img,
                "product_id" => $productId
            ]);
        }

        return response()->json(['mess' => "Продукт успешно добавлен", 'success' => true], 200);
    }

    /**
     * Set new product in MoySklad.
     *
     * @param $brandId
     * @param $categoryId
     * @param $request
     * @param bool $id
     * @return mixed
     */
    private function moySkladCreateUpdateProduct($brandId, $categoryId, $request, $id = false)
    {
        $moySkladBrandId = Brand::select("moy_sklad_id")->where("id", $brandId)->first()->moy_sklad_id;
        $moySkladCategoryId = Category::select("moy_sklad_id")->where("id", $categoryId)->first()->moy_sklad_id;

        $productFolder = ProductFolder::where("id", "=", (string)$moySkladCategoryId)->first()->meta;
        $moySkladBrand = CustomEntity::where("id", "=", (string)$moySkladBrandId)
            ->getEntityVal(config("moy_sklad.brand.id"))
            ->meta;

        $arr = [
            "name" => $request->name,
            "description" => $request->description,
            "productFolder" => ["meta" => $productFolder],
            "externalCode" => Str::slug($request->name, "_"),
            "article" => Str::slug($request->name, "_"),
            "salePrices" => [
                [
                    "value" => (float)$request->price * 100,
                    "currency" => config("moy_sklad.price.currency"),
                    "priceType" => config("moy_sklad.price.priceType")
                ],
                [
                    "value" => (float)$request->price_discount * 100,
                    "currency" => config("moy_sklad.price_discount.currency"),
                    "priceType" => config("moy_sklad.price_discount.priceType")
                ]
            ],
            "attributes" => [
                [
                    "meta" => config("moy_sklad.brand.meta"),
                    "value" => [
                        "meta" => (array)$moySkladBrand
                    ]
                ],
                [
                    "meta" => config("moy_sklad.quantity.meta"),
                    "value" => 12
                ],
                [
                    "meta" => config("moy_sklad.composition.meta"),
                    "value" => $request->composition
                ],
                [
                    "meta" => config("moy_sklad.about_delivery.meta"),
                    "value" => $request->about_delivery
                ],
                [
                    "meta" => config("moy_sklad.guarantees.meta"),
                    "value" => $request->guarantees
                ],
                [
                    "meta" => config("moy_sklad.where_apply.meta"),
                    "value" => $request->where_apply
                ],
                [
                    "meta" => config("moy_sklad.maintenance.meta"),
                    "value" => $request->maintenance
                ],
                [
                    "meta" => config("moy_sklad.meta_description.meta"),
                    "value" => $request->meta_description
                ],
                [
                    "meta" => config("moy_sklad.meta_title.meta"),
                    "value" => $request->meta_title
                ],
                [
                    "meta" => config("moy_sklad.meta_keywords.meta"),
                    "value" => $request->meta_keywords
                ],
            ]
        ];

        if ($id == false) {
            return ProductSklad::create($arr)->id;
        } else {
            return ProductSklad::where("id", "=", $id)->update($arr);
        }
    }

    /**
     * Create/update modification in MoySklad.
     *
     * @param $productId
     * @param $request
     * @param bool $id
     * @return
     */
    protected function moySkladCreateUpdateModification($productId, $request, $id = false)
    {
        $parentProductSkladId = Product::where("id", $productId)->first()["moy_sklad_id"];
        $skladProductMeta = ProductSklad::where("id", "=", $parentProductSkladId)->first()->meta;
        $mod = [
            "name" => $request->input("size", false) . $request->input("color", false),
            "characteristics" => [
                [
                    "id" => config("moy_sklad.mods.name"),
                    "value" => (string)$request->name,
                ]
            ],
            "salePrices" => [
                [
                    "value" => (float)$request->price * 100,
                    "currency" => config("moy_sklad.price.currency"),
                    "priceType" => config("moy_sklad.price.priceType")
                ],
                [
                    "value" => (float)$request->price_discount * 100,
                    "currency" => config("moy_sklad.price_discount.currency"),
                    "priceType" => config("moy_sklad.price_discount.priceType")
                ]
            ],
            "product" => [
                "meta" => $skladProductMeta
            ]
        ];

        if ($request->mod) {
            $mod["characteristics"][] = [
                "id" => config("moy_sklad.mods.size"),
                "value" => (string)$request->mod,
            ];
        }

        if ($request->color) {
            $mod["characteristics"][] = [
                "id" => config("moy_sklad.mods.color"),
                "value" => (string)$request->color,
            ];
        }

        if ($id == false) {
            return Mods::create($mod)->id;
        } else {
            return Mods::where("id", "=", $id)->update($mod);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $parentProductId = \Request::get('parent_product_id', false);

        $product = Product::find($id);
        $categoriesInfo = Category::get();
        $brandsInfo = Brand::get();

        $modifications = Product::where("product_id", $id)->get();

        $productsImages = ProductsImage::where("product_id", $id)->get();
        $colors = Product::select("color")
            ->where("color", "!=", null)
            ->groupBy("color")
            ->orderBy("color")
            ->pluck('color')
            ->toArray();

        return $this->renderOutputAdmin("admin.products.form", [
            "product" => $product,
            'categories' => $categoriesInfo,
            'brands' => $brandsInfo,
            "route" => route("admin.products.update", ["id_product" => $id]),
            "update" => true,
            "products_images" => $productsImages,
            "countries" => Countries::getList('ru'),
            "colors" => $colors,
            "modifications" => $modifications,
            "parent_product_id" => $parentProductId
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //remove unused images
        $images = json_decode($request->images);
        if (!count((array)$images)) {
            $data["success"] = false;
            $data["mess"] = ["Добавьте изображения товаров"];
            return response()->json($data, 400);
        }

        $newImages = [];
        $useImages = [];
        foreach ($images as $image) {
            if (is_int($image)) {
                $useImages[] = (int)$image;
            } else {
                $newImages[] = $image;
            }
        }

        ProductsImage::where("product_id", $id)
            ->whereNotIn("id", $useImages)
            ->delete();

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            $data["success"] = false;
            $data["mess"] = $validator->errors()->all();
            return response()->json($data, 400);
        }

        Product::whereId($id)->update([
            "name" => $request->name,
            "description" => $request->description,
            "price" => $request->price,
            "price_discount" => $request->input("price_discount", null) ?: null,
            "quantity" => $request->input("quantity", 1),
            "category_id" => $request->category_id,
            "brand_id" => $request->brand_id,
            "is_popular" => $request->input("is_popular", false),
            "slug" => Str::slug($request->name, "_"),
            "composition" => $request->composition,
            "about_delivery" => $request->about_delivery,
            "guarantees" => $request->guarantees,
            "where_apply" => $request->where_apply,
            "maintenance" => $request->maintenance,
            "country" => $request->country,
            "meta_description" => $request->meta_description,
            "meta_title" => $request->meta_title,
            "meta_keywords" => $request->meta_keywords,
            "size" => $request->mod == false ? null : $request->mod,
            "color" => $request->color == false ? null : $request->color,
            "slug_size" => $request->input("mod", false) != false ? Str::slug($request->mod) : null,
            "slug_color" => $request->input("color", false) != false ? Str::slug($request->color) : null,
        ]);

        $userUploadedImg = $this->loadImages($newImages, 800, null);

        foreach ($userUploadedImg as $img) {
            ProductsImage::create([
                "link" => $img,
                "product_id" => $id
            ]);
        }

        try {
            if ($request->input("parent_product_id", false)) {
                $skladProductId = $this->moySkladCreateUpdateModification(
                    $request->parent_product_id,
                    $request,
                    $request->product_sklad_id
                );
            } else {
                $this->moySkladCreateUpdateProduct(
                    $request->brand_id,
                    $request->category_id,
                    $request,
                    $request->product_sklad_id
                );
            }
        } catch (ErrorException $e) {
            $skladProductId = null;
        }

        return response()->json(['mess' => "Продукт успешно изменен", 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $skladProduct = Product::select(["moy_sklad_id", "product_id"])->whereId($id)->first();
        try {
            if ($skladProduct->product_id != null) {
                Mods::where("id", "=", $skladProduct->moy_sklad_id)->delete();
            } else {
                ProductSklad::where("id", "=", $skladProduct->moy_sklad_id)->delete();
            }
        } catch (ErrorException $e) {
        }

        Product::whereId($id)->delete();
        if (\Request::ajax()) {
            return response()->json(["success" => true]);
        } else {
            return redirect()->route("admin.products.index")->withSuccess("Проудкт успешно удален");
        }
    }
}

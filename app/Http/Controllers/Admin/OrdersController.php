<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\SiteController;
use App\Order;
use App\OrdersProducts;
use App\PaymentType;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class OrdersController
 *
 * @package App\Http\Controllers\Admin
 */
class OrdersController extends SiteController
{
    private $ordersInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $orders = Order::orderBy('created_at', 'desc')
            ->with("products")
            ->paginate($this->ordersInPage);

//        return $this->renderOutputAdmin("admin.orders.list", [
//            "orders" => $orders
//        ]);
        return $orders;
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Factory|View
     */
    public function show($id)
    {
        $order = Order::whereId($id)->with('deliveryType')->with("products")->first();
        return $this->renderOutputAdmin('admin.orders.show', [
            'order' => $order,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view("orders.form", [
            'route' => route("admin.orders.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'address' => 'required',
            'delivery_type_id' => 'required',
            'payment_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        $userId = Auth::id();

        Order::create([
            'user_id' => $userId,
            'phone' => $request->phone,
            'address' => $request->address,
            'delivery_type_id' => $request->delivery_type_id,
            'promocode_id' => $request->promocode_id,
            'payment_type_id' => $request->payment_type_id
        ]);

        return response()->redirectTo('admin.orders.index')->withSuccess("Заказ успешно создан");
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $order = Order::find($id);
        return view("admin.order.form", [
            'order' => $order,
            'route' => route('admin.orders.update', ['id_order' => $id]),
            'update' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'address' => 'required',
            'delivery_type_id' => 'required',
            'payment_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        Order::whereId($id)->update([
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'delivery_type_id' => $request->input('delivery_type_id'),
            'promocode_id' => $request->input('promocode_id'),
            'payment_type_id' => $request->input('payment_type_id')
        ]);

        return redirect()->route('admin.orders.index')->withSuccess("Заказ успешно отредактирован");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        Order::whereId($id)->delete();
        return redirect()->route('admin.orders.index')->withSuccess("Заказ успешно удален");
    }
}

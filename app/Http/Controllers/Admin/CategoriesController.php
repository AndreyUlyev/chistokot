<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\SiteController;
use Dobro\MoySkladApi\Facades\ProductFolder;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class CategoriesController
 * @package App\Http\Controllers
 */
class CategoriesController extends SiteController
{
    private $categoriesInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $categories = Category::orderBy("name")->paginate($this->categoriesInPage);
//        return $this->renderOutputAdmin("admin.categories.list", [
//            "categories" => $categories
//        ]);
        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return $this->renderOutputAdmin("admin.categories.form", [
            "route" => route("admin.categories.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'is_on_main' => 'boolean',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['mess' => $validator->errors()->all(), 'success' => false], 400);
        }

        $userUploadedImg = $this->loadImages(json_decode($request->images), null, 600);

        if (sizeof($userUploadedImg) > 1) {
            $data["success"] = false;
            $data["mess"] = "Допустимое количество изображений - не более одного";
            return response()->json($data, 400);
        }

        $categorySklad = ProductFolder::create([
            "name" => $request->name,
            "description" => $request->description
        ]);

        Category::create([
            'name' => $request->name,
            'description' => $request->description,
            'is_on_main' => $request->input("is_on_main", false),
            'image' => $userUploadedImg[0],
            'slug' => Str::slug($request->name, "_"),
            'moy_sklad_id' => $categorySklad->id
        ])->id;

        return response()->json(['mess' => "Категория успешно добавлена", 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return $this->renderOutputAdmin("admin.categories.form", [
            "category" => $category,
            "route" => route("admin.categories.update", ["id_category" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'images' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['mess' => $validator->errors()->first(), 'success' => true], 400);
        }

        $categoryArr = [
            'name' => $request->name,
            'description' => $request->description,
            'is_on_main' => $request->input("is_on_main", false),
            'slug' => Str::slug($request->name, "_")
        ];

        $images = json_decode($request->images, true);

        if (!is_int($images[0])) {
            $userUploadedImg = $this->loadImages($images, 800, null);
            $categoryArr["image"] = $userUploadedImg[0];
        }

        Category::whereId($id)->update($categoryArr);

        ProductFolder::where("id", "=", (string)$request->category_sklad_id)->update([
            "name" => $request->name,
            "description" => $request->description
        ]);

        return response()->json(['mess' => "Категория успешно изменена", 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     * @throws Exception
     */
    public function destroy($id)
    {
        $cetagory = Category::select("moy_sklad_id")->where("id", $id)->first();
        try {
            Category::whereId($id)->delete();
        } catch (\ErrorException $e) {
        }
        ProductFolder::where("id", "=", (string)$cetagory->moy_sklad_id)->delete();
        return redirect()->route("admin.categories.index")->withSuccess("Категория успешно удалена");
    }

    /**
     * Sorted categories.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sortable(Request $request)
    {
        $categories = $request->sortable;

        foreach ($categories as $id => $position) {
            Category::whereId($id)->update(['position' => $position]);
        }

        $data["success"] = true;
        $data["mess"] = "Обновлено";
        return response()->json($data);
    }
}

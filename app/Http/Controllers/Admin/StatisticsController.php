<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function getStatistics()
    {
        return [
            "orders_amount" => Payment::where("success", true)->sum("total_price"),
            "orders_count" => Payment::where("success", true)->count(),
            "products_count" => Product::count(),
            "users_count" => User::count()
        ];
    }
}

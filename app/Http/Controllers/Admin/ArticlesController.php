<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\SiteController;
use App\Product;
use App\ProductsArticle;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class ArticlesController
 *
 * @package App\Http\Controllers\Admin
 */
class ArticlesController extends SiteController
{
    private $articlesInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->paginate($this->articlesInPage);

        return $this->renderOutputAdmin("admin.articles.list", [
            "articles" => $articles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Category::orderBy("name")->with('products')->get();

        return $this->renderOutputAdmin("admin.articles.form", [
            'categories' => $categories,
            "route" => route("admin.articles.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'content' => 'required',
            'category_id' => 'required',
            'images' => 'required|min:1'
        ]);

        if ($validator->fails()) {
            return response()->json(['mess' => $validator->errors()->all(), 'success' => false], 400);
        }

        $userUploadedImg = $this->loadImages(json_decode($request->images), null, 600);

        if (sizeof($userUploadedImg) > 1) {
            $data["success"] = false;
            $data["mess"] = "Допустимое количество изображений - не более одного";
            return response()->json($data, 400);
        }

        $articleId = Article::create([
            'name' => $request->name,
            'content' => $request->input('content'),
            'image' => $userUploadedImg[0],
            'image_caption' => $request->input('image_caption', null),
            'category_id' => $request->category_id,
            'is_promotion' => $request->is_promotion
        ])->id;

        if ($request->products) {
            $articleProducts = $request->products;

            foreach ($articleProducts as $articleProduct) {
                ProductsArticle::create([
                    "article_id" => $articleId,
                    "product_id" => $articleProduct
                ]);
            }
        }

        return redirect()->route("admin.articles.index")->withSuccess("Статья успешно добавлена");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $article = Article::whereId($id)->with('productsArticles')->first();
        $categoriesInfo = Category::all();
        $chosenProducts = ProductsArticle::where('article_id', $id)->get()->pluck('product_id')->toArray();

        return $this->renderOutputAdmin("admin.articles.form", [
            "article" => $article,
            'categories' => $categoriesInfo,
            'chosen_products' => $chosenProducts,
            "route" => route("admin.articles.update", ["id_article" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse|void
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            $data["success"] = false;
            $data["mess"] = $validator->errors()->first();
            return response()->json($data, 400);
        }

        $images = json_decode($request->images);

        $userUploadedImg = $this->loadImages($images, 800, null);

        if (sizeof($userUploadedImg) > 1) {
            $data["success"] = false;
            $data["mess"] = "Допустимое количество изображений - не более одного";
            return response()->json($data, 400);
        }

        Article::whereId($id)->update(
            [
                'name' => $request->name,
                'content' => $request->input('content'),
                'image_caption' => $request->image_caption,
                'category_id' => $request->category_id,
                'is_promotion' => $request->is_promotion,
                'image' => $userUploadedImg[0]
            ]
        );

        ProductsArticle::where("article_id", $id)->delete();

        if ($request->products) {
            $articleProducts = $request->products;

            foreach ($articleProducts as $articleProduct) {
                ProductsArticle::create([
                    "article_id" => $id,
                    "product_id" => $articleProduct
                ]);
            }
        }

        return response()->json(['mess' => "Статья успешно изменена", 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        Article::whereId($id)->delete();
        return redirect()->route("admin.articles.index")->withSuccess("Статья успешно удалена");
    }
}

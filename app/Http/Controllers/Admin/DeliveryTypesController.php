<?php

namespace App\Http\Controllers\Admin;

use App\DeliveryType;
use App\Http\Controllers\SiteController;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class DeliveryTypesController
 *
 * @package App\Http\Controllers\Admin
 */
class DeliveryTypesController extends SiteController
{
    private $typesInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $deliveryTypes = DeliveryType::orderBy("name")->paginate($this->typesInPage);
        return $this->renderOutputAdmin("admin.delivery_types.list", [
            "delivery_types" => $deliveryTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return $this->renderOutputAdmin("admin.delivery_types.form", [
            "route" => route("admin.delivery_types.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        DeliveryType::create([
            'name' => $request->name
        ]);

        return redirect()->route("admin.delivery_types.index")->withSuccess("Способ доставки успешно добавлен");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $deliveryTypes = DeliveryType::find($id);
        return $this->renderOutputAdmin("admin.delivery_types.form", [
            "delivery_type" => $deliveryTypes,
            "route" => route("admin.delivery_types.update", ["id_delivery_type" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        DeliveryType::whereId($id)->update([
            'name' => $request->name
        ]);

        return redirect()->route("admin.delivery_types.index")->withSuccess("Способ доставки успешно изменен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        DeliveryType::whereId($id)->delete();
        return redirect()->route("admin.delivery_types.index")->withSuccess("Способ доставки успешно удален");
    }
}

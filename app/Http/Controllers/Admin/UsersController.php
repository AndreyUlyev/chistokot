<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\SiteController;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Admin
 */
class UsersController extends SiteController
{
    private $usersInPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $users = User::paginate($this->usersInPage);
//        return $this->renderOutputAdmin('admin.users.list', [
//            'users' => $users
//        ]);
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return $this->renderOutputAdmin("admin.users.form", [
            "route" => route("admin.users.store"),
            "update" => false
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route("admin.users.index")->withSuccess("Пользователь успешно добавлен");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $user = User::whereId($id)->first();

        return $this->renderOutputAdmin("admin.users.form", [
            "user" => $user,
            "route" => route("admin.users.update", ["id_user" => $id]),
            "password_route" => route("admin.users.update_password", ["id_user" => $id]),
            "update" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        User::whereId($id)->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route("admin.users.index")->withSuccess("Пользователь успешно изменен");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function updatePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        User::whereId($id)->update([
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route("admin.users.index")->withSuccess("Пользователь успешно изменен");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param User $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        User::whereId($id)->delete();
        return redirect()->route("admin.users.index")->withSuccess("Пользователь успешно удален");
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Http\Controllers\SiteController;
use App\Like;
use App\Order;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class ChartsController
 *
 * @package App\Http\Controllers\Admin
 */
class ChartsController extends SiteController
{
    /**
     * Get average orders sum.
     *
     * @return int
     */
    public function avgOrdersSum()
    {
        $sum = Order::avg('total_price');
        return (int)$sum;
    }

    /**
     * Get orders for register and nor register users.
     *
     * @return Collection
     */
    public function ordersForRegisteredOrNot()
    {
        $orders = Order::all()->pluck('user_id');
        return $orders;
    }

    /**
     * Get statistic by month
     *
     * @return array
     */
    public function statisticsPerMonth()
    {
        $data = $this->ordersPerMonth();
        array_push($data, $this->usersPerMonth());
        array_push($data, $this->likesPerMonth());

        $statistics = array(
            $this->ordersPerMonth(),
            $this->usersPerMonth(),
            $this->likesPerMonth()
        );

        return $statistics;
    }

    /**
     * Get orders by month.
     *
     * @return array
     */
    private function ordersPerMonth()
    {
        $data = Order::select([
            DB::raw('DATE(created_at) AS date'),
            DB::raw('COUNT(id) AS count'),
        ])
            ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get()
            ->toArray();

        $orders = [
            'name' => 'orders',
            'data' => $data,
            'unit' => 'Заказов/мес.',
            "type" => "line",
            "valueDecimals" => 0
        ];

        return $orders;
    }

    /**
     * Get new users by month.
     *
     * @return array
     */
    private function usersPerMonth()
    {
        $data = User::selectRaw('COUNT(*) as count, MONTH(created_at) month')
            ->groupBy('month')
            ->get();

        $users = array(
            'name' => 'users',
            'data' => $data,
            'unit' => 'Пользователей/мес.',
            "type" => "line",
            "valueDecimals" => 0
        );

        return $users;
    }

    /**
     * Get likes by month
     *
     * @return array
     */
    private function likesPerMonth()
    {
        $data = Like::selectRaw('COUNT(*) as count, MONTH(created_at) month')
            ->groupBy('month')
            ->get();

        $likes = [
            'name' => 'likes',
            'data' => $data,
            'unit' => 'Лайков/мес.',
            "type" => "line",
            "valueDecimals" => 0
        ];

        return $likes;
    }
}

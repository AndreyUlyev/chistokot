<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\SiteController;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Validator;

/**
 * Class CommentsController
 *
 * @package App\Http\Controllers\Admin
 */
class CommentsController extends SiteController
{
    private $comments_in_page = 1;

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $approvedComments = Comment::where('approved', true)
            ->paginate($this->comments_in_page, ['*'], 'approved_comments');
        $notApprovedComments = Comment::where('approved', false)
            ->paginate($this->comments_in_page, ['*'], 'not_approved_comments');
        return $this->renderOutputAdmin("admin.comments.list", [
            "approved_comments" => $approvedComments,
            'not_approved_comments' => $notApprovedComments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        Comment::whereId($id)->update([
            'approved' => true
        ]);

        return redirect()->route("admin.comments.index")->withSuccess("Комментарий успешно добавлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        Comment::whereId($id)->delete();
        return redirect()->route("admin.comments.index")->withSuccess("Комментарий успешно удален");
    }
}

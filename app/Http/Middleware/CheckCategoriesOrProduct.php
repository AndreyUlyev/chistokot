<?php

namespace App\Http\Middleware;

use App\Category;
use App\Http\Controllers\ProductsController;
use Closure;
use Illuminate\Http\Request;

/**
 * Class CheckCategoriesOrProduct
 *
 * @package App\Http\Middleware
 */
class CheckCategoriesOrProduct
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $category = Category::where("slug", $request->slug)->first();
        if ($category == null) {
            $productController = new ProductsController();
            return ($productController);
        } else {
            return $next($request);
        }
    }
}

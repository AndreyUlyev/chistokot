<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModification extends Model
{
    protected $guarded = [];
}

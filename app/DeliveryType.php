<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DeliveryType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DeliveryType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DeliveryType extends Model
{
    protected $guarded = [];
}

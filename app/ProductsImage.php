<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProductsImage
 *
 * @property int $id
 * @property string $link
 * @property int $id_product
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage create($value)
 * @mixin \Eloquent
 * @property int $product_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsImage whereProductId($value)
 */
class ProductsImage extends Model
{
    protected $guarded = [];
    protected $table = "products_images";
}

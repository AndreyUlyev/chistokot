<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Article
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article orderBy(string $string)
 * @mixin \Eloquent
 * @property string $content
 * @property string $image
 * @property string|null $image_caption
 * @property int $category_id
 * @property-read \App\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereImageCaption($value)
 * @property int $is_promotion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereIsPromotion($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProductsArticle[] $productsArticles
 * @property-read int|null $products_articles_count
 */

class Article extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get list images of product.
     *
     * @return HasMany
     */
    public function productsArticles()
    {
        return $this->hasMany(ProductsArticle::class, 'product_id');
    }
}

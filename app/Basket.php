<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Basket
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Brand where($value, $value)
 * @mixin Eloquent
 * @property int $product_id
 * @property int $quantity
 * @property int $user_id
 * @property-read Collection|\App\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereUserId($value)
 */

class Basket extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}

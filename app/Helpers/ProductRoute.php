<?php

    function route_product($product)
    {
        if ($product->color && $product->size) {
            return  route("products.page", [
                "slug" => $product->slug,
                "size" => $product->slug_size,
                "color" => $product->slug_color
            ]);
        } else if ($product->size && $product->size != "default"){
            return  route("products.page", [
                "slug" => $product->slug,
                "size" => $product->slug_size
            ]);
        } else if ($product->color){
            return  route("products.page", [
                "slug" => $product->slug,
                "size" => "default",
                "color" => $product->slug_color
            ]);
        } else {
            return  route("products.page", [
                "slug" => $product->slug
            ]);
        }
    }

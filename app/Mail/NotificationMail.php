<?php

namespace App\Mail;

use App\Order;
use App\Promocode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NotificationMail
 *
 * @package App\Mail
 */
class NotificationMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    protected $mailData;

    /**
     * Create a new message instance.
     *
     * @param $mailData
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->mailData["type"] == "order") {
            $this->successfulOrder();
        } elseif ($this->mailData["type"] == "promocode") {
            $this->sendPromocode();
        }
    }

    /**
     * Send details order.
     *
     * @return NotificationMail
     */
    private function successfulOrder()
    {
        return $this->markdown('emails.orders.shipped', [
            "order" => $this->mailData['order'],
            "user" => $this->mailData['user'],
        ])->subject("Заказ успешно оформлен! \"" . $this->mailData['order']['id'] . "\"");
    }

    /**
     * Send promocode.
     *
     * @return NotificationMail
     */
    private function sendPromocode()
    {
        $promocode = Promocode::where('name', '=', 'PMCD1')->first();
        return $this->markdown('emails.promocodes.send', [
            'promocode' => $promocode
        ])->subject("Ура! Вот ваш промокод на скидку");
    }
}

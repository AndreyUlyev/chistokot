<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewOrder
 * 
 * @package App\Mail
 */
class NewOrder extends Mailable
{
    use Queueable;
    use SerializesModels;

    protected $mailData;

    /**
     * Create a new message instance.
     *
     * @param $mailData
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.new_order', [
            "order" => $this->mailData
        ])->subject("Новый заказ!");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * App\Category
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category get()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category update($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category orderBy($value)
 * @mixin \Eloquent
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category wherePosition($value)
 * @property string|null $description
 * @property int $is_on_main
 * @property string $slug
 * @property string $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIsOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @property string|null $moy_sklad_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereMoySkladId($value)
 */
class Category extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}

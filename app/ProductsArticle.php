<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProductsArticle
 *
 * @property int $id
 * @property int $article_id
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProductsArticle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductsArticle extends Model
{
    protected $guarded = [];
    protected $table = "products_articles";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Promocode
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode paginate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode find($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode get()
 * @mixin \Eloquent
 * @property float $discount
 * @property int|null $category_id
 * @property int|null $product_id
 * @property int|null $brand_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promocode whereProductId($value)
 */

class Promocode extends Model
{
    //
    protected $guarded = [];
}

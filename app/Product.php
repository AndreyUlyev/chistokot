<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * App\Product
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property float $price
 * @property float|null $price_discount
 * @property int $quantity
 * @property int $category_id
 * @property int $brand_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePriceDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product create($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product where($value, $value)
 * @method static paginate(int $productsInPage)
 * @mixin \Eloquent
 * @property int $is_popular
 * @property string $slug
 * @property string|null $composition
 * @property string|null $about_delivery
 * @property string|null $guarantees
 * @property string|null $where_apply
 * @property string|null $maintenance
 * @property string|null $country
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProductsImage[] $productImages
 * @property-read int|null $product_images_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereAboutDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereGuarantees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereIsPopular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMaintenance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereWhereApply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product filterOrderBy($order)
 * @property string|null $moy_sklad_id
 * @property string|null $meta_keywords
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMoySkladId($value)
 */
class Product extends Model
{
    protected $guarded = [];

    /**
     * Get categories of product.
     *
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }


    public function modifications()
    {
        return $this->belongsToMany(
            Modification::class,
            'product_modifications',
            'product_id',
            'modification_id'
        );
    }

    /**
     * Get list images of product.
     *
     * @return HasMany
     */
    public function productImages()
    {
        return $this->hasMany(ProductsImage::class, 'product_id');
    }

    /**
     * Filters products by find.
     *
     * @param $query
     * @param $order
     * @return mixed
     */
    public function scopeFilterOrderBy($query, $order)
    {
        switch ($order) {
            case "byPopularity":
                return $query->orderBy(
                    Comment::selectRaw('count(id)')
                        ->whereColumn('product_id', 'products.id')
                        ->limit(1),
                    "desc"
                );
            case "byPriceAsc":
                return $query->orderByRaw("LEAST(price, IFNULL(price_discount, price)) ASC");
            case "byPriceDesc":
                return $query->orderByRaw("LEAST(price, IFNULL(price_discount, price)) DESC");
            case "byNovelty":
                return $query->orderBy("created_at");
        }
    }
}

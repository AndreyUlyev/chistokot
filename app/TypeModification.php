<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeModification extends Model
{
    protected $guarded = [];
    protected $table = "types_modifications";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Like
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $user_ip
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereUserIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like create($value)
 * @mixin \Eloquent
 * @property int $product_id
 * @property-read \App\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Like whereProductId($value)
 */
class Like extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->hasOne(Product::class, "id", "product_id");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;

class Modification extends Model
{
    protected $guarded = [];

    public function modifications()
    {
        return $this->hasMany(Modification::class, 'modification_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'product_modifications',
            'modification_id',
            'product_id'
        );
    }

    public function color()
    {
        return $this->belongsTo(Color::class, "color_id", "id");
    }

    public function type()
    {
        return $this->belongsTo(TypeModification::class, "type_modification_id", "id");
    }
}

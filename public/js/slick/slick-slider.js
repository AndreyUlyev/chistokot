$(document).on('ready', function() {
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        focusOnSelect: true,
        arrows: true,
        draggable: true,
        prevArrow: '<div class="prevProd"><img src="../images/long-arrow-left.png"></div>',
        nextArrow: '<div class="nextProd"><img src="../images/long-arrow-right.png"></div>'
    });
});
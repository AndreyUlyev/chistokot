
var upload_images = {};
var index = 0;

/**
 * Построение DOM для кропера
 *
 * @param img
 * @returns {*|jQuery.fn.init|jQuery|HTMLElement}
 */
function create_cropper(img){
    var croper_html = $('<div/>', {
        'class':  'cropper',
        'id': 'custom_cropper'
    });
    var cropper_img_wrap = $('<div/>', {
        'class':  'cropper-img'
    }).prependTo(croper_html);
    var cropper_img = $('<img/>', {
        'class':  'img-responsive',
        'id': 'cropper-img',
        'src': img
    }).prependTo(cropper_img_wrap);


    var cropper_toolbar = $('<div/>', {
        'class':  'cropper-toolbar'
    }).prependTo(croper_html);

    $('<button/>', {
        'class':  'btn btn-link link-muted hidden-xs',
        'data-option': 'move',
        'data-toggle': 'Повернуть на 90° по часовой',
        'data-animation': 'false',
        'data-method': 'setDragMode',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-arrows icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted hidden-xs',
        'data-option': 'crop',
        'data-method': 'setDragMode',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-crop icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-option': '0.1',
        'data-method': 'zoom',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-search-plus icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-option': '-0.1',
        'data-method': 'zoom',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-search-minus icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-option': '-45',
        'data-method': 'rotate',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-rotate-left icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-option': '45',
        'data-method': 'rotate',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-rotate-right icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-option': '-1',
        'data-method': 'scaleX',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-arrows-h icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-option': '-1',
        'data-method': 'scaleY',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-arrows-v icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted',
        'data-method': 'getCroppedCanvas',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-check icon-lg'
    })).appendTo(cropper_toolbar);

    $('<button/>', {
        'class':  'btn btn-link link-muted destroy-btn',
        'type': 'button'
    }).append($('<span/>', {
        'class':  'icon icon-times '
    })).appendTo(cropper_toolbar);
    return croper_html;
}


/**
 * Инициализация кропера
 */
function start_cropper(aspectRatio) {
    'use strict';

    var console = window.console || {
        log: function () {
        }
    };
    var URL = window.URL || window.webkitURL;
    var $image = $('#cropper-img');
    var uploadedImageType = 'image/jpeg';
    var uploadedImageURL;
    var options = {
        aspectRatio: aspectRatio,
        viewMode: 3,
        minWidth: 300,
        responsive: true,
    };

    console.log($image);
    // Cropper
    $image.cropper(options);

    // Methods
    $('.cropper-toolbar').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var cropper = $image.data('cropper');
        var cropped;
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if (cropper && data.method) {
            data = $.extend({}, data); // Clone a new one
            if (typeof data.target !== 'undefined') {
                $target = $(data.target);
                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        console.log(e.message);
                    }
                }
            }

            cropped = cropper.cropped;

            switch (data.method) {
                case 'rotate':
                    if (cropped && options.viewMode > 0) {
                        $image.cropper('clear');
                    }

                    break;

                case 'crop':
                    if (uploadedImageType === 'image/jpeg') {
                        if (!data.option) {
                            data.option = {};
                        }
                        data.option.fillColor = '#fff';
                    }
                    break;
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            switch (data.method) {
                case 'rotate':
                    if (cropped && options.viewMode > 0) {
                        $image.cropper('crop');
                    }
                    break;

                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;

                case 'destroy':
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                        uploadedImageURL = '';
                        $image.attr('src', originalImageURL);
                    }
                    break;

                case 'getCroppedCanvas':
                    if (result) {
                        var base64 = result.toDataURL(uploadedImageType);
                        show_cropped_image(base64, index);
                        upload_images[index] = base64;
                        index += 1;
                        console.log(upload_images);
                        $.hieraModal('hide');
                    }
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    console.log(e.message);
                }
            }

        }
    });
}


function show_cropped_image(src, index){
    var wraper = $('<div/>', {
        'class': 'listImg'
    }).prependTo("#images_list");

    $('<img/>', {
        'src': src,
        'width': '130px'
    }).prependTo(wraper);

    $('<button/>', {
        'html': 'Удалить',
        'data-index': index
    }).prependTo(wraper);
}

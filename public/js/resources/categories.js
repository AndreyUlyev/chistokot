$(window).load(function () {
    $(".listImg").each(function () {
        upload_images[index] = $(this).children("button").data("id");
        $(this).children("button").attr("data-index", index);
        index += 1;
    });
});

$(document).on('click', '.listImg button', function (e) {
    e.preventDefault();
    Swal.fire({
        title: 'Удаление изображения категории',
        text: 'Вы действительно хотите удалить изображение для категории?',
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#0288d1',
        confirmButtonText: 'Да, удалить!',
    }).then((result) => {
        if (result.value) {
            $("#images_label :input").prop("disabled", false);
            $("#images_label").removeAttr("disabled");

            var index = $(this).data("index");
            delete upload_images[index];
            $(this).parent().remove();
        }
    });
});

$(document).on('change', '.form_element input[type=file]', function (e) {
    var files = $(".create_category").find("#files")[0].files;
    $.hieraModal({
        "content": create_cropper(URL.createObjectURL(files[0]))
    });
    start_cropper(1 / 1);
});

$(document).on('submit', 'form.create_category', function (e) {
    e.preventDefault();
    var data = {
        "name": $("#name").val(),
        "description": $("#description").val(),
        "is_on_main": $("#is_on_main").is(':checked') ? 1 : 0,
        "images": JSON.stringify(upload_images),
        "category_sklad_id": $("#-").val()
    };

    var update = $("#update").val();
    var aj_method = "POST";
    var aj_route = route("admin.categories.store");
    var category_id = $("#id_category").val();

    if (update) {
        aj_method = "PUT";
        aj_route = route("admin.categories.update", {"id": category_id});
    }

    $.ajax({
        type: aj_method,
        url: aj_route,
        data: data,
        success: function (data) {
            Swal.fire(
                'Успешно!',
                data["mess"],
                'success'
            ).then((result) => {
                location = route("admin.categories.index")
            });
        },
        error: function (error) {
            console.log(error.responseJSON["mess"]);
            Swal.fire(
                'Ошибка!',
                error.responseJSON["mess"],
                'error'
            );
        }
    });
});


$(document).on('click', "span.icon-check", function (e) {
    $("#images_label :input").prop("disabled", true);
    $("#images_label").attr("disabled", true);
});

$(document).ready(function() {
   $('.js-example-basic-multiple').select2();
});

$(window).load(function() {
    $(".listImg").each(function() {
        upload_images[index] = $(this).children("button").data("id");
        $(this).children("button").attr("data-index", index);
        index += 1;
    });
});

$(document).on('click','#adminCategoriesBtn', function () {
    $.hieraModal({
        id: 'categories',
        title: 'Категории',
        content: showPreloader()
    });
    $.ajax({
        type: "GET",
        url: route('admin.categories.index'),
        success: function (categories_list_item) {
            setTimeout(function () {
                $("#modal_categories .modalContent").html(categories_list_item);
                $("#modal_categories .modalContent").sortable({
                    axis: "y",
                    handle: ".categories_item_btn_drag",
                    containment: "parent",
                    placeholder: "modal_categories_item_placeholder",
                });
                $(".modal_categories_item").css({"box-shadow":"black 0px 0px 4px"});
                $("#modal_categories .modalContent").disableSelection();
            }, 600);
        }
    });
});

$(document).on('click','.listImg button',function(e){
    e.preventDefault();
    Swal.fire({
        title: 'Удаление изображения статьи',
        text: 'Вы действительно хотите удалить изображение для статьи?',
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#0288d1',
        confirmButtonText: 'Да, удалить!',
    }).then((result) => {
        if (result.value) {
            $("#images_label :input").prop("disabled", false);
            $("#images_label").removeAttr("disabled");

            var index = $(this).data("index");
            delete upload_images[index];
            $(this).parent().remove();
        }
    });
});

$(document).on('change','.form_element input[type=file]',function(e){
    var files = $(".create_article").find("#files")[0].files;
    $.hieraModal({
        "content": create_cropper(URL.createObjectURL(files[0]))
    });
    start_cropper(16 / 9);
});

$(document).on('submit','form.create_article',function(e){
    e.preventDefault();
    var products = $('.js-example-basic-multiple').select2().val();

    var data = {
        "name": $(".create_article").find("#name").val(),
        "content": $(".create_article").find("#content").val(),
        "image_caption": $(".create_article").find("#image_caption").val(),
        "category_id": $(".create_article").find("#category :selected").val(),
        "is_promotion": $(".create_article").find("#is_promotion").is(':checked') ? 1 : 0,
        "products": products,
    "images": JSON.stringify(upload_images)
    };

    var update = $("#update").val();
    var aj_method = "POST";
    var aj_route = route("admin.articles.store");
    var article_id = $("#id_article").val();

    if (update){
        aj_method = "PUT";
        aj_route = route("admin.articles.update", {"id": article_id });
    }

    $.ajax({
        type: aj_method,
        url: aj_route,
        data: data,
        success: function(data){
            Swal.fire(
                'Успешно!',
                data["mess"],
                'success'
            ).then((result) => {
                location = route("admin.articles.index")
            });
        },
        error: function (error) {
            console.log( error.responseJSON["mess"]);
            Swal.fire(
                'Ошибка!',
                error.responseJSON["mess"].join("<br/>"),
                'error'
            );
        }
    });
});

$(document).on('click', "span.icon-check", function (e) {
    $("#images_label :input").prop("disabled", true);
    $("#images_label").attr("disabled", true);
});

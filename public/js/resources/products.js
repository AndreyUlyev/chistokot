ClassicEditor.create(document.querySelector('#description'), {
    toolbar: ['undo', 'redo', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
})
    .catch(error => {
        console.error(error);
    });

$(window).load(function () {
    $(".listImg").each(function () {
        upload_images[index] = $(this).children("button").data("id");
        $(this).children("button").attr("data-index", index);
        index += 1;
    });
});

$(document).on('click', '#adminCategoriesBtn', function () {
    $.hieraModal({
        id: 'categories',
        title: 'Категории',
        content: showPreloader()
    });
    $.ajax({
        type: "GET",
        url: route('categories.index'),
        success: function (categories_list_item) {
            setTimeout(function () {
                $("#modal_categories .modalContent").html(categories_list_item);
                $("#modal_categories .modalContent").sortable({
                    axis: "y",
                    handle: ".categories_item_btn_drag",
                    containment: "parent",
                    placeholder: "modal_categories_item_placeholder",
                });
                $(".modal_categories_item").css({"box-shadow": "black 0px 0px 4px"});
                $("#modal_categories .modalContent").disableSelection();
            }, 600);
        }
    });
});

$(document).on('click', '.listImg button', function (e) {
    e.preventDefault();
    Swal.fire({
        title: 'Удаление изображения товара',
        text: 'Вы действительно хотите удалить изображение товара?',
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#0288d1',
        confirmButtonText: 'Да, удалить!',
    }).then((result) => {
        if (result.value) {
            var index = $(this).data("index");
            delete upload_images[index];
            $(this).parent().remove();
        }
    });
});

$(document).on('change', '.form_element input[type=file]', function (e) {
    var files = $(".create_product").find("#files")[0].files;
    $.hieraModal({
        "content": create_cropper(URL.createObjectURL(files[0]))
    });
    start_cropper(1 / 1);
});

$(document).on('submit', 'form.create_product', function (e) {
    e.preventDefault();
    var data = {
        "name": $(".create_product").find("#name").val(),
        "description": $(".create_product").find("#description").val(),
        "price": $(".create_product").find("#price").val(),
        "price_discount": $(".create_product").find("#price_discount").val() || 0,
        "quantity": $(".create_product").find("#quantity").val(),
        "is_popular": $(".create_product").find("#is_popular").is(':checked') ? 1 : 0,
        "category_id": $(".create_product").find("#category :selected").val(),
        "brand_id": $(".create_product").find("#brand :selected").val(),
        "composition": $("#composition").val(),
        "about_delivery": $("#about_delivery").val(),
        "guarantees": $("#guarantees").val(),
        "where_apply": $("#where_apply").val(),
        "maintenance": $("#maintenance").val(),
        "country": $(".create_product").find("#country :selected").val(),
        "images": JSON.stringify(upload_images),
        "product_sklad_id": $(".create_product").find("#product_sklad_id").val(),
        "meta_description": $(".create_product").find("#meta_description").val(),
        "meta_title": $(".create_product").find("#meta_title").val(),
        "meta_keywords": $(".create_product").find("#meta_keywords").val(),
        "parent_product_id": $(".create_product").find("#parent_product_id").val(),
        "mod": $(".create_product").find("#mod").val(),
        "color": $(".create_product").find("#products_colors :selected").val(),
    };

    var update = $("#update").val();
    var aj_method = "POST";
    var aj_route = route("admin.products.store");
    var product_id = $("#id_product").val();

    if (update) {
        aj_method = "PUT";
        aj_route = route("admin.products.update", {"id_product": product_id});
    }

    $.ajax({
        type: aj_method,
        url: aj_route,
        data: data,
        success: function (data) {
            Swal.fire(
                'Успешно!',
                data["mess"],
                'success'
            ).then((result) => {
                location = route("admin.products.index")
            });
        },
        error: function (error) {
            console.log(error.responseJSON["mess"]);
            Swal.fire(
                'Ошибка!',
                error.responseJSON["mess"].join("<br/>"),
                'error'
            );
        }
    });
});

$("#products_colors").select2({
    tags: true
});

$(document).on('click', '.del_product', function (e) {
    e.preventDefault();
    let id = $(this).attr("data-id");
    Swal.fire({
        title: 'Удаление товар',
        text: 'Вы действительно хотите удалить товар?',
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#0288d1',
        confirmButtonText: 'Да, удалить!',
    }).then((result) => {
        if (result.value) {
            var route_e = route("admin.products.destroy", {"id_product": id});
            $.ajax({
                url: route_e,
                type: 'DELETE',
                success: function (result) {
                    location = route("admin.products.index")
                }
            });
        }
    });
});


$(window).load(function() {
    $(".listImg").each(function() {
        upload_images[index] = $(this).children("button").data("id");
        $(this).children("button").attr("data-index", index);
        index += 1;
    });
});

$(document).on('click','#adminCategoriesBtn', function () {
    $.hieraModal({
        id: 'categories',
        title: 'Категории',
        content: showPreloader()
    });
    $.ajax({
        type: "GET",
        url: route('admin.categories.index'),
        success: function (categories_list_item) {
            setTimeout(function () {
                $("#modal_categories .modalContent").html(categories_list_item);
                $("#modal_categories .modalContent").sortable({
                    axis: "y",
                    handle: ".categories_item_btn_drag",
                    containment: "parent",
                    placeholder: "modal_categories_item_placeholder",
                });
                $(".modal_categories_item").css({"box-shadow":"black 0px 0px 4px"});
                $("#modal_categories .modalContent").disableSelection();
            }, 600);
        }
    });
});

$(document).on('click','.listImg button',function(e){
    e.preventDefault();
    Swal.fire({
        title: 'Удаление изображения слайдера',
        text: 'Вы действительно хотите удалить изображение для слайдера?',
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#0288d1',
        confirmButtonText: 'Да, удалить!',
    }).then((result) => {
        if (result.value) {
            $("#images_label :input").prop("disabled", false);
            $("#images_label").removeAttr("disabled");

            var index = $(this).data("index");
            delete upload_images[index];
            $(this).parent().remove();
        }
    });
});

$(document).on('change','.form_element input[type=file]',function(e){
    var files = $(".create_slider").find("#files")[0].files;
    $.hieraModal({
        "content": create_cropper(URL.createObjectURL(files[0]))
    });
    start_cropper(2 / 1);
});

$(document).on('submit','form.create_slider',function(e){
    e.preventDefault();
    var data = {
        "link": $(".create_slider").find("#link").val(),
        "description": $(".create_slider").find("#description").val(),
        "images": JSON.stringify(upload_images),
        "name": $(".create_slider").find("#name").val()
    };

    var update = $("#update").val();
    var aj_method = "POST";
    var aj_route = route("admin.slider.store");
    var slider_id = $("#id_slider").val();

    if (update){
        aj_method = "PUT";
        aj_route = route("admin.slider.update", {"id": slider_id });
    }

    $.ajax({
        type: aj_method,
        url: aj_route,
        data: data,
        success: function(data){
            Swal.fire(
                'Успешно!',
                data["mess"],
                'success'
            ).then((result) => {
                location = route("admin.slider.index")
            });
        },
        error: function (error) {
            console.log( error.responseJSON["mess"]);
            Swal.fire(
                'Ошибка!',
                error.responseJSON["mess"].join("<br/>"),
                'error'
            );
        }
    });
});

$(document).on('click', "span.icon-check", function (e) {
    $("#images_label :input").prop("disabled", true);
    $("#images_label").attr("disabled", true);
});

(function( $ ){
    var methods = {
        settings: false,
        init : function( options ) {
            methods.settings = $.extend( {
                'id' : '',
                'title' : '',
                'background-content' : '',
                'animated': 'pulse'
            }, options);
            methods.show();
        },
        show : function() {
            $(".hieraModal").first().css("display", "none");
            if ($(".hieraModal").length == 0){
                var animated_class = 'animated animated ' + this.settings['animated'];
            }
            var div_modal = $('<div/>', {
                class:  'modal hieraModal ' + animated_class,
                css: {
                    'display': 'block',
                    'pointer-events': 'all'
                }
            }).prependTo('body');
            var modal_dialog = $('<div/>', {
                class:  'modalWrapper',
                id: 'modal_' + this.settings['id']
            }).appendTo(div_modal);
            var modal_content = $('<div/>', {
                class:  'modalInner'
            }).appendTo(modal_dialog);
            var modal_header = $('<div/>', {
                class:  'modalHeader'
            }).appendTo(modal_content);
            var modal_title = $('<div/>', {
                class:  'modalTitle',
                text: this.settings['title']
            }).appendTo(modal_header);
            var a = $('<a/>', {
                class:  'closeBtn close_modal',
                text: '+',
                href: '',
                title: 'close',
                click: this.hide
            }).appendTo(modal_header);

            var modal_body = $('<div/>', {
                class:  'modalContent scrollbar-inner',
                html: this.settings['content']
            }).appendTo(modal_content);
            $("body").css("overflow", "hidden");
        },
        hide : function(e) {
            if (e){
                e.preventDefault();
            }
            $(".hieraModal").first().remove();
            if ($(".hieraModal").length){
                $(".hieraModal").first().css("display", "block");
            } else {
                $("body").css("overflow", "auto");
            }
        }
    };

    $.hieraModal = function( method ) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.tooltip' );
        }
    };
})( jQuery );
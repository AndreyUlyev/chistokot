$(document).ready(function () {
    // при наведении на слайдер три ссылки НАЧАЛО
    $('.category__good-block').mouseover(function() {
        $(this).children().children('#ItemButtons').css({"display":"flex"})
    });
    $('.category__good-block').mouseleave(function() {
        $(this).children().children('#ItemButtons').css({"display":"none"})
    });
    // при наведении на слайдер три ссылки КОНЕЦ




    // for modal start
    $(".modalbox").fancybox({
        "maxWidth" : 600,
        "padding" : 0,
        //"margin" : 0 auto,
    });
    $(".one_click").fancybox({
        "maxWidth" : 700,
        "padding": 30,
        "height": 500,

        //"margin" : 0 auto,
    });
    $(".modalboxCallBack").fancybox({
        "width" : 500,
        "padding" : 20,
        "margin": 0,
    });
    $(".modalboxIMG").fancybox({
        "padding" : 63,
    });

    $("#f_login").submit(function(){ return false; });
    $("#do_login").on("click", function(){
        // тут дальнейшие действия по обработке формы
        // закрываем окно, как правило делать это нужно после обработки данных
        // $("#f_login").fadeOut("fast", function(){
        //     $(this).before("<p><strong>Ваше сообщение отправлено!</strong></p>");
        //     setTimeout("$.fancybox.close()", 1000);
        // });
    });
    $("#f_registration").submit(function(){ return false; });
    $("#do_registration").on("click", function(){
        $.fancybox.close();
    });

    $("#f_conditions").submit(function(){ return false; });
    $("#do_conditions").on("click", function(){
        $.fancybox.close();
    });

    $("#f_callback").submit(function(){ return false; });
    $("#do_callback").on("click", function(){
        $.fancybox.close();
    });

    $("#f_img-prod").submit(function(){ return false; });
    $("#f_img-prod").on("click", function(){
        $.fancybox.close();
    });
    // for modal end
});




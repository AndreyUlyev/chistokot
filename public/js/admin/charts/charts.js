document.addEventListener('DOMContentLoaded', function () {
    $.ajax({
        type: "GET",
        async: false,
        url: route('admin.charts.avg_orders_sum'),
        success: function (results) {
            $('#result').text(results + '           RUB');
        }
    });

    let registered = 0;
    let unregistered = 0;
    $.ajax({
        type:"GET",
        async: false,
        url: route('admin.charts.orders_for_registered_or_not'),
        success : function(results) {
            registered =  results.filter(i => i ).length;
            unregistered = results.filter(i => i == null).length;
            console.log(registered, unregistered);
        }
    });

    var myChart = Highcharts.chart('container1', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Кто чаще покупает?'
        },
        xAxis: {
            categories: ['За все время совершено заказов']
        },
        yAxis: {
            title: {
                text: 'Кто чаще покупает?'
            }
        },
        series: [{
            name: 'Зарегистрированные',
            data: [registered]
        }, {
            name: 'Незарегистрированные',
            data: [unregistered]
        }]
    });
});

function retrieve_data(data) {
    var array = [0];

    if (!data) {
        array = 0;
    }
    else{
        data.forEach(element => array.push(element['count']));
    }
    console.log(array);
    return array
}

function retrieve_period_from_all_dataset(data) {
    var array = [];

    if (!data) {
        array = [0];
    }
    else{
        // data.forEach(element => array.push(element['date'].getMonth()));
        data.forEach(element => array.push(new Date(element['date']).getDay() + 1));

    }
    console.log('lol' + array);
    return array
}
//
// document.addEventListener('DOMContentLoaded', function () {//2019-12-02
//     let data = 0;
//
//     $.ajax({
//         type: "GET",
//         async: false,
//         url: route('admin.charts.statistics_per_month'),
//         success: function (results) {
//             data = results;
//         }
//     });
//
//     Highcharts.chart('container2', {
//         chart: {
//             type: 'areaspline'
//         },
//         title: {
//             text: 'Статистика за месяц'
//         },
//         legend: {
//             layout: 'vertical',
//             align: 'left',
//             verticalAlign: 'top',
//             x: 150,
//             y: 100,
//             floating: true,
//             borderWidth: 1,
//             backgroundColor:
//                 Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
//         },
//         xAxis: {
//             categories: [
//                 'January',
//                 'February12345',
//                 'March',
//                 'April',
//                 'May',
//                 'June',
//                 'July',
//                 'August',
//                 'September',
//                 'November',
//                 'December',
//             ],
//             plotBands: [{ // visualize the weekend
//                 from: 4.5,
//                 to: 6.5,
//                 color: 'rgba(68, 170, 213, .2)'
//             }]
//         },
//         yAxis: {
//             title: {
//                 text: 'Количество'
//             }
//         },
//         tooltip: {
//             shared: true,
//             valueSuffix: ' штук'
//         },
//         credits: {
//             enabled: false
//         },
//         plotOptions: {
//             areaspline: {
//                 fillOpacity: 0.5
//             }
//         },
//         series: [{
//             name: 'Заказов',
//             data: retrieve_data(data[0]['data'])
//         }, {
//             name: 'Лайков',
//             data: retrieve_data(data[1]['data'])
//         },
//          {
//             name: 'Новых пользователей',
//             data: retrieve_data(data[2]['data'])
//         }]
//     });
// });
//
//

$(window).on('load', function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$(document).on('click', '.delete_btn', function (e) {
    e.preventDefault();
    Swal.fire({
        title: 'Вы уверены?',
        text: "Действие невозможно отменить!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да, удалить!',
        cancelButtonText: 'Отмена'
    }).then((result) => {
        if (result.value) {
            $(this).find("form").submit();
        }
    })
});

$(document).on('click', "#allow_category", function (e) {
    var categorySelect = document.getElementById("category_select");

    if (e.target.checked) {
        categorySelect.disabled = false;
    } else {
        categorySelect.disabled = true;
    }
})

$(document).on('click', "#allow_brand", function (e) {
    var brandSelect = document.getElementById("brand_select");

    if (e.target.checked) {
        brandSelect.disabled = false;
    } else {
        brandSelect.disabled = true;
    }
});

$(document).on('click', "#allow_product", function (e) {
    var productSelect = document.getElementById("product_select");

    if (e.target.checked) {
        productSelect.disabled = false;
    } else {
        productSelect.disabled = true;
    }
});

$(document).ready(function () {
    $('.custom-select').select2();
});

$(document).ready(function () {


    // if ($('.like').attr("data-like") == "1") {
    //     $('.like').addClass('active');
    // } else {
    //     $('.like').removeClass('active');
    // }
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });

    $(".first_filter h3").click(function () {
        if ($('#icon_rotate').hasClass('angle_icon_rotate')) {
            $("#icon_rotate").removeClass('angle_icon_rotate');
        } else {
            $("#icon_rotate").addClass("angle_icon_rotate");
        }
        $(this).parent().next().slideToggle();
    });

    $(".second_filter h3").click(function () {
        if ($('#icon_rotate2').hasClass('angle_icon_rotate')) {
            $("#icon_rotate2").removeClass('angle_icon_rotate');
        } else {
            $("#icon_rotate2").addClass("angle_icon_rotate");
        }
        $(this).parent().next().slideToggle();
    });

    $(".third_filter h3").click(function () {
        if ($('#icon_rotate3').hasClass('angle_icon_rotate')) {
            $("#icon_rotate3").removeClass('angle_icon_rotate');
        } else {
            $("#icon_rotate3").addClass("angle_icon_rotate");
        }
        $(this).parent().next().slideToggle();
    });

    $(document).on("mouseenter", ".dropdown", function () {
        let width = $(".header-flexbox-catalog").width();
        $('.dropdown-child.catalog-flexbox').css({'width': width});
    });
});

// $(document).on('click', '#do_login', function (e) {
//     $.ajax({
//         type: "POST",
//         url: route("ajax_login"),
//         data: $("#f_login").serialize(),
//         error: function (data) {
//             alert("Неверный пароль или имя пользователя");
//         },
//         success: function (data) {
//             location.reload();
//         }
//     });
// });


// $(document).on('click', '.add_in_basket', function (e) {
//     e.preventDefault();
//     if ($(this).hasClass('inBasket')) {
//         this.value = 'В корзине';
//         $(this).removeClass('inBasket');
//         $(this).addClass('inBasket-selected');
//     } else if ($(this).hasClass('buy-btn')) {
//         this.value = 'В корзине';
//         $(this).removeClass('buy-btn');
//         $(this).addClass('selected-buy-btn');
//     } else {
//         location.href = route("baskets.index");
//     }
//
//     var data = {
//         "quantity": $("#count_products").val() || 1,
//         "product_id": $(this).attr("data-id"),
//     };
//
//     $.ajax({
//         type: "POST",
//         url: route("baskets.store"),
//         data: data,
//         error: function (data) {
//             console.log(data);
//         },
//         success: function (data) {
//             if (data["new_product"] === true) {
//                 var count = parseInt($("#circle-price").children("p").text());
//                 $("#circle-price").children("p").text(count + 1);
//             }
//             var goalParams = {
//                 order_price: data["price"],
//                 currency: "RUB"
//             };
//             ym('57002299', 'reachGoal', 'BUY', goalParams);
//             return true;
//         }
//     });
// });

// $(document).on('click', '#buyOneClick', function () {
//     var quantity = $(".count option:selected").val();
//     var price = $("#actual_price").val();
//     $("#count_by_one_click").html(quantity);
//     $("#price_one_click").html(quantity * price);
//     $(".modalbox").fancybox({
//         "maxWidth": 600,
//         "padding": 0,
//         //"margin" : 0 auto,
//     });
// });
//
// $(document).on("click", "#sendByOneClick", function (e) {
//     e.preventDefault();
//     $.ajax({
//         type: "POST",
//         url: route("orders.by_one_click"),
//         data: {
//             product_id: $(this).attr("data-id"),
//             quantity: $(".count option:selected").val(),
//             name: $("#name_input-modal-buyOneClick").val(),
//             phone: $("#phone_input-modal-buyOneClick").val(),
//             delivery_type_id: $("#check_shipping_input option:selected").val(),
//             payment_type_id: $(".form_radio input:checked").val()
//         },
//         error: function (data) {
//             var error = JSON.parse(data.responseText)[0];
//             Swal.fire({
//                 icon: 'error',
//                 title: 'Ошибка',
//                 text: error
//             })
//         },
//         success: function (data) {
//             if (data.redirect) {
//                 location.href = data.redirect;
//             } else {
//                 Swal.fire({
//                     icon: 'success',
//                     title: 'Успех',
//                     text: data.message
//                 })
//             }
//         }
//     });
// });

// $(document).on('click', '#like_product', function () {
//     var is_like = $(this).attr("data-like");
//
//     var route_e = route("likes.store");
//     var method = "POST";
//
//     if (is_like == 1) {
//         $(this).attr("data-like", 0);
//         route_e = route("likes.destroy");
//         method = "DELETE";
//     } else {
//         $(this).attr("data-like", 1);
//     }
//
//     if ($('.like').attr("data-like") == "1") {
//         $(this).addClass('active');
//     } else {
//         $(this).removeClass('active');
//     }
//
//     $.ajax({
//         type: method,
//         url: route_e,
//         data: {
//             product_id: $(this).attr("data-id")
//         },
//         error: function (data) {
//             console.log(data);
//         },
//         success: function (data) {
//             console.log(data);
//         }
//     });
// });

$(document).on("click", ".menuBtn", function () {
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
        $(this).siblings(".mobile-menu-content").fadeIn();
        $('.searchContainer').addClass('changeColorRed');
        $('.mobile-menu').addClass('changeColorRed');
        $("#m_search").attr("src", "../images/icon/searchBwhite.png");
        $("#m_search_btn").attr("src", "../images/icon/searchBwhite.png");
        $("#m_login").attr("src", "../images/icon/loginWhite.png");
        $("#m_like").attr("src", "../images/icon/likeWhite.png");
        $("#m_basket").attr("src", "../images/icon/basketWhite.png");
    } else {
        $(this).siblings(".mobile-menu-content").fadeOut();
        $('.searchContainer').removeClass('changeColorRed');
        $('.mobile-menu').removeClass('changeColorRed');
        $("#m_search").attr("src", "../images/icon/searchB.png");
        $("#m_search_btn").attr("src", "../images/icon/searchB.png");
        $("#m_login").attr("src", "../images/icon/login.png");
        $("#m_like").attr("src", "../images/icon/like.png");
        $("#m_basket").attr("src", "../images/icon/basket.png");
    }
});

$(document).on("click", ".searchBtn", function () {
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
        $(this).siblings(".searchContainer").fadeIn();
        //$("#m_search").attr("src","images/icon/m_close.png");
    } else {
        $(this).siblings(".searchContainer").fadeOut();
        //$("#m_search").attr("src","images/icon/searchB.png");
    }
});

$(document).on("click", ".footer_head_text", function () {
    $(this).next().slideToggle();
    $(this).children('img').toggleClass('active');
});

// $(window).on("load", function () {
//     if ($("#basket_success").length) {
//         var goalParams = {
//             order_price: $("#basket_success").attr("data-price"),
//             currency: "RUB"
//         };
//         ym('57002299', 'reachGoal', 'ORDER', goalParams);
//         return true;
//     }
// })// ;

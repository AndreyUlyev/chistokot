<?php

return [
//    "login" => "admin@chistokot-main",
//    "password" => "123456",
//    "brand" => [
//        "id" => "79d1424a-2303-11ea-0a80-00d300109863",
//        "name" => "Бренд",
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d1424a-2303-11ea-0a80-00d300109863",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "customEntityMetaId" => "3b751ff3-2303-11ea-0a80-01ea00116ab0"
//    ],
//    "quantity" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14585-2303-11ea-0a80-00d300109864",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14585-2303-11ea-0a80-00d300109864",
//        "name" => "Количество"
//    ],
//    "composition" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14690-2303-11ea-0a80-00d300109865",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14690-2303-11ea-0a80-00d300109865",
//        "name" => "Состав"
//    ],
//    "about_delivery" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14790-2303-11ea-0a80-00d300109866",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14790-2303-11ea-0a80-00d300109866",
//        "name" => "О доставке",
//    ],
//    "guarantees" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d1492c-2303-11ea-0a80-00d300109867",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d1492c-2303-11ea-0a80-00d300109867",
//        "name" => "Гарантии",
//    ],
//    "where_apply" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14a15-2303-11ea-0a80-00d300109868",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14a15-2303-11ea-0a80-00d300109868",
//        "name" => "Применение"
//    ],
//    "maintenance" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14ac2-2303-11ea-0a80-00d300109869",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14ac2-2303-11ea-0a80-00d300109869",
//        "name" => "Уход",
//    ],
//    "meta_description" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14d55-2303-11ea-0a80-00d30010986c",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14d55-2303-11ea-0a80-00d30010986c",
//        "name" => "Meta description"
//    ],
//    "meta_title" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14c6e-2303-11ea-0a80-00d30010986b",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14c6e-2303-11ea-0a80-00d30010986b",
//        "name" => "Meta title"
//    ],
//    "meta_keywords" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/79d14b73-2303-11ea-0a80-00d30010986a",
//            "type" => "attributemetadata",
//            "mediaType" => "application/json"
//        ],
//        "id" => "79d14b73-2303-11ea-0a80-00d30010986a",
//        "name" => "Meta keywords",
//    ],
//
//
//    "organization" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/organization/b54889bd-229e-11ea-0a80-0468000d4313",
//            "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/organization/metadata",
//            "type" => "organization",
//            "mediaType" => "application/json",
//            "uuidHref" => "https://online.moysklad.ru/app/#mycompany/edit?id=b54889bd-229e-11ea-0a80-0468000d4313"
//        ]
//    ],
//    "agent" => [
//        "meta" => [
//            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/counterparty/b5531657-229e-11ea-0a80-0468000d4327",
//            "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/counterparty/metadata",
//            "type" => "counterparty",
//            "mediaType" => "application/json",
//            "uuidHref" => "https://online.moysklad.ru/app/#company/edit?id=b5531657-229e-11ea-0a80-0468000d4327"
//        ]
//    ],
//    "price" => [
//        "currency" => [
//            "meta" => [
//                "href" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/b549eaa4-229e-11ea-0a80-0468000d431a",
//                "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/metadata",
//                "type" => "currency",
//                "mediaType" => "application/json",
//                "uuidHref" => "https://online.moysklad.ru/app/#currency/edit?id=b549eaa4-229e-11ea-0a80-0468000d431a"
//            ]
//        ],
//        "priceType" => [
//            "meta" => [
//                "href" => "https://online.moysklad.ru/api/remap/1.2/context/companysettings/pricetype/b54b832d-229e-11ea-0a80-0468000d431b",
//                "type" => "pricetype",
//                "mediaType" => "application/json"
//            ],
//            "id" => "b54b832d-229e-11ea-0a80-0468000d431b",
//            "name" => "Цена без скидки",
//            "externalCode" => "cbcf493b-55bc-11d9-848a-00112f43529a"
//        ]
//    ],
//    "price_discount" => [
//        "currency" => [
//            "meta" => [
//                "href" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/b549eaa4-229e-11ea-0a80-0468000d431a",
//                "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/metadata",
//                "type" => "currency",
//                "mediaType" => "application/json",
//                "uuidHref" => "https://online.moysklad.ru/app/#currency/edit?id=b549eaa4-229e-11ea-0a80-0468000d431a"
//            ]
//        ],
//        "priceType" => [
//            "meta" => [
//                "href" => "https://online.moysklad.ru/api/remap/1.2/context/companysettings/pricetype/009ffaa5-2410-11ea-0a80-0241001fc9d2",
//                "type" => "pricetype",
//                "mediaType" => "application/json"
//            ],
//            "id" => "009ffaa5-2410-11ea-0a80-0241001fc9d2",79d14d55-2303-11ea-0a80-00d30010986c
//            "name" => "Цена со скидкой",
//            "externalCode" => "f289ef84-6874-4c04-a5f4-3dd08b5aa58d"
//        ]
//    ]
    "login" => "admin@programmer12",
    "password" => "9ee60e8271",
    "brand" => [
        "id" => "d55329ee-55b5-11ea-0a80-001500060e8c",
        "name" => "Бренды",
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/288aaad3-55b8-11ea-0a80-02830006053e",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "type" => "customentity"
    ],
    "quantity" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df1aa-55b9-11ea-0a80-05130005fcb8",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df1aa-55b9-11ea-0a80-05130005fcb8",
        "name" => "Количество"
    ],
    "composition" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df412-55b9-11ea-0a80-05130005fcb9",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df412-55b9-11ea-0a80-05130005fcb9",
        "name" => "Состав"
    ],
    "about_delivery" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df4da-55b9-11ea-0a80-05130005fcba",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df4da-55b9-11ea-0a80-05130005fcba",
        "name" => "О доставке",
    ],
    "guarantees" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df57b-55b9-11ea-0a80-05130005fcbb",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df57b-55b9-11ea-0a80-05130005fcbb",
        "name" => "Гарантии",
    ],
    "where_apply" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df668-55b9-11ea-0a80-05130005fcbc",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df668-55b9-11ea-0a80-05130005fcbc",
        "name" => "Применение"
    ],
    "maintenance" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df703-55b9-11ea-0a80-05130005fcbd",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df703-55b9-11ea-0a80-05130005fcbd",
        "name" => "Уход",
    ],
    "meta_description" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df900-55b9-11ea-0a80-05130005fcc0",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df900-55b9-11ea-0a80-05130005fcc0",
        "name" => "Meta description"
    ],
    "meta_title" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df845-55b9-11ea-0a80-05130005fcbf",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df845-55b9-11ea-0a80-05130005fcbf",
        "name" => "Meta title"
    ],
    "meta_keywords" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/059df900-55b9-11ea-0a80-05130005fcc0",
            "type" => "attributemetadata",
            "mediaType" => "application/json"
        ],
        "id" => "059df900-55b9-11ea-0a80-05130005fcc0",
        "name" => "Meta keywords",
    ],


    "organization" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/organization/70834aef-522a-11ea-0a80-01ba00046c4e",
            "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/organization/metadata",
            "type" => "organization",
            "mediaType" => "application/json",
            "uuidHref" => "https://online.moysklad.ru/app/#mycompany/edit?id=70834aef-522a-11ea-0a80-01ba00046c4e"
        ]
    ],
    "agent" => [
        "meta" => [
            "href" => "https://online.moysklad.ru/api/remap/1.2/entity/counterparty/7093a277-522a-11ea-0a80-01ba00046c62",
            "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/counterparty/metadata",
            "type" => "counterparty",
            "mediaType" => "application/json",
            "uuidHref" => "https://online.moysklad.ru/app/#company/edit?id=7093a277-522a-11ea-0a80-01ba00046c62"
        ]
    ],
    "price" => [
        "currency" => [
            "meta" => [
                "href" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/70851c00-522a-11ea-0a80-01ba00046c55",
                "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/metadata",
                "type" => "currency",
                "mediaType" => "application/json",
                "uuidHref" => "https://online.moysklad.ru/app/#currency/edit?id=70851c00-522a-11ea-0a80-01ba00046c55"
            ]
        ],
        "priceType" => [
            "meta" => [
                "href" => "https://online.moysklad.ru/api/remap/1.2/context/companysettings/pricetype/70871d1d-522a-11ea-0a80-01ba00046c56",
                "type" => "pricetype",
                "mediaType" => "application/json"
            ],
            "id" => "70871d1d-522a-11ea-0a80-01ba00046c56",
            "name" => "Цена без скидки",
            "externalCode" => "cbcf493b-55bc-11d9-848a-00112f43529a"
        ]
    ],
    "price_discount" => [
        "currency" => [
            "meta" => [
                "href" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/70851c00-522a-11ea-0a80-01ba00046c55",
                "metadataHref" => "https://online.moysklad.ru/api/remap/1.2/entity/currency/metadata",
                "type" => "currency",
                "mediaType" => "application/json",
                "uuidHref" => "https://online.moysklad.ru/app/#currency/edit?id=70851c00-522a-11ea-0a80-01ba00046c55"
            ]
        ],
        "priceType" => [
            "meta" => [
                "href" => "https://online.moysklad.ru/api/remap/1.2/context/companysettings/pricetype/589886ce-5707-11ea-0a80-03e90010bd5c",
                "type" => "pricetype",
                "mediaType" => "application/json"
            ],
            "id" => "589886ce-5707-11ea-0a80-03e90010bd5c",
            "name" => "Цена со скидкой",
            "externalCode" => "7b3cf46e-a251-468f-bd27-bfe4c16a0cee"
        ]
    ],
    "mods" => [
        "name" => "036d3238-58d6-11ea-0a80-0103000df965",
        "color" => "036d35ce-58d6-11ea-0a80-0103000df966",
        "size" => "036d36b6-58d6-11ea-0a80-0103000df967"
    ]
];

<?php

return [
    [
        "name" => "Главная",
        "ico" => "icon-home",
        "route" => "admin.main"
    ],
    [
        "name" => "Бренды",
        "ico" => "fa-tag",
        "route" => "admin.brands.index"
    ],
    [
        "name" => "Категории",
        "ico" => "icon-th",
        "route" => "admin.categories.index"
    ],
    [
        "name" => "Пользователи",
        "ico" => "fa-users",
        "route" => "admin.users.index"
    ],
    [
        "name" => "Слайды",
        "ico" => "fa-film",
        "route" => "admin.slider.index"
    ],
    [
        "name" => "Продукты",
        "ico" => "fa-cube",
        "route" => "admin.products.index"
    ],
    [
        "name" => "Промокоды",
        "ico" => "fa-codepen",
        "route" => "admin.promocodes.index"
    ],
    [
        "name" => "Статьи",
        "ico" => "fa-newspaper",
        "route" => "admin.articles.index"
    ],
    [
        "name" => "Заказы",
        "ico" => "fa-shopping-basket",
        "route" => "admin.orders.index"
    ],
    [
        "name" => "Способы доставки",
        "ico" => "fa-truck",
        "route" => "admin.delivery_types.index"
    ],
    [
        "name" => "Комментарии",
        "ico" => "fa-comments",
        "route" => "admin.comments.index"
    ],

    [
        "name" => "Вернуться на сайт",
        "ico" => "fa-backward",
        "route" => "home"
    ],
//    [
//        "name" => "Выход",
//        "ico" => "fa-times-circle",
//        "route" => "admin.main"
//    ]
];

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text("composition")->nullable();
            $table->text("about_delivery")->nullable();
            $table->text("guarantees")->nullable();
            $table->text("where_apply")->nullable();
            $table->text("maintenance")->nullable();
            $table->string("country")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('composition');
            $table->dropColumn('about_delivery');
            $table->dropColumn('guarantees');
            $table->dropColumn('where_apply');
            $table->dropColumn('maintenance');
            $table->dropColumn('country');
        });
    }
}

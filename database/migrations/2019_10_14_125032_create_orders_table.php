<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Удалить пункт sum
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone');
            $table->string('address')->nullable();
            $table->bigInteger('delivery_type_id')->unsigned();
            $table->foreign('delivery_type_id')
                ->references('id')->on('delivery_types')
                ->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->bigInteger('promocode_id')->unsigned()->nullable();
            $table->foreign('promocode_id')
                ->references('id')
                ->on('promocodes')
                ->onDelete('cascade');
            $table->bigInteger('payment_type_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

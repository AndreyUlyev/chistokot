<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('slug');

            $table->bigInteger('modification_id')->unsigned()->nullable();
            $table->foreign('modification_id')->references('id')->on('modifications')->onDelete('cascade');

            $table->bigInteger('main_modification_id')->unsigned()->nullable();
            $table->foreign('main_modification_id')->references('id')->on('modifications')->onDelete('cascade');

            $table->bigInteger('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');

            $table->bigInteger('type_modification_id')->unsigned()->nullable();
            $table->foreign('type_modification_id')->references('id')->on('types_modifications')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modifications');
    }
}

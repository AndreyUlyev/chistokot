<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsForMoySklad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string("moy_sklad_id")->nullable();
        });

        Schema::table('brands', function (Blueprint $table) {
            $table->string("moy_sklad_id")->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string("moy_sklad_id")->nullable();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string("moy_sklad_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('moy_sklad_id');
        });

        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('moy_sklad_id');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('moy_sklad_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('moy_sklad_id');
        });
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Comment::class, function (Faker $faker) {
    $userId = User::select("id")->orderByRaw('RAND()')->first()->id;
    return [
        "user_id" => $faker->randomElement([null, $userId]),
        "title" => $faker->catchPhrase,
        "name" => $faker->name,
        "rating" => rand(1, 5),
        "comment" => $faker->text(),
        "approved" => true
    ];
});

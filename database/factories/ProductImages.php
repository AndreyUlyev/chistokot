<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductsImages;
use Faker\Generator as Faker;

$factory->define(\App\ProductsImage::class, function (Faker $faker) {
    $image = $faker->image('public/uploads/standard/', 600, 600, null, null);
    copy('public/uploads/standard/'.$image, 'public/uploads/mini/'.$image);
    return [
        "link" => $image
    ];
});

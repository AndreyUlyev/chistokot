<?php

use App\Brand;
use Illuminate\Database\Seeder;

class InsertBrands extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
            "name" => "Тестовый бренд",
            "moy_sklad_id" => null
        ]);
    }
}

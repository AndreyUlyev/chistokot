<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class CreateSuperUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = "PcY6oM7fJ";
        $user = User::create([
            'name' => "Admin",
            'second_name' => "Adminov",
            'email' => "mail@chistokot.ru",
            'password' => Hash::make($pass),
            'email_verified_at' => \Carbon\Carbon::now()
        ]);

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Администратор';
        $admin->description  = 'Администратор проекта';
        $admin->save();
        $user->attachRole('admin');
    }
}

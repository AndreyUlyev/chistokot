<?php

use App\Category;
use Illuminate\Database\Seeder;

class InsertCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Ножи кухонные",
            "Салфетки SMART",
            "Щетки",
            "Коврики грязезащитные",
            "Коврики защитные"
        ];
        foreach ($categories as $category) {
            Category::create([
                "name" => $category,
                "slug" => Str::slug($category)
            ]);
        }
    }
}

<?php

use App\User;
use Illuminate\Database\Seeder;

class CreateMoySkladUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = "VP4WlpY3";
        $user = User::create([
            'name' => "MoySklad",
            'second_name' => "MoySklad",
            'email' => "moysklad@chistokot.ru",
            'password' => Hash::make($pass),
            'email_verified_at' => \Carbon\Carbon::now()
        ]);
        $admin = \App\Role::where("name", "admin")->first();
        $user->attachRole($admin);
    }
}

<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateSuperUser::class);
//        $this->call(CreateMoySkladUser::class);
//        $this->call(InsertCategories::class);
//        $this->call(InsertBrands::class);
        $this->call(InsertProductsAndModifications::class);
    }
}

<?php

use App\Brand;
use App\Category;
use App\Color;
use App\Modification;
use App\Product;
use App\TypeModification;
use Illuminate\Database\Seeder;
use App\ProductModification;

class InsertProductsAndModifications extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertBrands();
        $this->insertCategories();
        $this->insertTypeModifications();
        $this->insertColors();
        $this->insertModifications();
        $this->insertProduct();
        $this->insertProductModification();
        $this->insertProductImages();
        $this->insertUsers();
        $this->insertProductsComments();
    }

    private function insertBrands()
    {
        Brand::create([
            "id" => 1,
            "name" => "тестовый бренд"
        ]);
    }

    private function insertCategories()
    {
        Category::create([
            "id" => 1,
            "name" => "Тестовая категория",
            "slug" => "test"
        ]);
    }

    private function insertTypeModifications()
    {
        TypeModification::create([
            "id" => 1,
            "name" => "цвет"
        ]);
        TypeModification::create([
            "id" => 2,
            "name" => "оттенок"
        ]);
        TypeModification::create([
            "id" => 3,
            "name" => "размер"
        ]);
    }

    private function insertColors()
    {
        Color::insert([
            "id" => 1,
            "color" => "Красный",
            "hex_color" => "#ff0000"
        ]);
        Color::insert([
            "id" => 2,
            "color" => "Зеленый",
            "hex_color" => "#008000"
        ]);
        Color::insert([
            "id" => 3,
            "color" => "Светло-зеленый",
            "hex_color" => "#90ee90"
        ]);
        Color::insert([
            "id" => 4,
            "color" => "Темно-зеленый",
            "hex_color" => "#013220"
        ]);
    }

    private function insertModifications()
    {
        Modification::create([
            "id" => 1,
            "name" => null,
            "slug" => Str::slug("red"),
            "type_modification_id" => 1,
            "color_id" => 1
        ])->id;
        Modification::create([
            "id" => 2,
            "name" => null,
            "slug" => Str::slug("green"),
            "type_modification_id" => 1,
            "color_id" => 2
        ])->id;
        Modification::create([
            "id" => 3,
            "name" => null,
            "modification_id" => 2,
            "main_modification_id" => 2,
            "slug" => Str::slug("light-green"),
            "type_modification_id" => 2,
            "color_id" => 3
        ])->id;
        Modification::create([
            "id" => 4,
            "name" => null,
            "modification_id" => 2,
            "main_modification_id" => 2,
            "slug" => Str::slug("hard-green"),
            "type_modification_id" => 2,
            "color_id" => 4
        ])->id;
        Modification::create([
            "id" => 5,
            "name" => "1x1",
            "modification_id" => 3,
            "main_modification_id" => 2,
            "slug" => Str::slug("1x1"),
            "type_modification_id" => 3
        ])->id;
        Modification::create([
            "id" => 6,
            "name" => "2x2",
            "modification_id" => 3,
            "main_modification_id" => 2,
            "slug" => Str::slug("2x2"),
            "type_modification_id" => 3
        ])->id;
        Modification::create([
            "id" => 7,
            "name" => "3x3",
            "modification_id" => 4,
            "main_modification_id" => 2,
            "slug" => Str::slug("3x3"),
            "type_modification_id" => 3
        ])->id;
    }

    private function insertProduct()
    {
        Product::create([
            "id" => 1,
            "name" => "Продукт без модификации",
            "price" => 920,
            "price_discount" => 400,
            "is_popular" => true,
            "category_id" => 1,
            "brand_id" => 1,
        ]);

        Product::create([
            "id" => 2,
            "name" => "Продукт с модификацией 1",
            "price" => 680,
            "is_popular" => true,
            "category_id" => 1,
            "brand_id" => 1,
        ])->id;

        Product::create([
            "id" => 3,
            "name" => "Продукт с модификацией 2",
            "price" => 1034,
            "price_discount" => 600,
            "is_popular" => true,
            "category_id" => 1,
            "brand_id" => 1,
        ])->id;

        Product::create([
            "id" => 4,
            "name" => "Продукт с модификацией 3",
            "price" => 956,
            "is_popular" => true,
            "category_id" => 1,
            "brand_id" => 1,
        ])->id;
    }

    private function insertProductModification()
    {
        ProductModification::insert([
            "modification_id" => 1,
            "product_id" => 2
        ]);

        ProductModification::insert([
            "modification_id" => 6,
            "product_id" => 3
        ]);

        ProductModification::insert([
            "modification_id" => 7,
            "product_id" => 4,
            "is_main" => 1
        ]);
    }

    private function insertProductImages()
    {
        for ($i = 1; $i < 5; $i++) {
            factory(\App\ProductsImage::class, rand(1, 8))->create([
                'product_id' => $i
            ]);
        }
    }

    private function insertUsers()
    {
        factory(\App\User::class, 25)->create();
    }

    private function insertProductsComments()
    {
        for ($i = 1; $i < 5; $i++) {
            factory(\App\Comment::class, rand(1, 25))->create([
                'product_id' => $i
            ]);
        }
    }
}

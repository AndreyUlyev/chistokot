<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('home', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('home'));
});

Breadcrumbs::for('search', function ($breadcrumbs, $search) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Поиск'.($search ? ' / '.$search : false), route('categories.index'));
});

Breadcrumbs::for('categories.list', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Категории', route('categories.index'));
});

Breadcrumbs::for('categories.page', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('categories.list');
    $breadcrumbs->push($category->name, route('categories.page', ["slug" => $category->slug]));
});

Breadcrumbs::for('product', function ($breadcrumbs, $category, $product) {
    $breadcrumbs->parent('categories.page', $category);
    $breadcrumbs->push($product->name);
});

Breadcrumbs::for('news.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Новости", route("news.index"));
});

Breadcrumbs::for('news.show', function ($breadcrumbs, $article) {
    $breadcrumbs->parent('news.index');
    $breadcrumbs->push($article);
});





Breadcrumbs::for('pages.contact', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Контакты");
});

Breadcrumbs::for('pages.shipping', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Доставка');
});

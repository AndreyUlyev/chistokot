<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Product;
use Symfony\Component\Console\Input\Input;

Route::pattern('id_slider', '[0-9]+');
Route::pattern('id_category', '[0-9]+');
Route::pattern('id_brand', '[0-9]+');
Route::pattern('id_basket', '[0-9]+');
Route::pattern('id_product', '[0-9]+');
Route::pattern('id_promocode', '[0-9]+');

//URL::forceScheme('https');
Route::post('/ajax_login', 'Auth\LoginController@ajaxLogin')->name("ajax_login");
Auth::routes();


Route::get('/', 'MainPageController@main')->name("home");
Route::get('/yml', 'YmlController@index')->name("sitemap");
Route::get('/content/{slug}', 'SiteRedirectController@redirect');

Route::group(['prefix' => 'blocks', 'as' => 'blocks.'], function () {
    Route::get("/popular_products", "MainPageController@popularProducts")->name("popular_products");
    Route::get("/products_category/{id}", "MainPageController@productsCategory")->name("products_category");
});

/* Static Pages routing */
Route::prefix('page')->group(function () {
    Route::get('/contact', 'StaticPagesController@contact')->name('contact');
    Route::get('/shipping', 'StaticPagesController@shipping')->name('shipping');
    Route::get('/gifts', 'StaticPagesController@gifts')->name('gifts');
    Route::get('/price_list', 'StaticPagesController@priceList')->name('price_list');
    Route::get('/cooperation', 'StaticPagesController@cooperation')->name('cooperation');
    Route::get('/job_offers', 'StaticPagesController@jobOffers')->name('job_offers');
    Route::get('/purchase_rules', 'StaticPagesController@purchaseRules')->name('purchase_rules');
});

Route::group([], function () {
    Route::get('/search', 'SearchController@index')->name('search');

    Route::group(['as' => 'payments.', 'prefix' => 'payments'], function () {
        Route::get('/success', 'PaymentsController@success')->name('success');
        Route::get('/error', 'PaymentsController@error')->name('error');
    });

    Route::group(['as' => 'orders.', 'prefix' => 'order'], function () {
        Route::post('/', 'OrdersController@store')->name('store');
        Route::post('/one_click', 'OrdersController@byOneClick')->name('by_one_click');
    });

    Route::group(['as' => 'user.', 'prefix' => 'user'], function () {
        Route::get('/edit', 'UsersController@edit')->name('user.edit');
    });

    Route::group(['as' => 'news.', 'prefix' => 'news'], function () {
        Route::get('/', 'ArticlesController@index')->name('index');
        Route::get('/{id}', 'ArticlesController@show')->name('show');
    });

    Route::group(['as' => 'categories.', 'prefix' => 'categories'], function () {
        Route::get('/', 'CategoriesController@index')->name('index');
        Route::get('/products', 'CategoriesController@getProducts')->name("products");
        Route::get('/filters/{slug}', 'CategoriesController@filters')->name("filters");
        Route::get('/{slug}', 'CategoriesController@show')->name("page");
    });

    Route::group(['as' => 'products.', 'prefix' => 'products'], function () {
        Route::get('/{slug}/{size?}/{color?}', 'ProductsController@show')->name("page");
    });

    Route::group(['as' => 'likes.', 'prefix' => 'likes'], function () {
        Route::get('/', 'LikesController@index')->name("index");
        Route::post('/', 'LikesController@store')->name("store");
        Route::delete('/', 'LikesController@destroy')->name("destroy");
    });

    Route::group(['as' => 'comments.', 'prefix' => 'comments'], function () {
        Route::get('/{product_id}', 'CommentsController@index')->name("index");
        Route::post('/{product_id}', 'CommentsController@store')->name("store");
    });

    Route::group(['as' => 'baskets.', 'prefix' => 'baskets'], function () {
        Route::get('/', 'BasketsController@index')->name('index');
        Route::get('/data', 'BasketsController@getData')->name('get_data');
        Route::put('/{product_id}', 'BasketsController@update')->name('update');
        Route::post('/', 'BasketsController@store')->name('store');
        Route::delete('/{product_id?}', 'BasketsController@destroy')->name('destroy');
    });
});

Route::group(['prefix' => 'admin1', 'as' => 'admin.'], function () {
    Route::get("statistics", "Admin\StatisticsController@getStatistics");
    Route::get("orders", "Admin\OrdersController@index");
    Route::get("brands", "Admin\BrandsController@index");
    Route::get("categories", "Admin\CategoriesController@index");
    Route::get("users", "Admin\UsersController@index");
});

/* Admin routing */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware'=>['auth', 'role:admin']], function () {
    Route::get('/', 'StaticPagesController@admin')->name("main");

    Route::group(['as' => 'charts.', 'prefix' => 'charts'], function () {
        Route::get('/orders_for_registered_or_not', 'Admin\ChartsController@ordersForRegisteredOrNot')->name('orders_for_registered_or_not');
        Route::get('/avg_orders_sum', 'Admin\ChartsController@avgOrdersSum')->name('avg_orders_sum');
        Route::get('/statistics_per_month', 'Admin\ChartsController@statisticsPerMonth')->name('statistics_per_month');
    });


    Route::resource('/products', 'Admin\ProductsController', [
        'names' => [
            'index' => 'products.index',
            'show' => 'products.show',
            'create' => 'products.create',
            'update' => 'products.update',
            'edit' => 'products.edit',
            'store' => 'products.store',
            'destroy' => 'products.destroy',
        ],
        'parameters' => [
            'products' => 'id_product'
        ]
    ]);

    Route::resource('/brands', 'Admin\BrandsController', [
        'names' => [
            'index' => 'brands.index',
            'create' => 'brands.create',
            'update' => 'brands.update',
            'edit' => 'brands.edit',
            'store' => 'brands.store',
            'destroy' => 'brands.destroy',
        ],
        'parameters' => [
            'brands' => 'id_brand'
        ]
    ])->except('show');

    Route::resource('/categories', 'Admin\CategoriesController', [
        'names' => [
            'index' => 'categories.index',
            'create' => 'categories.create',
            'update' => 'categories.update',
            'edit' => 'categories.edit',
            'store' => 'categories.store',
            'destroy' => 'categories.destroy',
        ],
        'parameters' => [
            'categories' => 'id_category'
        ]
    ])->except('show');

    Route::resource('/comments', 'Admin\CommentsController', [
        'name' => [
            'index' => 'comments.index',
            'update' => 'comments.update',
            'destroy' => 'comments.destroy',
        ],
        'parameters' => [
            'comments' => 'id_comment',
        ]
    ])->except(['show', 'create', 'edit', 'store']);

    Route::resource('/delivery_types', 'Admin\DeliveryTypesController', [
        'name' => [
            'index' => 'delivery_types.index',
            'create' => 'delivery_types.create',
            'update' => 'delivery_types.update',
            'edit' => 'delivery_types.edit',
            'store' => 'delivery_types.store',
            'destroy' => 'delivery_types.destroy',
        ],
        'parameters' => [
            'delivery_types' => 'id_delivery_type',]
    ])->except('show');


    Route::resource('/slider', 'Admin\SliderController', [
        'names' => [
            'create' => 'slider.create',
            'store' => 'slider.store',
            'update' => 'slider.update',
            'edit' => 'slider.edit',
            'destroy' => 'slider.destroy',
        ],
        'parameters' => [
            'slider' => 'id_slider'
        ]
    ])->except(['show']);

    Route::resource('/users', 'Admin\UsersController', [
        'names' => [
            'index' => 'users.index',
            'create' => 'users.create',
            'store' => 'users.store',
            'edit' => 'users.edit',
            'update' => 'users.update',
            'destroy' => 'users.destroy'
        ],
        'parameters' => [
            'users' => 'id_user'
        ]
    ])->except('show');
    Route::put('/users/{id_user}/edit', 'Admin\UsersController@updatePassword')->name('users.update_password');

    Route::resource('/articles', 'Admin\ArticlesController', [
        'names' => [
            'index' => 'articles.index',
            'create' => 'articles.create',
            'update' => 'articles.update',
            'edit' => 'articles.edit',
            'store' => 'articles.store',
            'destroy' => 'articles.destroy',
        ],
        'parameters' => [
            'articles' => 'id_article'
        ]
    ])->except('show');

    Route::resource('/promocodes', 'Admin\PromocodesController', [
        'name' => [
            'index' => 'promocodes.index',
            'create' => 'promocodes.create',
            'update' => 'promocodes.update',
            'edit' => 'promocodes.edit',
            'store' => 'promocodes.store',
            'destroy' => 'promocodes.destroy',
        ],
        'parameters' => [
            'promocodes' => 'id_promocode',
        ]
    ])->except('show');

    Route::resource('/orders', 'Admin\OrdersController', [
        'name' => [
            'index' => 'orders.index',
            'show' => 'orders.show',
            'create' => 'orders.create',
            'update' => 'orders.update',
            'edit' => 'orders.edit',
            'store' => 'orders.store',
            'destroy' => 'orders.destroy',
        ],
        'parameters' => [
            'orders' => 'id_order',
        ]
    ]);
});

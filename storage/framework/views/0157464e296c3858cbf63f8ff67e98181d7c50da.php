<?php $__env->startSection('content'); ?>
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">Пользователи</span>
        </h1>
    </div>
    <?php if(session('success')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo e(session('success')); ?>

        </div>
    <?php endif; ?>
    <a href="<?php echo e(route("admin.users.create")); ?>" class="btn btn-primary btn-lg btn-warning">Добавить пользователя</a>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables-1" class="table table-middle nowrap">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>Имя</th>
                                <th>email</th>
                                <th>Почта подтверждена</th>
                                <th>Дата создания</th>
                                <th>Дата изменения</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <?php echo e($loop->iteration + ($users->currentpage() - 1) * $users->perpage()); ?>

                                    </td>
                                    <td>
                                        <strong><?php echo e($user["name"]); ?></strong>
                                    </td>
                                    <td>
                                        <strong><?php echo e($user["email"]); ?></strong>
                                    </td>
                                    <td>
                                        <strong><?php echo e($user["email_verified_at"]); ?></strong>
                                    </td>
                                    <td class="maw-320">
                                        <?php echo e($user["created_at"]); ?>

                                    </td>
                                    <td><?php echo e($user["updated_at"]); ?></td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?php echo e(route("admin.users.edit", ["id_user" => $user["id"]])); ?>">Изменить</a></li>
                                                <li>
                                                    <a href="" class="delete_btn">
                                                        Забанить
                                                        <form class="hidden_form" method="post" action="<?php echo e(route("admin.users.destroy", ["id_user" => $user["id"]])); ?>">
                                                            <?php echo csrf_field(); ?>
                                                            <?php echo method_field("DELETE"); ?>
                                                        </form>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="dataTables_paginate paging_simple_numbers" id="demo-dynamic-tables-2_paginate">
                                <?php echo e($users->render()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/admin/users/list.blade.php ENDPATH**/ ?>
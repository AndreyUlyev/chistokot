<?php $__env->startSection('content'); ?>
    <section class="content-section">
        <div class="container site-path">
            <a href="<?php echo e(route('mainPage')); ?>">Главная</a> / <a href="">Каталог</a>
        </div>
        <div class="container prod-flexbox">
            <?php if($product->discount != null): ?>
                <div id="square">
                    <p class="black-color"><b><?php echo e($product->discount); ?>%</b> скидка</p>
                </div>
            <?php endif; ?>

            <div class="text-center " id="prod-flexbox1">
                <product-slider-component :images="<?php echo e(json_encode($product->productImages)); ?>"></product-slider-component>
            </div>
            <div id="prod-flexbox2">
                <div class="title_prod">
                    <div id="prod-name-sale">
                        <p class="h2 category__title"><?php echo e($product->name); ?></p>
                    </div>
                    <img class="category__wave" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
                </div>
                <div id="price-prod">
                    <?php if($product->price_discount != null): ?>
                        <div class="old-price-prod"><?php echo e($product->price); ?> РУБ.</div>
                        <div class="new-price-prod"><?php echo e($product->price_discount); ?> РУБ.</div>
                    <?php else: ?>
                        <div class="new-price-prod"><?php echo e($product->price); ?> РУБ.</div>
                    <?php endif; ?>
                        <div id="square-mobile">
                            <p class="black-color"><b><?php echo e($product->discount); ?>%</b> скидка</p>
                        </div>
                </div>

                <p class="about black-color">
                    <?php echo e($product->short_description); ?>

                </p>
                <div class="btns">
                    <select class="count">
                        <?php for($i = 1; $i <= $product->quantity; $i++): ?>
                            <option><?php echo e($i); ?></option>
                        <?php endfor; ?>
                    </select>
                    <input type="submit" class="buyOneClick" id="buyOneClick" value="Купить в 1 клик">
                    <input type="submit" class="inBasket" id="inBasket" value="В корзину" data-id="<?php echo e($product->id); ?>">
                    <input type="submit" class="like" id="like_product" value="" data-like="<?php echo e($like); ?>"
                           data-id="<?php echo e($product->id); ?>">
                </div>
                <div class="about-prod">
                    <?php if($product->composition): ?>
                        <p class="title-text">Состав</p>
                        <div class="horizontalStrip"></div>
                        <p class="about">
                            <?php echo e($product->composition); ?>

                        </p>
                    <?php endif; ?>

                    <?php if($product->about_delivery): ?>
                        <p class="title-text">О доставке</p>
                        <div class="horizontalStrip"></div>
                        <p class="about">
                            <?php echo e($product->about_delivery); ?>

                        </p>
                    <?php endif; ?>

                    <?php if($product->guarantees): ?>
                        <p class="title-text">Гарантии</p>
                        <div class="horizontalStrip"></div>
                        <p class="about">
                            <?php echo e($product->guarantees); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <section class="content-section">
        <div class="container tabs category__block">
            <input id="tab1" type="radio" name="tabs" checked>
            <label class="title-text-tab" for="tab1">Описание</label>
            <input id="tab2" type="radio" name="tabs">
            <label class="title-text-tab" for="tab2">Отзывы</label>
            <div class="horizontalStrip"></div>
            <section id="content-tab1">
                <p class="review-desc">
                    <?php echo e($product->description); ?>

                    <br><br><strong>Применение</strong><br><br>
                    <?php echo e($product->where_apply); ?>

                    <br><br><strong>Уход</strong><br><br>
                    <?php echo e($product->maintenance); ?>

                    <br><br><strong>Производитель:</strong>
                    <?php echo e($product->country_name); ?>

                </p>
            </section>
            <section id="content-tab2">
                <comments-component :product_id="<?php echo e($product->id); ?>"></comments-component>
            </section>
        </div>
    </section>

    <section class="container">
        <?php echo $__env->make('includes.with_buy', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>
    <section>
        <?php echo $__env->make('includes.be_aware_of_sales', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>
    <section class="container">
        <?php echo $__env->make('includes.short_about', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/products/page.blade.php ENDPATH**/ ?>
<div class="text-center category__block news-block">
    <p class="h2 category__title">Скидка 500 рублей</p>
    <img class="category__wave" src="<?php echo e(asset("images/wavww.png")); ?>" alt="">
    <p class="category__description">За подписку на наши новости.</p>
    <a href="#conditions" class="modalbox" id="stock">Условия акции.</a>
    <form action="<?php echo e(route('send_subscription_promocode')); ?>" method="get">
        <?php echo csrf_field(); ?>
        <div class="promoSub">
            <label>
                <input name="email" type="text" class="news-form" placeholder="Email">
            </label>
            <a href="<?php echo e(route('send_subscription_promocode')); ?>" class="modalbox">
                <input type="submit" class="news-form-btn" value="Готово"></a>
        </div>
    </form>
</div>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/includes/subscription_sale.blade.php ENDPATH**/ ?>
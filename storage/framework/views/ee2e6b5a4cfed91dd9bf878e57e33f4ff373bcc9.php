<?php $__env->startSection('content'); ?>
    <div class="basket">
        <div class="basket_first_column">
            <div class="site-path">
                <a href="<?php echo e(route('mainPage')); ?>">Главная</a> / <a href="<?php echo e(route('shipping')); ?>">Доставка</a>
            </div>
            <div class="block-head">
                <p class="h2 category__title">Доставка</p>
                <img class="category__wave address__description" src="imgs/wave.png" alt="">
            </div>
            <p class="shipping_first_column_text"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
                pretium felis nec nulla aliquet volutpat.
                Fusce tempus diam eu lorem ultricies egestas non vitae nisi. Suspendisse odio diam, iaculis vel mollis
                vel,
                suscipit vel sapien. Nulla facilisi. Vestibulum fermentum non arcu iaculis blandit. Ut consequat elit
                vitae
                lectus mattis varius. Phasellus non mi ex. Donec in sodales justo, vel interdum ligula. Mauris laoreet,
                arcu
                vitae auctor porttitor, leo tortor sodales nunc, vulputate aliquam nunc eros quis leo. Aliquam mattis
                ultrices purus ac vestibulum. Morbi dictum magna eu mauris fringilla, vitae porta sapien ullamcorper.
                Nullam
                consectetur est nisl, ut lobortis mi fringilla eget.</p>
            <p class="shipping_first_column_text"> Nullam egestas ipsum a lectus sagittis congue. Nulla ultricies
                imperdiet dictum. Proin ut blandit leo.
                Aliquam non est volutpat urna euismod convallis.
                Pellentesque placerat sem id neque ultricies mollis. Quisque pharetra nisl at felis vestibulum, ut
                vestibulum enim congue. Fusce posuere auctor sem nec ornare. Sed aliquet condimentum quam et feugiat.
                Nullam
                faucibus elit eu justo consequat laoreet. Ut molestie placerat lacinia. Integer quis pharetra ligula.
                Vivamus laoreet libero libero, et tincidunt ex sollicitudin id. Suspendisse id libero viverra, luctus
                ipsum
                vel, tincidunt risus. Nunc eget vulputate erat. Maecenas pulvinar elit nulla, sed accumsan libero
                efficitur
                non.</p>
            <p class="shipping_first_column_text">Nullam faucibus sem vitae odio laoreet sollicitudin. Nunc nec dui et
                mi gravida rutrum at non urna.
                Pellentesque laoreet odio a magna condimentum, sed pulvinar urna hendrerit. Suspendisse facilisis nulla
                in
                sapien tincidunt aliquet. Nam imperdiet volutpat ligula, eget blandit dui feugiat sodales. Maecenas
                luctus
                ipsum non purus volutpat auctor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                inceptos himenaeos. Integer tincidunt convallis malesuada. Orci varius natoque penatibus et magnis dis
                parturient montes, nascetur ridiculus mus. Fusce sem odio, eleifend eu aliquam non, ornare ut mi.
                Integer
                consectetur, massa ac volutpat sollicitudin, tellus enim vehicula dui, ut molestie felis enim ut nunc.
                Nulla
                et mauris libero.</p>
            <p class="shipping_first_column_text">Nullam faucibus sem vitae odio laoreet sollicitudin. Nunc nec dui et
                mi gravida rutrum at non urna.
                Pellentesque laoreet odio a magna condimentum, sed pulvinar urna hendrerit. Suspendisse facilisis nulla
                in
                sapien tincidunt aliquet. Nam imperdiet volutpat ligula, eget blandit dui feugiat sodales. Maecenas
                luctus
                ipsum non purus volutpat auctor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                inceptos himenaeos. Integer tincidunt convallis malesuada. Orci varius natoque penatibus et magnis dis
                parturient montes, nascetur ridiculus mus. Fusce sem odio, eleifend eu aliquam non, ornare ut mi.
                Integer
                consectetur, massa ac volutpat sollicitudin, tellus enim vehicula dui, ut molestie felis enim ut nunc.
                Nulla
                et mauris libero.</p>
        </div>
    </div>
    <?php echo $__env->make('includes.useful_product', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('includes.be_aware_of_sales', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('includes.short_about', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/pages/shipping.blade.php ENDPATH**/ ?>
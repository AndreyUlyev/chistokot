<?php if($paginator->hasPages()): ?>
    <ul class="pagination">
        
        <?php if($paginator->onFirstPage()): ?>
            <li class="disabled"><span>&laquo;</span></li>
        <?php else: ?>
            <li><a href="<?php echo e($elements[0][1]); ?>" rel="prev">&laquo;</a></li>
        <?php endif; ?>

        
        <?php for($i = ($paginator->currentPage() - 3) < 1 ? 1 : ($paginator->currentPage() - 3); $i < $paginator->currentPage(); $i++): ?>
            <li><a href="<?php echo e($paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i); ?>"><?php echo e($i); ?></a></li>
        <?php endfor; ?>

        
        <li class="active"><span><?php echo e($paginator->currentPage()); ?></span></li>

        
        <?php for($i = $paginator->currentPage() + 1; $i <= (($paginator->currentPage() + 3) > $paginator->lastPage() ? $paginator->lastPage() : $paginator->currentPage() + 3); $i++): ?>
            <li><a href="<?php echo e($paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i); ?>"><?php echo e($i); ?></a></li>
        <?php endfor; ?>

        
        <?php if($paginator->hasMorePages()): ?>
            <li><a href="<?php echo e(end($elements)[$paginator->lastPage()]); ?>" rel="next">&raquo;</a></li>
        <?php else: ?>
            <li class="disabled"><span>&raquo;</span></li>
        <?php endif; ?>
    </ul>
<?php endif; ?>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/admin/pagination/default.blade.php ENDPATH**/ ?>
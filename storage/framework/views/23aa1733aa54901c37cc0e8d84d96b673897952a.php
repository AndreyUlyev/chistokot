<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <title>Чистокот админпанель</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="<?php echo e(asset("css/admin/vendor.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("css/admin/elephant.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("css/admin/application.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("css/admin/custom.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("css/style.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("/css/admin/categories.css")); ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/fea7d5cbbb.js" crossorigin="anonymous"></script>
</head>
<body class="layout layout-header-fixed">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="<?php echo e(asset("js/admin/vendor.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/admin/elephant.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/admin/application.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/admin/custom.js")); ?>"></script>
<script src='<?php echo e(asset("js/admin/cropper.js")); ?>'></script>
<script src='<?php echo e(asset("js/custom_modal.js")); ?>'></script>
<script src='<?php echo e(asset("js/admin/charts/charts.js")); ?>'></script>
<?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>
<div class="layout-header">
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <a class="navbar-brand navbar-brand-center" href="<?php echo e(route("admin.main")); ?>">
                <img src="/images/logo_admin.png" alt="" class="logo-admin-chisticot">
            </a>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
                <span class="sr-only">Toggle navigation</span>
                <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
                <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
            </button>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="arrow-up"></span>
                <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="img/0180441436.jpg" alt="Teddy Wilson">
            </span>
            </button>
        </div>
        <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">
                <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
                </button>
            </nav>
        </div>
    </div>
</div>
<div class="layout-main">
    <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
            <div class="custom-scrollbar">
                <nav id="sidenav" class="sidenav-collapse collapse">
                    <ul class="sidenav">
                        <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="sidenav-item
                                <?php if(explode('.', Route::currentRouteName())[1] === explode('.', $item["route"])[1]): ?>)
                                    active
                                <?php endif; ?>
                            ">
                                <a href="<?php echo e(route($item["route"])); ?>" aria-haspopup="true">
                                    <span class="sidenav-icon icon <?php echo e($item["ico"]); ?>"></span>
                                    <span class="sidenav-label"><?php echo e($item["name"]); ?></span>
                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <?php $__env->startSection('content'); ?>
                <div class="title-bar">
                    <h1 class="title-bar-title">
                        <span class="d-ib">Админпанель</span>
                    </h1>
                </div>

                <?php echo $__env->make('includes.charts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->yieldSection(); ?>
        </div>
    </div>
</div>
</body>
</html>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/layouts/admin_app.blade.php ENDPATH**/ ?>
<!--Header start-->

<section class="section-info">
    <div class="container numbers">
        <a class="number1" href="tel:+74955178871">8 (495) 517-88-71</a>
        <a class="number2" href="tel:+74955064952">8 (495)506-49-52</a>
    </div>
</section>
<section class="content-section menu-bar">
    <div class="container header-flexbox header_text">
        <div class="col-md-5 headerLinks">
            <a class="aboutUs" href="<?php echo e(route('contact')); ?>">Контакты</a>
            <a class="delivery" href="<?php echo e(route('shipping')); ?>">Доставка и оплата</a>
            <a class="present" href="<?php echo e(route('news.index')); ?>">Новости</a>
        </div>
        <a class="col-md-2 header-logo" href="<?php echo e(route("mainPage")); ?>">
            <img src="<?php echo e(asset("images/logo.png")); ?>" width="280" height="" class="logo">
        </a>
        <div class="col-md-5 icons header_text">
            <?php if(Auth::check()): ?>
                <form class="icon" method="post" action="<?php echo e(route("logout")); ?>">
                    <?php echo csrf_field(); ?>

                    <button type="submit">
                        <img src="<?php echo e(asset("images/icon/logout.png")); ?>">
                    </button>
                </form>
            <?php else: ?>
                <a href="#login" rel="nofollow" class="icon modalbox">
                    <img class="" src="<?php echo e(asset("images/icon/login.png")); ?>">
                </a>
            <?php endif; ?>
            <a href="<?php echo e(route("likes.index")); ?>" class="icon">
                <img class="" src="<?php echo e(asset("images/icon/like.png")); ?>">
            </a>
            <a href="<?php echo e(route("baskets.index")); ?>" class="icon basket-icon">
                <img class="" src="<?php echo e(asset("images/icon/basket.png")); ?>">
                <div id="circle-price">
                    <p class=""><?php echo e($countProductsInBasket ?? ''); ?></p>
                </div>
            </a>

        </div>
    </div>
</section>
<section class="section-catalog">
    <div class="container header-flexbox-catalog header_text">
        <div class="dropdown catalog">
            <i class="fas fa-bars"></i>
            <span>Каталог</span>
            <div class="dropdown-child catalog-flexbox">
                <?php $__currentLoopData = $menu["categories"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(route("categories.page", ["slug" => $category->slug])); ?>"
                       data-typeobject="categories"
                       data-id="<?php echo e($category["id"]); ?>"
                    ><?php echo e($category["name"]); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <a href="#">новинки</a>
                <a href="#">популярное</a>
                <a href="#">акции</a>
                <a href="#">хиты продаж</a>
            </div>
        </div>

        <?php $__currentLoopData = $menu["popular_products"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a class="catalog-all" href="#"><?php echo e($products["name"]); ?></a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <div class="colSearch">
            <form action="<?php echo e(route('search')); ?>" method="GET" role="search">
                <?php echo csrf_field(); ?>

                <input type="search" name="search" placeholder="Что ищем?" class="search_form">
                <input type="submit" value="Поиск" class="search_btn">
            </form>
        </div>
    </div>
</section>
<section class="mobile-padding"></section>
<section class="mobile-menu">
    <div class="left-nav">
        <div class="menuBtn">
            <div class="mobileWrapper">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>

        </div>
        <div class="mobile-menu-content">

            <div class="menuWrapper">
                <div class="menuDesc">
                    <a href="<?php echo e(route('contact')); ?>">О нас</a>
                    <a href="<?php echo e(route('shipping')); ?>">Доставка и оплата</a>
                    <a href="<?php echo e(route('gifts')); ?>">Подарки</a></div>
                <h2>КАТАЛОГ</h2>
                <div class="catalogList">
                    <?php $__currentLoopData = $menu["categories"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php echo e(route("categories.page", ["slug" => $category->slug])); ?>"
                           data-typeobject="categories"
                           data-id="<?php echo e($category["id"]); ?>"
                        ><?php echo e($category["name"]); ?></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>

        </div>
        <div class="searchBtn">
            <img id="m_search" src="<?php echo e(asset("images/icon/searchB.png")); ?>" alt="">
        </div>
        <div class="searchContainer">
            <input class="searchFormInput" type="text" placeholder="Что ищем?">
            <div class="searchSubmit">
                <img id="m_search_btn" src="<?php echo e(asset("images/icon/searchB.png")); ?>" alt="">
            </div>
        </div>
    </div>
    <div class="right-nav">
        <div class="header_text">
            <?php if(Auth::check()): ?>
                <form method="post" action="<?php echo e(route("logout")); ?>">
                    <?php echo csrf_field(); ?>
                    <input type="submit" value="Выход" id="logout">
                </form>
            <?php else: ?>
                <a href="#login" class="icon">
                    <img id="m_login" class="" src="<?php echo e(asset("images/icon/login.png")); ?>">
                </a>
            <?php endif; ?>
            <a href="<?php echo e(route("likes.index")); ?>" class="icon">
                <img id="m_like" class="" src="<?php echo e(asset("images/icon/like.png")); ?>">
            </a>
            <a href="<?php echo e(route("baskets.index")); ?>" class="icon basket-icon">
                <img id="m_basket" class="" src="<?php echo e(asset("images/icon/basket.png")); ?>">
                <div id="circle-price">
                    <p class=""><?php echo e($countProductsInBasket); ?></p>
                </div>
            </a>
        </div>
    </div>

</section>
<!--Header end-->
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/includes/header.blade.php ENDPATH**/ ?>
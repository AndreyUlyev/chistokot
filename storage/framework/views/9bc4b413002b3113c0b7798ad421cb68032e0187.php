<?php if($paginator->hasPages()): ?>
    <nav>
        <div class="page_navigation">
            
            <?php if(!$paginator->onFirstPage()): ?>
                <a class="page-link" href="<?php echo e($elements[0][1]); ?>">
                    <p class="page_number" aria-label="« Назад">‹</p>
                </a>
            <?php endif; ?>

            
            <?php for($i = ($paginator->currentPage() - 3) < 1 ? 1 : ($paginator->currentPage() - 3); $i < $paginator->currentPage(); $i++): ?>
                <a href="<?php echo e($paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i); ?>"><li class="page_number"><?php echo e($i); ?></li></a>
            <?php endfor; ?>

            
            <li class="active page_number"><span><?php echo e($paginator->currentPage()); ?></span></li>

            
            <?php for($i = $paginator->currentPage() + 1; $i <= (($paginator->currentPage() + 3) > $paginator->lastPage() ? $paginator->lastPage() : $paginator->currentPage() + 3); $i++): ?>
                <a href="<?php echo e($paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i); ?>"><li class="page_number"><?php echo e($i); ?></li></a>
            <?php endfor; ?>

            
            <?php if($paginator->hasMorePages()): ?>
                <a href="<?php echo e($elements[0][$paginator->lastPage()]); ?>" rel="next"><li class="page_number">&raquo;</li></a>
            <?php else: ?>
                <li class="disabled page_number"><span>&raquo;</span></li>
            <?php endif; ?>
        </div>
    </nav>
<?php endif; ?>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/pagination/custom1.blade.php ENDPATH**/ ?>
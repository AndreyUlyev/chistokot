<?php $__env->startSection('content'); ?>
    <div class="basket">
        <div class="basket_first_column">
            <div class="site-path">
                <a href="<?php echo e(route('mainPage')); ?>">Главная</a> / <a href="<?php echo e(route("baskets.index")); ?>">Корзина</a>
            </div>
            <div class="block-head">
                <p class="h2 category__title">Оформление заказа</p>
                <img class="category__wave" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
            </div>

            <?php if($errors->any()): ?>
                <span id="error">
                </span>
            <?php endif; ?>

            <form action="<?php echo e(route('orders.store')); ?>" method="post">
                <?php echo csrf_field(); ?>
                <div class="basket_checkout">
                    <div class="checkout_form">
                        <div class="checkout_form_first_column">
                            <div class="checkout_input">
                                <label for="phone_input" id="phone_input_label">Телефон:</label>
                                <input type="tel" name="phone" placeholder="" id="phone_input">
                            </div>
                            <div class="checkout_input">
                                <label for="city_input">Город:</label>
                                <input type="text" name="address" placeholder="" id="city_input">
                            </div>
                            <div class="checkout_input">
                                <label for="check_shipping_input"> Способ доставки:</label>
                                <select name="delivery_type_id" id="check_shipping_input">
                                    <?php $__currentLoopData = $delivery_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $delivery_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($delivery_type->id); ?>"><?php echo e($delivery_type->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="checkout_form_second_form">
                            <div class="checkout_input">
                                <label for="first_name_input">Имя:</label>
                                <input type="text" value="<?php echo e($user["name"] ?? ''); ?>" name="name" placeholder=""
                                       id="first_name_input">
                            </div>
                            <div class="checkout_input">
                                <label for="second_name_input">Фамилия:</label>
                                <input type="text" name="second_name" value="<?php echo e($user["second_name"] ?? ''); ?>"
                                       placeholder="" id="second_name_input">
                            </div>
                            <div class="checkout_input">
                                <label for="email_input">E-mail:</label>
                                <input type="text" name="email" value="<?php echo e($user["email"] ?? ''); ?>" placeholder=""
                                       id="email_input">
                            </div>
                        </div>
                    </div>
                    <div class="checkout">
                        <div class="checkout_price">
                            <div class="first_column">
                                <h1 class="checkout_price_title"><?php echo e(count($user_products )); ?> товаров на сумму
                                    <input name="total_price" type="hidden" value="<?php echo e($total_price); ?>"/>
                                    <?php echo e($total_price); ?> рублей</h1>
                                <div class="promocode">
                                    <label for="promocode_input">Промокод:</label>
                                    <input type="text" name="promocode" id="promocode_input">
                                </div>
                                <div class="shipping">
                                    <h3>Способ оплаты</h3>
                                    <?php $__currentLoopData = $payment_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$payment_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="form_radio">
                                            <input name="payment_type_id" id="radio-<?php echo e($loop->iteration); ?>" type="radio"
                                                   value="<?php echo e($key); ?>">
                                            <label for="radio-<?php echo e($loop->iteration); ?>"><?php echo e($payment_type["name"]); ?></label>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                            <div class="second_column">
                                <h1 class="checkout_price_title">Корзина</h1>
                                <div class="list_of_producte">
                                    <?php $__currentLoopData = $user_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p>
                                            <?php echo e($loop->iteration); ?>

                                            .<?php echo e($user_product->name); ?> <?php echo e($user_product->quantity); ?>шт.
                                            ................
                                            <?php if($user_product->price_discount != null): ?>
                                                <?php echo e($user_product->price_discount * $user_product->quantity); ?>

                                            <?php else: ?>
                                                <?php echo e($user_product->price * $user_product->quantity); ?>

                                            <?php endif; ?>
                                            руб.
                                            <button id="delFromBasket">
                                                <img src="<?php echo e(asset("images/icon/m_close.png")); ?>">
                                            </button>
                                        </p>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="checkout_button">
                            <div class="checkout_button_first_column">
                                <p>Нажимая на кнопку «Оформить заказ», вы принимаете условия Публичной оферты</p>
                            </div>
                            <?php if(count($user_products) > 0): ?>
                                <div class="checkout_button_second_column">
                                    <input type="submit" value="Оформить заказ"/>
                                </div>
                            <?php endif; ?>
                            <div class="checkout_button_second_column">
                                <input id="destroy_all" type="button" value="Очистить корзину"/>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/baskets/list.blade.php ENDPATH**/ ?>
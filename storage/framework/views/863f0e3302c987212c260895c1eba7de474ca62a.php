<div id="useful_product_modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img class="prod-modal" src="imgs/prod1.png">
                <div id="prod-flexbox2">

                    <div id="prod-name-sale">
                        <p class="h2 category__title">Название товара</p>
                        <a href="#close" title="Close" class="close">×</a>
                    </div>

                    <div id="price-prod" class="price-modal">
                        <div class="new-price-prod">399 РУБ.</div>
                    </div>
                    <p class="about-modal black-color">Текст описания товара. Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Donec
                        faucibus maximus vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a href="" class="to-good">Перейти к товару &#8594;</a><br>
                    <input type="submit" class="inBasket inBasket-modal" value="Добавить корзину">
                    <div class="btns btns-modal">
                        <a href="#useful_product_modal-buyOneClick"><input type="submit" class="buyOneClick buyOneClick-modal" value="Купить в 1 клик"></a>
                        <input type="submit" class="like like-modal" value="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="useful_product_modal-buyOneClick" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Купить в 1 клик</h3>
                <a href="#close" title="Close" class="close">×</a>
            </div>
            <div class="modal-body">
                <div class="good-modal-buyOneClick">
                    <img class="prod-modal-buyOneClick" src="imgs/prod1.png">
                    <div>
                        <div class="name-prod-modal-buyOneClick">Название товара</div>
                        <p>Количество: <span>1</span> шт</p>
                        <p>Цена: <span>14999</span> руб.</p>
                    </div>
                </div>
                <div class="checkout_form">
                    <div class="checkout_input">
                        <label for="name_input-modal-buyOneClick">Ф.И.О.</label>
                        <input type="text" placeholder="Петров Денис Альбертович" id="name_input-modal-buyOneClick">
                    </div>
                    <div class="checkout_input">
                        <label for="phone_input-modal-buyOneClick" id="phone_input_label-modal-buyOneClick">Телефон</label>
                        <input type="text" placeholder="+7 999 888 7766" id="phone_input-modal-buyOneClick">
                    </div>
                    <div class="checkout_input">
                        <label for="email_input-modal-buyOneClick">E-mail</label>
                        <input type="text" placeholder="email@mail.ru" id="email_input-modal-buyOneClick">
                    </div>
                </div>
                <div class="inputs-text"></div>
                <div class="checkout_button_second_column-modal-buyOneClick">
                    <a href="">Оформить</a>
                </div>
            </div>

        </div>
    </div>
</div>
<!--modal end-->

<div class="basket_second_column">
    <div class="h3">это может вам пригодиться</div>
    <a href="#useful_product_modal"  id="open_useful_product_modal">
        <div class="info_item">
            <i class="far fa-heart heart_icon"></i>
            <img src="<?php echo e(asset("images/image prod1.png")); ?>" alt="">
            <p>Название товара</p>
        </div>
    </a>
    <a href="#useful_product_modal"  id="open_useful_product_modal2">
        <div class="info_item">
            <i class="far fa-heart heart_icon"></i>
            <img src="<?php echo e(asset("images/image prod2.png")); ?>" alt="">
            <p>Название товара</p>
        </div>
    </a>
    <a href="#useful_product_modal" id="open_useful_product_modal3">
        <div class="info_item">
            <i class="far fa-heart heart_icon"></i>
            <img src="<?php echo e(asset("images/image prod3.png")); ?>" alt="">
            <p>Название товара</p>
        </div>
    </a>
</div>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/includes/useful_product.blade.php ENDPATH**/ ?>
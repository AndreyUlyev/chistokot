<section class="category__block address-flexbox">
    <div class="container-fluid address-flex">
        <div class="col-md-6 address address__description">
            <div class="block-head">
                <p class="h2 category__title">Адрес магазина</p>
                <img class="category__wave address__description" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
            </div>
            <p class="title-address">Станция метро</p>
            <p class="">Метро "Улица 1905 года"</p>
            <p class="title-address">Адрес магазина</p>
            <p class="">Москва, Звенигородское шоссе, д. 4, торговый центр "Электроника на
                Пресне", желтая линия, стенд Б-12</p>
            <p class="title-address">Режим работы</p>
            <p class="">Ежедневно с 10:00 до 20:00</p>
            <p class="title-address">Как добраться</p>
            <p class="">Метро "Улица 1905 года", первый вагон из центра, выход на левую
                сторону, пройти до пересечения улицы 1905 года со Звенигородским шоссе, повернуть направо и через
                150
                метров с правой стороны вход на территорию торгового центра. Удобный подъезд со стороны нового
                восьмиполосного Звенигородского шоссе обеспечит максимум удобства покупателям, приехавшим на личном
                транспорте.</p>
            <a href="#callback" rel="nofollow" class="modalboxCallBack"><input type="submit" value="Заказать звонок" class="call-back"></a>
        </div>
        <div class="map-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2244.637184209241!2d37.55589611608901!3d55.76480459853347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5497ee2cbec7b%3A0xc68a2ac8c69f2def!2z0K3Qu9C10LrRgtGA0L7QvdC40LrQsCDQvdCwINCf0YDQtdGB0L3QtQ!5e0!3m2!1sru!2sru!4v1570174883201!5m2!1sru!2sru"
                    frameborder="0" style="width:100%;height:100%;" allowfullscreen=""></iframe>
        </div>
    </div>
</section>
<section class="desc-flexbox-section padding-bottom">
    <div class="container desc-flexbox category__block">
        <div class="col-md-6 text-center" id="desc-flexbox1">
            <img src="<?php echo e(asset("images/about.png")); ?>" class="img-about" alt="">
        </div>
        <div class="col-md-6  desc-text" id="desc-flexbox2">
            <div class="block-head about-h">
                <p class="h2 category__title">О компании "Белый кот"</p>
                <img class="category__wave address__description" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
            </div>
            <p class=" about descr_color about-h">Дорогие друзья!<br><br>

                Белый кот приветствует Вас и благодарит за то, что Вы проявили интерес к нашей кампании.
                Компания Белый кот основана в 1998 году.
                Белый кот - это зарегистрированная в Патентном Ведомстве торговая марка. Белый кот - это заслужено популярный бренд, известный миллионам хозяек от Москвы до Владивостока.
                Белый кот - это эффективные и современные средства для поддержания чистоты дома и ухода за телом. Наши средства сохраняют Ваше здоровье, экономят Ваши силы и время.</p>
        </div>
    </div>
</section>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/includes/about.blade.php ENDPATH**/ ?>
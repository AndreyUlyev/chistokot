<?php $__env->startSection('content'); ?>
    <section class="content-section category__block">
        <div class="with-buy">
            Результат поиска
        </div>

        <?php if($error ?? false): ?>
            <div class="review-desc alert alert-success"> <?php echo e($error); ?></div>
        <?php else: ?>
            <div class="review-desc card-header"><b><?php echo e($items->count()); ?> результатов для "<?php echo e(request('search')); ?>"</b></div>
            <div class="product-8 category__block">
                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item item-good" id="product1">
                        <a class="category__good-block-prod"
                           href="<?php echo e($item->route); ?>">
                            <img class="category__circle"
                                 src="<?php echo e($item->image ?? $item->productImages()->first()->link); ?>" alt="">
                        </a>
                        <a class="category__good-title"
                           href="<?php echo e($item->route); ?>">
                            <p><?php echo e($item->name); ?></p>
                        </a>
                        <p class="category__good-description"><?php echo e($item->description); ?></p>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>


            
            

            

            
            

            
            
            
            
            
            

            
            
        <?php endif; ?>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/search/list.blade.php ENDPATH**/ ?>
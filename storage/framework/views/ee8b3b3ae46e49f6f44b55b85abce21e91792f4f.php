<?php $__env->startSection('content'); ?>
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                <?php if($update ?? false): ?>
                    Изменить слайдер
                <?php else: ?>
                    Добавить слайдер
                <?php endif; ?>
            </span>
        </h1>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                <?php if(session('errors')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo e(session('errors')->first()); ?>

                    </div>
                <?php endif; ?>

                <form class="form form-horizontal create_slider" action="<?php echo e($route); ?>" method="post">
                    <input type="hidden" id="update" value="<?php echo e($update ?? false); ?>">
                    <input type="hidden" id="id_slider" value="<?php echo e($slider->id ?? false); ?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-control">Описание</label>
                        <div class="col-sm-9">
                            <textarea id="description" class="form-control"
                                      name="description"><?php echo e($slider["description"] ?? false); ?></textarea>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">Ссылка</label>
                        <div class="col-sm-9">
                            <input id="link" class="form-control" name="link" type="text" value="<?php echo e($slider["link"] ?? false); ?>"
                                   required>
                        </div>

                        <label class="col-sm-3 control-label shift-select-img">Изображения</label>
                        <div class="col-sm-9 form_element">
                            <label id="images_label" class="drive-uploader-btn btn btn-primary btn-lg shift-select-img"
                                   <?php if($slider['image'] ?? false): ?>
                                        disabled
                                   <?php endif; ?>>
                                   Select File
                                   <input class="drive-uploader-input" type="file" name="files" id="files"
                                       <?php if($slider['image'] ?? false): ?>
                                            disabled
                                       <?php endif; ?>/>
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div id="images_list">
                                <?php if($slider['image'] ?? false): ?>
                                    <div class="listImg">
                                        <button id="imgButton" data-id="<?php echo e($slider['image']); ?>" >Удалить</button>
                                        <img src="<?php echo e(asset( $slider['image'])); ?>" width="130px"/>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-9">
                            <label class="btn btn-success file-upload-btn">
                                <?php if($update ?? false): ?>
                                    Изменить
                                <?php else: ?>
                                    Добавить
                                <?php endif; ?>
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo e(asset("js/resources/cropped.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/resources/slider.js")); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/admin/slider/form.blade.php ENDPATH**/ ?>
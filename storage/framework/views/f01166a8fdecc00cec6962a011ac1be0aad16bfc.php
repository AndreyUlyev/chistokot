<?php $__env->startSection('content'); ?>
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                <?php if($update ?? false): ?>
                    Изменить категорию
                <?php else: ?>
                    Добавить категорию
                <?php endif; ?>
            </span>
        </h1>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                <?php if(session('errors')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo e(session('errors')->first()); ?>

                    </div>
                <?php endif; ?>

                <form class="form form-horizontal create_category" action="<?php echo e($route); ?>" method="post">
                    <input type="hidden" id="update" value="<?php echo e($update ?? false); ?>">
                    <input type="hidden" id="id_category" value="<?php echo e($category->id ?? false); ?>">
                    <input type="hidden" id="category_sklad_id" value="<?php echo e($category->moy_sklad_id ?? false); ?>">
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="form-control-1">Название</label>
                        <div class="col-sm-12">
                            <input id="name" class="form-control" name="name" value="<?php echo e($category->name ?? ''); ?>"
                                   type="text">
                        </div>

                        <label class="col-sm-1 control-label">Описание</label>
                        <div class="col-sm-12">
                            <textarea name="description" id="description"
                                      class="form-control form_element"><?php echo e($category->description ?? ''); ?></textarea>
                        </div>

                        <div class="label-for-admin">
                            <p class="col-sm-4 control-label">Добавить на главную</p>
                            <div class="col-xs-4 col-sm-10 shift-checkbox1">
                                <div class="input-group form_element">
                                    <div class="">
                                        <input type="checkbox" value="1" class="" id="is_on_main"
                                               name="is_on_main"
                                               <?php if($category->is_on_main ?? false): ?>
                                               checked="checked"
                                            <?php endif; ?>
                                        >
                                        <label for="is_on_main"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <label class="col-sm-3 control-label">Изображения</label>

                        <div class="col-sm-12">
                            <div id="images_list">
                                <?php if($category['image'] ?? false): ?>
                                    <div class="listImg">
                                        <button id="imgButton" data-id="<?php echo e($category['image']); ?>" >Удалить</button>
                                        <img src="<?php echo e(asset( $category['image'])); ?>" width="130px"/>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="col-sm-4 form_element select-file-button">
                            <label id="images_label" class="drive-uploader-btn btn btn-primary btn-lg"
                                   <?php if($category['image'] ?? false): ?>
                                   disabled
                                <?php endif; ?>>
                                Выбрать файл
                                <input class="drive-uploader-input" type="file" name="files" id="files"
                                       <?php if($category['image'] ?? false): ?>
                                       disabled
                                    <?php endif; ?>/>
                            </label>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-3" align="right">
                            <label class="btn btn-success file-upload-btn shift-select-img shift-img-left">
                                <?php if($update ?? false): ?>
                                    Изменить
                                <?php else: ?>
                                    Добавить
                                <?php endif; ?>
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo e(asset("js/resources/cropped.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/resources/categories.js")); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/admin/categories/form.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>

    <div class="basket">
        <div class="news_first_column">
            <div class="site-path-news">
                <a href="<?php echo e(route('mainPage')); ?>">Главная</a> / <a href="<?php echo e(route('news.index')); ?>">Новости</a>
            </div>
            <div class="news_single_page_first_column">
                <h1 class="shipping_first_column_title" style="margin: 30px 0"><?php echo e($article['name']); ?></h1>
                <p class="shipping_first_column_text"> <?php echo e($article['content']); ?></p>
                <img src="<?php echo e(asset($article->image)); ?>" alt="Картинка новости" class="news_single_page_img">
                <p class="news_single_page_img_dec"><?php echo e($article['image_caption']); ?></p>
                <div class="news_single_page_info">
                    <p><span><?php echo e($article['created_at']); ?></span><span>&nbsp;&#149;&nbsp;</span><span><?php echo e($article->category->name); ?></span></p>
                    <div class="social_link_news_single_page">
                        <p>Поделиться:</p>
                        <a href="" class="social_link_icon_news"><img src="imgs/icon/social/vk.png" alt=""></a>
                        <a href="" class="social_link_icon_news"><img src="imgs/icon/social/f.png" alt=""></a>
                        <a href="" class="social_link_icon_news"><img src="imgs/icon/social/ok.png" alt=""></a>
                        <a href="" class="social_link_icon_news"><img src="imgs/icon/social/viber.png" alt=""></a>
                        <a href="" class="social_link_icon_news"><img src="imgs/icon/social/whatsapp.png" alt=""></a>
                        <a href="" class="social_link_icon_news"><img src="imgs/icon/social/telegram.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="news_second_column">
            <h3 class="shipping_second_column_title">Похожие новости</h3>
            <div class="news_list">
                <div class="info_item">
                    <img src="imgs/page_news2.png" alt="">
                    <p>Новость 2</p>
                </div>
                <div class="info_item">
                    <img src="imgs/page_news3.png" alt="">
                    <p>Новость 3</p>
                </div>
                <div class="info_item">
                    <img src="imgs/page_news4.png" alt="">
                    <p>Новость 3</p>
                </div>
            </div>
        </div>
    </div>

    <?php echo $__env->make('includes.subscription_sale', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('includes.short_about', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/articles/page.blade.php ENDPATH**/ ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>"/>
    <title>Белый Кот - Сайт официального дистрибьютора</title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Playfair+Display&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset("css/animate.min.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("css/jquery.fancybox.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("/css/style.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("/css/style3.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("/css/media.css")); ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>
</head>

<body>
<div id="app">
    <!--modal start-->
    <div id="login">
        <?php echo $__env->make("auth.login", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <div id="registration">
        <?php echo $__env->make("auth.register", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <div id="conditions">
        <h3 class="modal-title">Условия акции</h3>
        <form id="f_conditions" name="conditions" action="#" method="post">
            <div class="modal-body">
                Подпишитесь на наши новости, привязав свою электронную почту и получите промокод на 500 рублей
                для покупок в нашем магазине в подарок!
            </div>
            <div class="btn-modal-ok">
                <input id="do_conditions" type='submit' class="btn-modal empty-ok" value='Принять'>
            </div>
        </form>
    </div>
    <div id="news-cell">
        <h3 class="modal-title">Подписка</h3>
        <form id="f_news-cell" name="news-cell" action="#" method="post">
            <div class="modal-body">
                Вы успешно оформили подписку на наши новости, ваш промокод был отправлен на электронную почту!
            </div>
            <div class="btn-modal-ok">
                <input id="do_news-cell" type='submit' class="btn-modal empty-ok" value='Принять'>
            </div>
        </form>
    </div>

    <div id="callback">
        <h3 class="modal-title">Заказать звонок</h3>
        <form id="f_callback" name="callback" action="#" method="post">
            <div class="modal-body">
                <input class="call-back-form" placeholder="Номер телефона">
                <textarea class="call-back-form" id="call-form-com" placeholder="Комментарий"></textarea>
                <p id="com">* укажите в какое время вам удобно принять звонок</p>
            </div>
            <div class="btn-modal-ok">
                <input id="do_callback" type='submit' class="btn-modal empty-ok" value='Заказать'>
            </div>
        </form>
    </div>
    <!--modal end-->

    <?php echo $__env->make('includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->yieldContent('content'); ?>

    <?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="https://kit.fontawesome.com/8e8f82ea31.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src='<?php echo e(asset("js/jquery-ui.js")); ?>'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
<script src='<?php echo e(asset("js/main.js")); ?>'></script>



<script type="text/javascript" src="<?php echo e(asset("js/jquery.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("js/jquery.fancybox.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("js/fancybox_my.js")); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset("js/app.js")); ?>"></script>
</html>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/layouts/app.blade.php ENDPATH**/ ?>
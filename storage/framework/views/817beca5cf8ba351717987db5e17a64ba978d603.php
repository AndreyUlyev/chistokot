<?php $__env->startSection('content'); ?>
    <main-slider-component :slides="<?php echo e(json_encode($sliders)); ?>"></main-slider-component>

    <section class="content-section">
        <div class="container justify-content-center">

            <div class="text-center category__block description-carousel">
                <p class="h2 category__title">Категории</p>
                <img class="category__wave" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
                <p class="category__description">Ознакомтесь с нашим огромным выбором товаров!</p>
            </div>



        <categories-slider-component :categories="<?php echo e(json_encode($categories)); ?>"></categories-slider-component>

        <div class="text-center category__link-block follow-to-catalog">
            <a href="<?php echo e(route("categories.index")); ?>">
                    Перейти к каталогу <img src="<?php echo e(asset("images/a-main-page.png")); ?>" alt="" style="max-width: 5px;">
            </a>
        </div>

        <div class="text-center category__block description-carousel">
            <p class="h2 category__title">Популярные товары</p>
            <img class="category__wave" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
            <p class="category__description">Самые популярные товары, которые зарекомендовали себя у наших клиентов!</p>
        </div>
        <products-slider-component :products="<?php echo e(json_encode($popular_products)); ?>"></products-slider-component>
        <div class="text-center category__link-block follow-to-catalog">
            <a href="#"><p>Перейти к каталогу <img src="<?php echo e(asset("images/a-main-page.png")); ?>" alt=""></p></a>
        </div>
        </div>
    </section>

    <section class="container content-section promo justify-content-center">
        <div class="text-center category__block">
            <p class="h2 category__title">Акции</p>
            <img class="category__wave" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
            <p class="category__description">Мы всегда производим акции на все товары в нашем ассортименте!</p>
        </div>
        <div class="sale-flexbox category__block">
            <?php $__currentLoopData = $promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="<?php echo e(route('news.show', $promotion['id'])); ?>" class="one-sale">
                    <p><?php echo e($promotion['name']); ?></p>
                    <img class="sale-img" src="<?php echo e(asset($promotion['image'])); ?>" alt="">
                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>














        </div>
    </section>

    <section>
        <?php echo $__env->make('includes.subscription_sale', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>

    <section class="content-section">
        <div class="container justify-content-center">
            <?php $__currentLoopData = $categories_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="text-center category__block">
                    <p class="h2 category__title"><?php echo e($category_info['name']); ?></p>
                    <img class="category__wave" src="<?php echo e(asset("images/wave.png")); ?>" alt="">
                </div>

                <product-category-slider-component :products="<?php echo e(json_encode($category_info->products)); ?>"></product-category-slider-component>

                <div class="text-center category__link-block follow-to-catalog">
                    <a href="#">
                        <p>
                            Перейти к категории <img src="<?php echo e(asset("images/a-main-page.png")); ?>" alt=""
                                                     style="max-width: 5px;">
                        </p>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

    </section>
    <?php echo $__env->make('includes.about', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/pages/mainPage.blade.php ENDPATH**/ ?>
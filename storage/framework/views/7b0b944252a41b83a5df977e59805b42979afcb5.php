<?php $__env->startSection('content'); ?>
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                <?php if($update ?? false): ?>
                    Изменить статью
                <?php else: ?>
                    Добавить статью
                <?php endif; ?>
            </span>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                <?php if(session('errors')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo e(session('errors')->first()); ?>

                    </div>
                <?php endif; ?>

                <form class="form form-horizontal create_article" action="<?php echo e($route); ?>" method="post">
                    <input type="hidden" id="update" value="<?php echo e($update ?? false); ?>">
                    <input type="hidden" id="id_article" value="<?php echo e($article->id ?? false); ?>">
                    <div class="form-group">

                        <div class="label-for-admin">
                        <p class="col-sm-3 control-label">Добавить на главную*</p>
                        <div class="col-xs-4 col-sm-10 shift-checkbox1 shift-checkbox2">
                            <div class="input-group form_element">
                                <div class="">
                                    <input type="checkbox" value="1" class="form-control" id="is_promotion"
                                           name="is_promotion"
                                           <?php if($article["is_promotion"] ?? false): ?>
                                           checked="checked"
                                        <?php endif; ?>
                                    >
                                    <label for="is_promotion"></label>
                                </div>
                            </div>
                        </div>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">Название*</label>
                        <div class="col-sm-9">
                            <input id="name" class="form-control" name="name" value="<?php echo e($article["name"] ?? ''); ?>" type="text" >
                        </div>

                        <label class="col-sm-3 control-label" for="form-control">Содержание*</label>
                        <div class="col-sm-9">
                            <textarea id="content" class="form-control"
                                      name="content"><?php echo e($article["content"] ?? false); ?></textarea>
                        </div>

                        <label class="col-sm-3 control-label">Категория*</label>
                        <div class="col-xs-4 col-sm-3">
                            <select class="custom-select" name="category_id" id="category" >
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option  <?php if($update ?? false): ?>
                                                <?php if(old('category_id', $article->category_id) == $category["id"]): ?>
                                                    selected="selected"
                                                <?php endif; ?>
                                             <?php endif; ?> value="<?php echo e($category["id"]); ?>" ><?php echo e($category["name"]); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <label class="col-sm-3 control-label shift-products">Продукты*</label>
                        <div class="col-xs-4 col-sm-3">
                            <select class="custom-select js-example-basic-multiple" multiple="multiple" name="products" id="products" >
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <optgroup label="<?php echo e($category['name']); ?>">
                                        <?php $__currentLoopData = $category->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option
                                                    <?php if($update ?? false): ?>
                                                        <?php if( in_array($product->id, $chosen_products)): ?>
                                                            selected="selected"
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                        id="product_id" name="product_id" value="<?php echo e($product["id"]); ?>"><?php echo e($product["name"]); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <label class="col-sm-3 control-label">Изображения*</label>
                        <div class="col-sm-9 form_element">
                            <label id="images_label" class="drive-uploader-btn btn btn-primary btn-lg"
                                   <?php if($article['image'] ?? false): ?>
                                   disabled
                                <?php endif; ?>>
                                Выбрать файл
                                <input class="drive-uploader-input" type="file" name="files" id="files"
                                       <?php if($article['image'] ?? false): ?>
                                       disabled
                                    <?php endif; ?>/>
                            </label>
                        </div>

                        <div class="col-sm-10">
                            <div id="images_list">
                                <?php if($article['image'] ?? false): ?>
                                    <div class="listImg">
                                        <button id="imgButton" data-id="<?php echo e($article['image']); ?>" >Удалить</button>
                                        <img src="<?php echo e(asset( $article['image'])); ?>" width="130px"/>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">Подпись к картинке</label>
                        <div class="col-sm-9">
                            <input id="image_caption" class="form-control" name="image_caption" value="<?php echo e($article["image_caption"] ?? ''); ?>" type="text" >
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-12" align="right">
                            <label class="btn btn-success file-upload-btn">
                                <?php if($update ?? false): ?>
                                    Изменить
                                <?php else: ?>
                                    Добавить
                                <?php endif; ?>
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo e(asset("js/resources/cropped.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/resources/articles.js")); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/admin/articles/form.blade.php ENDPATH**/ ?>
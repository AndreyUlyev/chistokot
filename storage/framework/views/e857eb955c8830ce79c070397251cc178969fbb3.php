<?php $__env->startSection('content'); ?>

    <div class="news_page">
        <div class="site-path-news">
            <a href="<?php echo e(route('mainPage')); ?>">Главная</a> / <a href="<?php echo e(route('news.index')); ?>">Новости</a>
        </div>
        <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="news">
                <img src="<?php echo e($article->image); ?>" alt="Картинка новости">
                <div class="news_text">
                    <a class="news_text_title" href="<?php echo e(route('news.show', $article->id)); ?> "><?php echo e($article->name); ?></a>
                    <p class="news_text_info"><span><?php echo e($article->created_at); ?></span><span>&#149;</span><span><?php echo e($article->category->name); ?></span></p>
                    <p class="news_text_desc"><?php echo e($article->content); ?></p>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php echo $__env->make('includes.subscription_sale', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('includes.short_about', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/articles/list.blade.php ENDPATH**/ ?>
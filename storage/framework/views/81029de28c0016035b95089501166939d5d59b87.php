<section class="content-section category__block">
    <div class="with-buy">
        С этим часто покупают
    </div>
    <div class="product-8">
        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="item item-good" id="product1">
                <a class="category__good-block-prod"
                   href="<?php echo e($product->route); ?>"><img class="category__good-prod"
                                                                                        src="<?php echo e(asset($product->productImages()->first()['link'])); ?>"
                                                                                        alt=""></a>
                <a class="category__good-title" href="<?php echo e($product->route); ?>"><?php echo e($product->name); ?></a>
                <p class="category__good-description"><?php echo e($product->description); ?></p>
                <p class="category__good-price"><?php echo e($product->price_discount	?? $product->price); ?> РУБ.</p>
                <input type="submit" class="buy-btn" value="Купить">
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</section>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/includes/with_buy.blade.php ENDPATH**/ ?>
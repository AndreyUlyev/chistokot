<h3 class="modal-title">Вход</h3>
<form id="f_login" name="login" action="<?php echo e(route('login')); ?>" method="POST">
    <?php echo csrf_field(); ?>

    <div class="modal-body">
        <input id="email" type="email" class="call-back-form form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
               name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus
               placeholder="Email">
        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

        <input id="password" type="password"
               class="call-back-form form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password"
               required autocomplete="current-password" placeholder="Пароль">
        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

    </div>
    <div>
        <input id="do_login" type='submit' class="btn-modal login" value='Войти'>
        <a class="forgotpass" href="<?php echo e(route('password.request')); ?>">Забыли пароль?</a>
    </div>
    <div class="registration">
        <a href="#registration" rel="nofollow" class="registration modalbox">Регистрация на сайте</a>
    </div>
</form>
<?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/auth/login.blade.php ENDPATH**/ ?>
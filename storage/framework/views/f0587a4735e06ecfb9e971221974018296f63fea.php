<?php $__env->startSection('content'); ?>

    <div class="container site-path-all-categories">
        <a href="<?php echo e(route('mainPage')); ?>" class="breadcrumb-main-link">Главная</a> / <a href="<?php echo e(route('categories.index')); ?>" class="active-breadcrumb">Категории</a>
    </div>
    <div class="container">
        <div class="col-md-3">
            <div class="type_filter">
                <div class="filter_title first_filter">
                    <h3>Категории <span class="angle_icon angle_icon_rotate" id="icon_rotate"> <img
                                src="<?php echo e(asset("images/icon/angle.png")); ?>" alt=""> </span>
                    </h3>
                </div>
                <ul class="filter_options">
                    <?php $__currentLoopData = $all_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <div class="filter_options_item ">
                                <hr class="filter_options_item_line">
                                <a href="<?php echo e(route("categories.page", ["slug" => $category->slug])); ?>"
                                   class="filter_options_item_title"><?php echo e($category->name); ?></a>
                                <p class="filter_options_item_number"><?php echo e($category->products()->count()); ?></p>
                            </div>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="banner_categories">
                <img src="<?php echo e(asset("images/banner_categories.png")); ?>" alt="баннер категорий">
                <div class="banner_categories_text">
                    <h1>Скидка 20%</h1>
                    <p>Текст описания первого слайда на главной странице. Lorem ipsum dolor sit amet, consectetur
                        adipiscing
                        elit. Donec faucibus maximus vehicula.</p>
                    <a href="">Перейти</a>
                </div>
            </div>

            <h3 class="all_categories_title">Все категории</h3>
            <div class="row">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 categories_item">
                        <a href=" <?php echo e(route("categories.page", ["slug" => $category->slug])); ?> ">
                            <img src="<?php echo e($category->image); ?>" alt="Картинка категории">
                            <p class="shop_item_title"><?php echo e($category->name); ?></p>
                        </a>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="dataTables_paginate" id="demo-dynamic-tables-2_paginate">
                        <?php echo e($categories->links('pagination.custom1')); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--news start-->
    <section>
        <?php echo $__env->make('includes.subscription_sale', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>
    <!--news end-->
    <?php echo $__env->make('includes.short_about', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/category/list.blade.php ENDPATH**/ ?>
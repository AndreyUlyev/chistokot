<?php $__env->startSection('content'); ?>
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">Статьи</span>
        </h1>
    </div>
    <?php if(session('success')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo e(session('success')); ?>

        </div>
    <?php endif; ?>
    <a href="<?php echo e(route("admin.articles.create")); ?>" class="btn btn-primary btn-lg btn-warning">Добавить статью</a>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables-1" class="table table-middle nowrap">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>Акция</th>
                                <th>Название статьи</th>
                                <th>Содержание статьи</th>
                                <th>Дата создания</th>
                                <th>Дата изменения</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <?php echo e($loop->iteration + ($articles->currentpage() - 1) * $articles->perpage()); ?>

                                    </td>
                                    <td>
                                        <strong><?php echo e($article["is_promotion"]); ?></strong>
                                    </td>
                                    <td>
                                        <strong><?php echo e($article["name"]); ?></strong>
                                    </td>
                                    <td>
                                        <p><?php echo e($article["content"]); ?></p>
                                    </td>
                                    <td class="maw-320">
                                        <strong><?php echo e($article["created_at"]); ?> </strong>
                                    </td>
                                    <td>
                                        <?php echo e($article["updated_at"]); ?>

                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true"
                                                    data-toggle="dropdown" type="button">
                                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="<?php echo e(route("admin.articles.edit", ["id_article" => $article["id"]])); ?>">Изменить</a>
                                                </li>
                                                <li>
                                                    <a href="" class="delete_btn">
                                                        Удалить
                                                        <form class="hidden_form" method="post"
                                                              action="<?php echo e(route("admin.articles.destroy", ["id_article" => $article["id"]])); ?>">
                                                            <?php echo csrf_field(); ?>
                                                            <?php echo method_field("DELETE"); ?>
                                                        </form>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="dataTables_paginate paging_simple_numbers" id="demo-dynamic-tables-2_paginate">
                                <?php echo e($articles->links('admin.pagination.default')); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\Chistokot\resources\views/admin/articles/list.blade.php ENDPATH**/ ?>
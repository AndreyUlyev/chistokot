var Ziggy = {
    namedRoutes: {
        "ajax_login": {"uri": "ajax_login", "methods": ["POST"], "domain": null},
        "login": {"uri": "login", "methods": ["GET", "HEAD"], "domain": null},
        "logout": {"uri": "logout", "methods": ["POST"], "domain": null},
        "register": {"uri": "register", "methods": ["GET", "HEAD"], "domain": null},
        "password.request": {"uri": "password\/reset", "methods": ["GET", "HEAD"], "domain": null},
        "password.email": {"uri": "password\/email", "methods": ["POST"], "domain": null},
        "password.reset": {"uri": "password\/reset\/{token}", "methods": ["GET", "HEAD"], "domain": null},
        "password.update": {"uri": "password\/reset", "methods": ["POST"], "domain": null},
        "home": {"uri": "\/", "methods": ["GET", "HEAD"], "domain": null},
        "sitemap": {"uri": "yml", "methods": ["GET", "HEAD"], "domain": null},
        "blocks.popular_products": {"uri": "blocks\/popular_products", "methods": ["GET", "HEAD"], "domain": null},
        "blocks.products_category": {
            "uri": "blocks\/products_category\/{id}",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "contact": {"uri": "page\/contact", "methods": ["GET", "HEAD"], "domain": null},
        "shipping": {"uri": "page\/shipping", "methods": ["GET", "HEAD"], "domain": null},
        "gifts": {"uri": "page\/gifts", "methods": ["GET", "HEAD"], "domain": null},
        "price_list": {"uri": "page\/price_list", "methods": ["GET", "HEAD"], "domain": null},
        "cooperation": {"uri": "page\/cooperation", "methods": ["GET", "HEAD"], "domain": null},
        "job_offers": {"uri": "page\/job_offers", "methods": ["GET", "HEAD"], "domain": null},
        "purchase_rules": {"uri": "page\/purchase_rules", "methods": ["GET", "HEAD"], "domain": null},
        "search": {"uri": "search", "methods": ["GET", "HEAD"], "domain": null},
        "payments.success": {"uri": "payments\/success", "methods": ["GET", "HEAD"], "domain": null},
        "payments.error": {"uri": "payments\/error", "methods": ["GET", "HEAD"], "domain": null},
        "orders.store": {"uri": "order", "methods": ["POST"], "domain": null},
        "orders.by_one_click": {"uri": "order\/one_click", "methods": ["POST"], "domain": null},
        "user.user.edit": {"uri": "user\/edit", "methods": ["GET", "HEAD"], "domain": null},
        "news.index": {"uri": "news", "methods": ["GET", "HEAD"], "domain": null},
        "news.show": {"uri": "news\/{id}", "methods": ["GET", "HEAD"], "domain": null},
        "categories.index": {"uri": "categories", "methods": ["GET", "HEAD"], "domain": null},
        "categories.products": {"uri": "categories\/products", "methods": ["GET", "HEAD"], "domain": null},
        "categories.filters": {"uri": "categories\/filters\/{slug}", "methods": ["GET", "HEAD"], "domain": null},
        "categories.page": {"uri": "categories\/{slug}", "methods": ["GET", "HEAD"], "domain": null},
        "products.page": {"uri": "products\/{slug}\/{size?}\/{color?}", "methods": ["GET", "HEAD"], "domain": null},
        "likes.index": {"uri": "likes", "methods": ["GET", "HEAD"], "domain": null},
        "likes.store": {"uri": "likes", "methods": ["POST"], "domain": null},
        "likes.destroy": {"uri": "likes", "methods": ["DELETE"], "domain": null},
        "comments.index": {"uri": "comments\/{product_id}", "methods": ["GET", "HEAD"], "domain": null},
        "comments.store": {"uri": "comments\/{product_id}", "methods": ["POST"], "domain": null},
        "baskets.index": {"uri": "baskets", "methods": ["GET", "HEAD"], "domain": null},
        "baskets.get_data": {"uri": "baskets\/data", "methods": ["GET", "HEAD"], "domain": null},
        "baskets.update": {"uri": "baskets\/{product_id}", "methods": ["PUT"], "domain": null},
        "baskets.store": {"uri": "baskets", "methods": ["POST"], "domain": null},
        "baskets.destroy": {"uri": "baskets\/{product_id?}", "methods": ["DELETE"], "domain": null},
        "admin.main": {"uri": "admin", "methods": ["GET", "HEAD"], "domain": null},
        "admin.charts.orders_for_registered_or_not": {
            "uri": "admin\/charts\/orders_for_registered_or_not",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.charts.avg_orders_sum": {
            "uri": "admin\/charts\/avg_orders_sum",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.charts.statistics_per_month": {
            "uri": "admin\/charts\/statistics_per_month",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.products.index": {"uri": "admin\/products", "methods": ["GET", "HEAD"], "domain": null},
        "admin.products.create": {"uri": "admin\/products\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.products.store": {"uri": "admin\/products", "methods": ["POST"], "domain": null},
        "admin.products.show": {"uri": "admin\/products\/{id_product}", "methods": ["GET", "HEAD"], "domain": null},
        "admin.products.edit": {
            "uri": "admin\/products\/{id_product}\/edit",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.products.update": {"uri": "admin\/products\/{id_product}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.products.destroy": {"uri": "admin\/products\/{id_product}", "methods": ["DELETE"], "domain": null},
        "admin.brands.index": {"uri": "admin\/brands", "methods": ["GET", "HEAD"], "domain": null},
        "admin.brands.create": {"uri": "admin\/brands\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.brands.store": {"uri": "admin\/brands", "methods": ["POST"], "domain": null},
        "admin.brands.edit": {"uri": "admin\/brands\/{id_brand}\/edit", "methods": ["GET", "HEAD"], "domain": null},
        "admin.brands.update": {"uri": "admin\/brands\/{id_brand}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.brands.destroy": {"uri": "admin\/brands\/{id_brand}", "methods": ["DELETE"], "domain": null},
        "admin.categories.index": {"uri": "admin\/categories", "methods": ["GET", "HEAD"], "domain": null},
        "admin.categories.create": {"uri": "admin\/categories\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.categories.store": {"uri": "admin\/categories", "methods": ["POST"], "domain": null},
        "admin.categories.edit": {
            "uri": "admin\/categories\/{id_category}\/edit",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.categories.update": {
            "uri": "admin\/categories\/{id_category}",
            "methods": ["PUT", "PATCH"],
            "domain": null
        },
        "admin.categories.destroy": {"uri": "admin\/categories\/{id_category}", "methods": ["DELETE"], "domain": null},
        "admin.comments.index": {"uri": "admin\/comments", "methods": ["GET", "HEAD"], "domain": null},
        "admin.comments.update": {"uri": "admin\/comments\/{id_comment}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.comments.destroy": {"uri": "admin\/comments\/{id_comment}", "methods": ["DELETE"], "domain": null},
        "admin.delivery_types.index": {"uri": "admin\/delivery_types", "methods": ["GET", "HEAD"], "domain": null},
        "admin.delivery_types.create": {
            "uri": "admin\/delivery_types\/create",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.delivery_types.store": {"uri": "admin\/delivery_types", "methods": ["POST"], "domain": null},
        "admin.delivery_types.edit": {
            "uri": "admin\/delivery_types\/{id_delivery_type}\/edit",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.delivery_types.update": {
            "uri": "admin\/delivery_types\/{id_delivery_type}",
            "methods": ["PUT", "PATCH"],
            "domain": null
        },
        "admin.delivery_types.destroy": {
            "uri": "admin\/delivery_types\/{id_delivery_type}",
            "methods": ["DELETE"],
            "domain": null
        },
        "admin.slider.index": {"uri": "admin\/slider", "methods": ["GET", "HEAD"], "domain": null},
        "admin.slider.create": {"uri": "admin\/slider\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.slider.store": {"uri": "admin\/slider", "methods": ["POST"], "domain": null},
        "admin.slider.edit": {"uri": "admin\/slider\/{id_slider}\/edit", "methods": ["GET", "HEAD"], "domain": null},
        "admin.slider.update": {"uri": "admin\/slider\/{id_slider}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.slider.destroy": {"uri": "admin\/slider\/{id_slider}", "methods": ["DELETE"], "domain": null},
        "admin.users.index": {"uri": "admin\/users", "methods": ["GET", "HEAD"], "domain": null},
        "admin.users.create": {"uri": "admin\/users\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.users.store": {"uri": "admin\/users", "methods": ["POST"], "domain": null},
        "admin.users.edit": {"uri": "admin\/users\/{id_user}\/edit", "methods": ["GET", "HEAD"], "domain": null},
        "admin.users.update": {"uri": "admin\/users\/{id_user}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.users.destroy": {"uri": "admin\/users\/{id_user}", "methods": ["DELETE"], "domain": null},
        "admin.users.update_password": {"uri": "admin\/users\/{id_user}\/edit", "methods": ["PUT"], "domain": null},
        "admin.articles.index": {"uri": "admin\/articles", "methods": ["GET", "HEAD"], "domain": null},
        "admin.articles.create": {"uri": "admin\/articles\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.articles.store": {"uri": "admin\/articles", "methods": ["POST"], "domain": null},
        "admin.articles.edit": {
            "uri": "admin\/articles\/{id_article}\/edit",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.articles.update": {"uri": "admin\/articles\/{id_article}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.articles.destroy": {"uri": "admin\/articles\/{id_article}", "methods": ["DELETE"], "domain": null},
        "admin.promocodes.index": {"uri": "admin\/promocodes", "methods": ["GET", "HEAD"], "domain": null},
        "admin.promocodes.create": {"uri": "admin\/promocodes\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.promocodes.store": {"uri": "admin\/promocodes", "methods": ["POST"], "domain": null},
        "admin.promocodes.edit": {
            "uri": "admin\/promocodes\/{id_promocode}\/edit",
            "methods": ["GET", "HEAD"],
            "domain": null
        },
        "admin.promocodes.update": {
            "uri": "admin\/promocodes\/{id_promocode}",
            "methods": ["PUT", "PATCH"],
            "domain": null
        },
        "admin.promocodes.destroy": {"uri": "admin\/promocodes\/{id_promocode}", "methods": ["DELETE"], "domain": null},
        "admin.orders.index": {"uri": "admin\/orders", "methods": ["GET", "HEAD"], "domain": null},
        "admin.orders.create": {"uri": "admin\/orders\/create", "methods": ["GET", "HEAD"], "domain": null},
        "admin.orders.store": {"uri": "admin\/orders", "methods": ["POST"], "domain": null},
        "admin.orders.show": {"uri": "admin\/orders\/{id_order}", "methods": ["GET", "HEAD"], "domain": null},
        "admin.orders.edit": {"uri": "admin\/orders\/{id_order}\/edit", "methods": ["GET", "HEAD"], "domain": null},
        "admin.orders.update": {"uri": "admin\/orders\/{id_order}", "methods": ["PUT", "PATCH"], "domain": null},
        "admin.orders.destroy": {"uri": "admin\/orders\/{id_order}", "methods": ["DELETE"], "domain": null}
    },
    // baseUrl: 'https://chistokot.webdobro.ru/',
    // baseProtocol: 'https',
    // baseDomain: 'chistokot.webdobro.ru',
    baseUrl: 'http://chistokot.org/',
    baseProtocol: 'http',
    baseDomain: 'chistokot.org',
    basePort: false,
    defaultParameters: []
};

if (typeof window.Ziggy !== 'undefined') {
    for (var name in window.Ziggy.namedRoutes) {
        Ziggy.namedRoutes[name] = window.Ziggy.namedRoutes[name];
    }
}

export {
    Ziggy
}

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import route from 'ziggy'
import {Ziggy} from './ziggy'
import VueCsrf from 'vue-csrf';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import Store from './store'
import VModal from 'vue-js-modal'
import vSelect from 'vue-select'

window.Vue = require('vue');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('v-select', vSelect);
Vue.component('comment-list-component', require('./components/CommentListComponent/CommentListComponent').default);
Vue.component('categories-component', require('./components/CategoriesComponent/CategoriesComponent').default);
Vue.component('main-slider-component', require('./components/MainSliderComponent/MainSliderComponent').default);
Vue.component('categories-slider-component', require('./components/CategoriesSliderComponent/CategoriesSliderComponent').default);
Vue.component('product-list-slider-component', require('./components/ProductListSliderComponent/ProductListSliderComponent').default);
Vue.component('product-slider-component', require('./components/ProductSliderComponent/ProductSliderComponent').default);
Vue.component('basket-component', require('./components/BasketComponent/BasketComponent').default);
Vue.component('by-button-component', require('./components/ByButton/ByButton').default);
Vue.component('user-panel-component', require('./components/UserPanelComponent/UserPanelComponent').default);
Vue.component('like-button-component', require('./components/LikesComponent/LikesComponent').default);
Vue.component('by-one-click-component', require('./components/ByOneClickComponent/ByOneClickComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(VModal, {dynamic: true, injectModalsContainer: true});
Vue.use(VueSweetalert2);
Vue.use(VueCsrf, {
    selector: 'meta[name="csrf-token"]',
    attribute: 'content',
});

Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    }
});

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': Vue.csrfToken
};

export const EventBus = new Vue();
Vue.prototype.route = route;

const app = new Vue({
    el: '#app',
    data: Store,
});

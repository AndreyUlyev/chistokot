<div class="basket_checkout_success">
    <div> Номер заказа: <span class="order_number">{{ $order_number }}</span></div>
    <div> Сумма заказа: <span class="order_price">{{ $total_price }}</span> руб.</div>
    <div> Способ оплаты: <span class="order_pay">{{ $payment_type }}</span></div>
    <div> Доставка: <span class="order_shipping">{{ $address }}</span></div>
    @if ($is_payment ?? false)
        <div> Состояние: <span class="order_shipping">Заказ оплачен</span></div>
    @endif
    <p class="more-inf">Подробная информация была выслана на электронную почту указанную в заказе.</p>
    <a href="{{ route("categories.index") }}">
        <input class="go_back_catalog" type="button" value="Вернуться в каталог">
    </a>
</div>

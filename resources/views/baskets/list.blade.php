@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="width100">
            <div class="site-path">
                <a href="{{ route('home') }}">Главная</a> / <a href="{{ route("baskets.index") }}">Корзина</a>
            </div>
            <div class="block-head">
                <p class="h2 category__title">Оформление заказа</p>
                <img class="category__wave" src="{{ asset("images/wave.png") }}" alt="">
            </div>

            <basket-component></basket-component>
        </div>
    </div>
@endsection


@extends('layouts.app')

@section('content')
    <div class="basket">
        <div class="basket_first_column">
            <div class="site-path">
                <a href="{{ route('home') }}">Главная</a> / <a href="{{ route("baskets.index") }}">Корзина</a>
            </div>
            <div class="block-head">
                <p class="h2 category__title">Оформление заказа</p>
                <img class="category__wave" src="imgs/wave.png" alt="">
            </div>
            <div class="basket_checkout_success">
                <p style="text-transform: uppercase;">Ошибка.</p>
                <div>Извините, произошла ошибка. Попробуйте заказать товар еще раз.</div>
                <a href=""> <input class="go_back_catalog" type="submit" value="Вернуться в каталог"></a>
            </div>
        </div>
    </div>
@stop

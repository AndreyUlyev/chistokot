@extends('layouts.app')
@section('content')

    <div class="basket">
        <div class="news_first_column">
            {{  Breadcrumbs:: render("news.show", $article->name)}}
            <div class="news_single_page_first_column">
                <h1 class="shipping_first_column_title" style="margin: 30px 0">{{ $article->name }}</h1>
                <p class="shipping_first_column_text"> {{ $article->content }}</p>
                <img src="{{ asset($article->image) }}" alt="Картинка новости" class="news_single_page_img">
                <p class="news_single_page_img_dec">{{ $article->image_caption }}</p>
                <div class="news_single_page_info">
                    <p>
                        <span>{{ $article->created_at }}</span>
                        <span>&nbsp;&#149;&nbsp;</span>
                        <span><a href="{{ route("categories.page", ["slug" => $article->category->slug]) }}">{{ $article->category->name }}</a></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="news_second_column">
            <h3 class="shipping_second_column_title">Похожие новости</h3>
            <div class="news_list">
                <div class="info_item">
                    <img src="imgs/page_news2.png" alt="">
                    <p>Новость 2</p>
                </div>
                <div class="info_item">
                    <img src="imgs/page_news3.png" alt="">
                    <p>Новость 3</p>
                </div>
                <div class="info_item">
                    <img src="imgs/page_news4.png" alt="">
                    <p>Новость 3</p>
                </div>
            </div>
        </div>
    </div>

    @include('partials.subscription_sale')
    @include('partials.short_about')
@stop


@extends('layouts.app')
@section('content')

    <div class="news_page">
        {{  Breadcrumbs:: render("news.index")}}

        @foreach($articles as $article)
            <div class="news">
                <img src="{{ $article->image }}" alt="Картинка новости">
                <div class="news_text">
                    <a class="news_text_title" href="{{ route('news.show', $article->id) }} ">{{ $article->name }}</a>
                    <p class="news_text_info"><span>{{ $article->created_at }}</span><span>&#149;</span><span>{{ $article->category->name }}</span></p>
                    <p class="news_text_desc">{{ $article->content }}</p>
                </div>
            </div>
        @endforeach
    </div>
    @include('partials.subscription_sale')
    @include('partials.short_about')
@stop

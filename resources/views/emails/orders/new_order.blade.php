@component('mail::message')
Здравствуйте!

На сайте chistokot.ru был оставлен новый заказ!

Номер заказа: {{ $order["id"] }}

Сумма заказа: {{ $order["price"] }}

Способ оплаты: {{ $order["payment_type"] }}

Email покупателя: {{ $order["email_user"] }}

Телефон покупателя: {{ $order["phone_user"] }}

ФИО покупателя: {{ $order["fio_user"] }}

---
Информация о товарах
---

@foreach ($order["products"] as $product)
@component('mail::panel')
Название - {{ $product->name }}

Цена - {{ ($product->price_discount != null) ? $product->price_discount : $product->price  }}

Количество - {{ $product->quantity }}

@if ($product->size)
Модификация - {{ $product->size }}
@endif

@if ($product->color)
Цвет - {{ $product->color }}
@endif

@if ($product->size)
    Модификация - {{ $product->size }}
@endif

@component('mail::button', ['url' => route_product($product)])Страница товара@endcomponent

@endcomponent
---
@endforeach

С уважением,
Команда тех. поддержки
@endcomponent

@component('mail::message')
    # Introduction

    The body of your message.
    <div>
        Price: {{ $order['total_price'] }}
    </div>
{{--    @foreach($order['products'] as $product)--}}
{{--        <strong>{{ $product->name.'   '.$product->price_discount ?? $product->price}} RUB</strong>--}}
{{--    @endforeach--}}


    @component('mail::button', ['url' => '', 'color' => 'success'])
        Продолжить шоппинг!
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent

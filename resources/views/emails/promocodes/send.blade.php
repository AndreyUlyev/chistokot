@component('mail::message')
# Introduction

The body of your message.
<div>
    Промокод: {{ $promocode->name }}
</div>
{{--@component('mail::table')--}}
{{--    | Товар         | Table         | Example  |--}}
{{--    | ------------- |:-------------:| --------:|--}}
{{--    @foreach($order->products as $product)--}}
{{--            <strong>{{ $product->name.'   '.$product->price_discount ?? $product->price}} RUB</strong>--}}
{{--    @endforeach--}}

{{--         $product->name| $product      | $10      |--}}

{{--@endcomponent--}}

@component('mail::button', ['url' => '', 'color' => 'success'])
Начать шоппинг!
@endcomponent

Спасибо,<br>
{{ config('app.name') }}
@endcomponent

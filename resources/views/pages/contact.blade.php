@extends('layouts.app')

@section('content')
<div class="contact_map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2244.637184209241!2d37.55589611608901!3d55.76480459853347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5497ee2cbec7b%3A0xc68a2ac8c69f2def!2z0K3Qu9C10LrRgtGA0L7QvdC40LrQsCDQvdCwINCf0YDQtdGB0L3QtQ!5e0!3m2!1sru!2sru!4v1570174883201!5m2!1sru!2sru"
            frameborder="0" allowfullscreen=""></iframe>
    <div class="contact_info">

        {{  Breadcrumbs:: render("pages.contact")}}

        <div class="block-head">
            <p class="h2 category__title">Контакты</p>
            <img class="category__wave address__description" src="{{ asset("images/wave.png") }}" alt="">
        </div>
        <p class="contact_info_text contact-sec"><span class="contact_info_text_bold">Номера телефонов</span><br>
            <a href="tel:+74955178871" class="text-style-contact">+7 (495) 517-88-71</a><br>
            <a href="tel:+74955064952" class="text-style-contact">+7 (495) 506-49-52</a><br><br>
            <span class="contact_info_text_bold">Электронная почта</span><br>
            <a class="text-style-contact">mail@chistokot.ru</a><br>
        </p>
        <div class="block-head">
            <p class="h2 category__title">Адрес магазина</p>
            <img class="category__wave address__description" src="{{ asset("images/wave.png") }}" alt="">
        </div>
        <div class="contact_info_store_address">
            <p class="contact_info_text"><span class="contact_info_text_bold">Станция метро</span></p>
            <p class="contact_info_text"> Метро "Улица 1905 года"</p>
            <p class="contact_info_text "><span class="contact_info_text_bold">Адрес магазина</span></p>
            <p class="contact_info_text"> Москва, Звенигородское шоссе, д. 4, торговый центр
                "Электроника на Пресне", желтая линия, стенд Б-12</p>
            <p class="contact_info_text"><span class="contact_info_text_bold">Режим работы</span></p>
            <p class="contact_info_text"> Ежедневно с 10:00 до 20:00</p>
            <p class="contact_info_text"><span class="contact_info_text_bold">Как добраться</span></p>
            <p class="contact_info_text"> Метро "Улица 1905 года", первый вагон из центра, выход на
                левую сторону, пройти до пересечения улицы 1905
                года со Звенигородским шоссе, повернуть направо и через 150 метров с правой стороны вход на территорию
                торгового центра. Удобный подъезд со стороны нового восьмиполосного Звенигородского шоссе обеспечит
                максимум
                удобства покупателям, приехавшим на личном транспорте.</p>
        </div>
    </div>
</div>

@endsection

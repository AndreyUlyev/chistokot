@extends('layouts.app')

@section('content')
    <main-slider-component :slides="{{ json_encode($sliders) }}"></main-slider-component>

    <section class="content-section">
        <div class="container justify-content-center">
            <div class="text-center category__block description-carousel">
                <p class="h2 category__title">Категории</p>
                <img class="category__wave" src="{{ asset("images/wave.png")}}" alt="">
                <p class="category__description">Ознакомтесь с нашим огромным выбором товаров!</p>
            </div>
            <categories-slider-component :categories="{{ json_encode($categories) }}"></categories-slider-component>
        </div>
    </section>

    <section class="content-section">
        <div class="container justify-content-center">
            <div class="text-center category__link-block follow-to-catalog">
                <a href="{{ route("categories.index") }}">
                    Перейти к каталогу <img src="{{ asset("images/a-main-page.png") }}" alt="" style="max-width: 5px;">
                </a>
            </div>

            <div class="text-center category__block description-carousel">
                <p class="h2 category__title">Популярные товары</p>
                <img class="category__wave" src="{{ asset("images/wave.png") }}" alt="">
                <p class="category__description">Самые популярные товары, которые зарекомендовали себя у наших
                    клиентов!</p>
            </div>

            <product-list-slider-component route_e="blocks.popular_products"></product-list-slider-component>

            <div class="text-center category__link-block follow-to-catalog">
                <a href="{{ route("categories.index") }}">
                    <p>Перейти к каталогу
                        <img src="{{ asset("images/a-main-page.png") }}" alt="">
                    </p>
                </a>
            </div>
        </div>
    </section>

    @if (count($promotions))
        <section class="container content-section promo justify-content-center">
            <div class="text-center category__block">
                <p class="h2 category__title">Акции</p>
                <img class="category__wave" src="{{ asset("images/wave.png") }}" alt="">
                <p class="category__description">Мы всегда производим акции на все товары в нашем ассортименте!</p>
            </div>
            <div class="sale-flexbox category__block">
                @foreach($promotions as $promotion)
                    <a href="{{ route('news.show', $promotion['id']) }}" class="one-sale">
                        <p>{{ $promotion['name'] }}</p>
                        <img class="sale-img" src="{{ asset($promotion['image']) }}" alt="">
                    </a>
                @endforeach
            </div>
        </section>
    @endif

    <section>
        @include('partials.subscription_sale')
    </section>


    @foreach($categories_info as $category_info)
        <section class="content-section">
            <div class="container justify-content-center">
                <div class="text-center category__block">
                    <p class="h2 category__title">{{ $category_info->name }}</p>
                    <img class="category__wave" src="{{ asset("images/wave.png") }}" alt="">
                </div>

                <product-list-slider-component route_e="blocks.products_category" :category_id="{{ $category_info->id }}"></product-list-slider-component>

                <div class="text-center category__link-block follow-to-catalog">
                    <a href="{{ route("categories.page", ["slug" => $category_info->slug]) }}">
                        <p>
                            Перейти к категории <img src="{{ asset("images/a-main-page.png") }}" alt=""
                                                     style="max-width: 5px;">
                        </p>
                    </a>
                </div>
            </div>

        </section>
    @endforeach

    @include('partials.about')
@endsection

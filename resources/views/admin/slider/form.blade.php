@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить слайдер
                @else
                    Добавить слайдер
                @endif
            </span>
        </h1>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                <form class="form form-horizontal create_slider" action="{{ $route }}" method="post">
                    <input type="hidden" id="update" value="{{ $update ?? false }}">
                    <input type="hidden" id="id_slider" value="{{ $slider->id ?? false }}">
                    <div class="form-group">
                        <div class="row row-slider">
                            <label class="col-sm-3 control-label" for="form-control">Название</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $slider->name ?? false }}" id="name">
                            </div>
                        </div>
                        <div class="row row-slider">
                            <label class="col-sm-3 control-label" for="form-control">Описание</label>
                            <div class="col-sm-9">
                                <textarea id="description" class="form-control"
                                          name="description">{{ $slider->description ?? false}}</textarea>
                            </div>
                        </div>
                        <div class="row row-slider">
                            <label class="col-sm-3 control-label" for="form-control-1">Ссылка</label>
                            <div class="col-sm-9">
                                <input id="link" class="form-control" name="link" type="text" value="{{ $slider->link ?? false}}"
                                       required>
                            </div>
                        </div>
                        <div class="row row-slider">
                                <label class="col-sm-3 control-label shift-select-img">Изображения</label>
                                <div class="col-sm-9 form_element">

                                <label id="images_label" class="drive-uploader-btn btn btn-primary btn-lg shift-select-img"
                                       @if($slider->image ?? false)
                                            disabled
                                       @endif>
                                       Select File
                                       <input class="drive-uploader-input" type="file" name="files" id="files"
                                           @if($slider->image ?? false)
                                                disabled
                                           @endif/>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div id="images_list">
                                @if($slider->image ?? false)
                                    <div class="listImg">
                                        <button id="imgButton" data-id="{{ $slider->image }}" >Удалить</button>
                                        <img src="{{ asset( $slider->image) }}" width="130px"/>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-9">
                            <label class="btn btn-success file-upload-btn">
                                @if($update ?? false)
                                    Изменить
                                @else
                                    Добавить
                                @endif
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset("js/resources/cropped.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/resources/slider.js") }}"></script>
@endsection

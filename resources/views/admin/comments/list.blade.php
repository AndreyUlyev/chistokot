@extends('layouts.admin_app')

@section('content')

    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">Комментарии</span>
        </h1>
    </div>
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <section id="wrapper">
        <div class="content">
            <!-- Tab links -->
            <div class="tabs">
                <button class="tablinks active" data-country="London"><p data-title="London">Одобренные</p></button>
                <button class="tablinks" data-country="Paris"><p data-title="Paris">Неодобренные</p></button>
            </div>

            <!-- Tab content -->
            <div class="wrapper_tabcontent">
                <div id="London" class="tabcontent active">
{{--                    <h3>Одобренные</h3>--}}
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dynamic-tables-1" class="table table-middle nowrap">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th></th>
                                    <th>Имя</th>
                                    <th>Рейтинг</th>
                                    <th>Заголовок</th>
                                    <th>Текст</th>
                                    <th>Дата создания</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($approved_comments as $approved_comment)
                                    <tr>
                                        <td>
                                            {{ $loop->iteration + ($approved_comments->currentpage() - 1) * $approved_comments->perpage() }}
                                        </td>
                                        <td data-order="Jessica Brown">
                                            <strong>{{ $approved_comment["name"] }}</strong>
                                        </td>
                                        <td>
                                            {{ $approved_comment["rating"] }}
                                        </td>
                                        <td class="maw-320">
                                            {{ $approved_comment["title"] }}
                                        </td>
                                        <td>
                                            {{ $approved_comment["comment"] }}
                                        </td>
                                        <td>
                                            {{ $approved_comment["created_at"] }}
                                        </td>
                                        <td>
                                            <div class="btn-group pull-right dropdown">
                                                <button class="btn btn-link link-muted" aria-haspopup="true"
                                                        data-toggle="dropdown" type="button">
                                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="" class="delete_btn">
                                                            Удалить
                                                            <form class="hidden_form" method="post"
                                                                  action="{{ route("admin.comments.destroy", ["id_comment" => $approved_comment["id"]]) }}">
                                                                @csrf
                                                                @method("DELETE")
                                                            </form>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="demo-dynamic-tables-2_paginate">
{{--                                    {{$approved_comments->appends(['approved_comments' => $approved_comments->currentPage()])->links()}}--}}
                                    {{ $approved_comments->render() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="Paris" class="tabcontent">
{{--                    <h3>Неодобренные</h3>--}}
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dynamic-tables-1" class="table table-middle nowrap">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th></th>
                                    <th>Имя</th>
                                    <th>Рейтинг</th>
                                    <th>Заголовок</th>
                                    <th>Текст</th>
                                    <th>Дата создания</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($not_approved_comments as $not_approved_comment)
                                    <tr>
                                        <td>
                                            {{ $loop->iteration + ($not_approved_comments->currentpage() - 1) * $not_approved_comments->perpage() }}
                                        </td>
                                        <td data-order="Jessica Brown">
                                            <strong>{{ $not_approved_comment["name"] }}</strong>
                                        </td>
                                        <td>
                                            {{ $not_approved_comment["rating"] }}
                                        </td>
                                        <td class="maw-320">
                                            {{ $not_approved_comment["	title"] }}
                                        </td>
                                        <td>
                                            {{ $not_approved_comment["comment"] }}
                                        </td>
                                        <td>
                                            {{ $not_approved_comment["created_at"] }}
                                        </td>
                                        <td>
                                            <div class="btn-group pull-right dropdown">
                                                <button class="btn btn-link link-muted" aria-haspopup="true"
                                                        data-toggle="dropdown" type="button">
                                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="{{ route("admin.comments.update", ["id_comment" => $not_approved_comment["id"]]) }}">Изменить</a>
                                                    </li>
                                                    <li>
                                                        <a href="" class="delete_btn">
                                                            Удалить
                                                            <form class="hidden_form" method="post"
                                                                  action="{{ route("admin.comments.destroy", ["id_comment" => $not_approved_comment["id"]]) }}">
                                                                @csrf
                                                                @method("DELETE")
                                                            </form>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="demo-dynamic-tables-2_paginate">
{{--                                    {{$not_approved_comments->appends(['approved_comments' => $not_approved_comments->currentPage()])->links()}}--}}

                                    {{ $not_approved_comments->links('admin.pagination.default') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="{{ asset("js/admin/comments.js") }}"></script>

@endsection

@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить бренд
                @else
                    Добавить бренд
                @endif
            </span>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                <form class="form form-horizontal" action="{{ $route }}" method="post">
                    @csrf
                    @if($update ?? false)
                        @method('PUT')
                    @endif

                    <input type="hidden" name="brand_sklad_id" value="{{ $brand->moy_sklad_id ?? false }}">
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="form-control-1">Название</label>
                        <div class="col-sm-9">
                            <input id="name" class="form-control" name="name" value="{{ $brand->name ?? '' }}" type="text" >
                        </div>

                        <label class="control-label" for="form-control-1"></label>
                        <div class="col-sm-2">
                            <label class="btn btn-success file-upload-btn">
                                @if($update ?? false)
                                    Изменить
                                @else
                                    Добавить
                                @endif
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

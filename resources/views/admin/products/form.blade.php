@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить @if ($parent_product_id == false) продукт @else модификацию @endif
                @else
                    Добавить @if ($parent_product_id == false) продукт @else модификацию @endif
                @endif
            </span>
        </h1>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                @if (!count($categories))
                    <div class="alert alert-danger" role="alert">
                        Для добавления продукта необходимо вначале добавить категорию!
                    </div>
                @elseif(!count($brands))
                    <div class="alert alert-danger" role="alert">
                        Для добавления продукта необходимо вначале добавить бренд!
                    </div>
                @else
                    <form class="form form-horizontal create_product" action="{{ $route }}" method="post">
                        <input type="hidden" id="parent_product_id" value="{{ $parent_product_id }}">
                        <input type="hidden" id="update" value="{{ $update ?? false }}">
                        <input type="hidden" id="id_product" value="{{ $product->id ?? false }}">
                        <input type="hidden" id="product_sklad_id" value="{{ $product->moy_sklad_id ?? false }}">

                        @if($update ?? false)
                            <button class="btn btn-primary del_product" data-id="{{ $product->id }}">
                                Удалить @if($parent_product_id == false) продукт @else модификацию @endif</button>
                        @endif

                        <div class="form-group">
                            @if ($parent_product_id == false)
                                @if($update ?? false)
                                    <label class="col-sm-3 control-label">Модификации</label>
                                    <div class="col-sm-9">
                                        <div class="form_element modifications">
                                            @forelse($modifications as $mod)
                                                <a target="_blank"
                                                   href="{{ route("admin.products.edit", ["id_product" => $mod->id, "parent_product_id" => $product->id]) }}">{{ $mod->name }} {{ $mod->size }} {{ $mod->color }}</a>
                                            @empty
                                                Нет модификаций
                                            @endforelse
                                            <div>
                                                <a href="{{ route("admin.products.create", ["parent_product_id" => $product->id]) }}"
                                                   class="btn btn-success file-upload-btn">Создать модификацию</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <label class="col-sm-3 control-label">Основной товар</label>
                                <div class="col-sm-9">
                                    <div class="form_element modifications">
                                        <a href="{{ route("admin.products.edit", ["id_product" => $parent_product_id]) }}">Страница
                                            товара</a>
                                    </div>
                                </div>
                            @endif

                            <label class="col-sm-3 control-label">Название*</label>
                            <div class="col-sm-9">
                                <input type="text" id="name" value="{{ $product->name ?? '' }}"
                                       class="form-control form_element"/>
                            </div>

                            <label class="col-sm-3 control-label">Описание*</label>
                            <div class="col-sm-9">
                                <textarea id="description"
                                          class="form-control form_element">{{ $product->description ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Цена*</label>
                            <div class="col-xs-4 col-sm-3">
                                <div class="input-group form_element">
                                    <input id="price" class="form-control" name="price"
                                           value="{{ $product->price ?? '' }}" type="number" min="1" step="0.01">
                                    <span class="input-group-addon">рублей</span>
                                </div>
                            </div>

                            <label class="col-sm-3 control-label">Цена со скидкой*</label>
                            <div class="col-xs-4 col-sm-3">
                                <div class="input-group form_element">
                                    <input id="price_discount" class="form-control" name="price_discount"
                                           value="{{ $product->price_discount ?? '' }}" type="number" min="0"
                                           step="0.01">
                                    <span class="input-group-addon">рублей</span>
                                </div>
                            </div>

                            <label class="col-sm-3 control-label">Количество*</label>
                            <div class="col-xs-4 col-sm-3">
                                <div class="input-group form_element">
                                    <input id="quantity" class="form-control" name="quantity"
                                           value="{{ $product->quantity ?? 1 }}" type="number" min="1">
                                    <span class="input-group-addon">штук</span>
                                </div>
                            </div>
                            <div class="label-for-admin">
                                <p class="col-sm-3 control-label">Добавить в популярные*</p>
                                <div class="col-xs-4 col-sm-3 shift-checkbox">
                                    <div class="input-group form_element">
                                        <div class="">
                                            <input type="checkbox" class="form-control" id="is_popular"
                                                   name="is_popular"
                                                   @if($product->is_popular ?? false)
                                                   checked="checked"
                                                @endif>
                                            <label for="is_popular"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <label class="col-sm-3 control-label">Категория*</label>
                            <div class="col-xs-4 col-sm-3">
                                <select class="custom-select" name="category" id="category">
                                    @foreach($categories as $category)
                                        <option
                                            @if(old('category_id', $product->category_id ?? null) == $category["id"]) selected="selected"
                                            @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="col-sm-3 control-label">Бренд*</label>
                            <div class="col-xs-4 col-sm-3">
                                <div class="form_element">
                                    <select class="custom-select" name="brand" id="brand">
                                        @foreach($brands as $brand)
                                            <option
                                                @if(old('brand_id', $product->brand_id ?? null) == $brand["id"]) selected="selected"
                                                @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <label class="col-sm-3 control-label">Meta description</label>
                            <div class="col-sm-9">
                                <textarea id="meta_description"
                                          class="form-control form_element">{{ $product->meta_description ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Meta title</label>
                            <div class="col-sm-9">
                                <textarea id="meta_title"
                                          class="form-control form_element">{{ $product->meta_title ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Meta keywords</label>
                            <div class="col-sm-9">
                                <textarea id="meta_keywords"
                                          class="form-control form_element">{{ $product->meta_keywords ?? '' }}</textarea>
                            </div>


                            <label class="col-sm-3 control-label">Состав</label>
                            <div class="col-sm-9">
                                <textarea id="composition"
                                          class="form-control form_element">{{ $product->composition ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">О доставке</label>
                            <div class="col-sm-9">
                                <textarea id="about_delivery"
                                          class="form-control form_element">{{ $product->about_delivery ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Гарантии</label>
                            <div class="col-sm-9">
                                <textarea id="guarantees"
                                          class="form-control form_element">{{ $product->guarantees ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Применение</label>
                            <div class="col-sm-9">
                                <textarea id="where_apply"
                                          class="form-control form_element">{{ $product->where_apply ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Уход</label>
                            <div class="col-sm-9">
                                <textarea id="maintenance"
                                          class="form-control form_element">{{ $product->maintenance ?? '' }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Страна</label>
                            <div class="col-sm-9 form_element">
                                <select class="custom-select" name="country" id="country">
                                    @foreach($countries as $key => $country)
                                        @if($country != null)
                                            <option value="{{ $key }}"
                                                    @if ($key == ($product->country ?? "RU"))
                                                    selected
                                                @endif
                                            >{{ $country }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>


                            <label class="col-sm-3 control-label">Модификация*</label>
                            <div class="col-xs-4 col-sm-3">
                                <div class="input-group form_element">
                                    <input id="mod" class="form-control" name="quantity"
                                           value="{{ $product["size"] ?? false }}">
                                </div>
                            </div>

                            <label class="col-sm-3 control-label">Цвет*</label>
                            <div class="col-xs-4 col-sm-3">
                                <div class="input-group form_element">
                                    <select id="products_colors">
                                        <option value="0">- Без цвета -</option>
                                        @foreach ($colors as $color)
                                            <option data-color="{{ $color }}" value="{{ $color }}"
                                                    @if ($color == ($product["color"] ?? false))
                                                    selected
                                                @endif
                                            >{{ $color }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="">
                                <label class="col-sm-3 control-label shift-select-img">Изображения</label>
                                <div class="col-sm-9 form_element">
                                    <label class="drive-uploader-btn btn btn-primary btn-lg shift-select-img">
                                        Select Files
                                        <input class="drive-uploader-input" type="file" name="files" id="files">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <div id="images_list">
                                    @foreach($products_images ?? [] as $image)
                                        <div class="listImg">
                                            <button data-id="{{ $image->id }}">Удалить</button>
                                            <img src="{{ asset("/uploads/mini/".$image->link) }}"/>
                                        </div>
                                    @endforeach
                                </div>
                            </div>


                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9 admin-update">
                                <label class="btn btn-success file-upload-btn">
                                    @if($update ?? false)
                                        Изменить
                                    @else
                                        Добавить
                                    @endif
                                    <input class="file-upload-input" type="submit" multiple="">
                                </label>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset("js/resources/cropped.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/resources/products.js") }}"></script>
    <style>
        .ck-content {
            margin-bottom: 5px;
        }
    </style>
@endsection

@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить пользователя
                @else
                    Добавить пользователя
                @endif
            </span>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                <form class="form form-horizontal" action="{{ $route }}" method="post">
                    @csrf
                    @if($update ?? false)
                        @method('PUT')
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-control-1">Имя</label>
                        <div class="col-sm-9">
                            <input id="name" class="form-control" name="name" value="{{ $user["name"] ?? '' }}"
                                   type="text">
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">E-Mail Address</label>
                        <div class="col-sm-9">
                            <input id="email" class="form-control" name="email" value="{{ $user["email"] ?? '' }}"
                                   type="text">
                        </div>

                        @if(!$update ?? false)
                            <label class="col-sm-3 control-label" for="form-control-1">Пароль</label>
                            <div class="col-sm-9">
                                <input id="password" class="form-control" name="password"
                                       type="password">
                            </div>

                            <label class="col-sm-3 control-label" for="form-control-1">Подтвердите пароль</label>

                            <div class="col-md-9">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required autocomplete="new-password">
                            </div>
                        @endif

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-12 submitBtn">
                            <label class="btn btn-success file-upload-btn shift-select-img">
                                @if($update ?? false)
                                    Изменить
                                @else
                                    Добавить
                                @endif
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
                @if($update ?? false)
                    <form class="form form-horizontal" action=" {{ $password_route }} " method="post">
                        @csrf
                        @method('PUT')

                        <label class="col-sm-3 control-label" for="form-control-1">Пароль</label>
                        <div class="col-sm-9">
                            <input id="password" class="form-control" name="password"
                                   type="password">
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">Подтвердите пароль</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" autocomplete="new-password">
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-9 submitBtn" align="right">
                            <label class="btn btn-success file-upload-btn">
                                @if($update ?? false)
                                    Изменить
                                @else
                                    Добавить
                                @endif
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </form>
                @endif

            </div>
        </div>
    </div>
@endsection

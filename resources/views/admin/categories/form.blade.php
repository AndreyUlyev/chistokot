@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить категорию
                @else
                    Добавить категорию
                @endif
            </span>
        </h1>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                <form class="form form-horizontal create_category" action="{{ $route }}" method="post">
                    <input type="hidden" id="update" value="{{ $update ?? false }}">
                    <input type="hidden" id="id_category" value="{{ $category->id ?? false }}">
                    <input type="hidden" id="category_sklad_id" value="{{ $category->moy_sklad_id ?? false}}">
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="form-control-1">Название</label>
                        <div class="col-sm-12">
                            <input id="name" class="form-control" name="name" value="{{ $category->name ?? '' }}"
                                   type="text">
                        </div>

                        <label class="col-sm-1 control-label">Описание</label>
                        <div class="col-sm-12">
                            <textarea name="description" id="description"
                                      class="form-control form_element">{{ $category->description ?? '' }}</textarea>
                        </div>

                        <div class="label-for-admin">
                            <p class="col-sm-4 control-label">Добавить на главную</p>
                            <div class="col-xs-4 col-sm-10 shift-checkbox1">
                                <div class="input-group form_element">
                                    <div class="">
                                        <input type="checkbox" value="1" class="" id="is_on_main"
                                               name="is_on_main"
                                               @if($category->is_on_main ?? false)
                                               checked="checked"
                                            @endif
                                        >
                                        <label for="is_on_main"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <label class="col-sm-3 control-label">Изображения</label>

                        <div class="col-sm-12">
                            <div id="images_list">
                                @if($category['image'] ?? false)
                                    <div class="listImg">
                                        <button id="imgButton" data-id="{{ $category['id'] }}" >Удалить</button>
                                        <img src="{{ asset("/uploads/mini/".$category['image']) }}" width="130px"/>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-4 form_element select-file-button">
                            <label id="images_label" class="drive-uploader-btn btn btn-primary btn-lg"
                                   @if($category['image'] ?? false)
                                   disabled
                                @endif>
                                Выбрать файл
                                <input class="drive-uploader-input" type="file" name="files" id="files"
                                       @if($category['image'] ?? false)
                                       disabled
                                    @endif/>
                            </label>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-3" align="right">
                            <label class="btn btn-success file-upload-btn shift-select-img shift-img-left">
                                @if($update ?? false)
                                    Изменить
                                @else
                                    Добавить
                                @endif
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset("js/resources/cropped.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/resources/categories.js") }}"></script>
@endsection

@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                Информация о заказе #{{ $order->id }}
            </span>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                <div>Идентификатор товара</label>
                <div class="col-sm-9">
                    <strong>{{ $order->id }} </strong>
                </div>

                <div>Имя пользователя</label>
                <div class="col-sm-9">
                    <strong>{{ $order->name.($order->second_name == 0 ? false : '  '.$order->second_name) }} </strong>
                </div>

                <label class="col-sm-3 control-label">Телефон</label>
                <div class="col-sm-9">
                    <strong>{{ $order->phone}} </strong>
                </div>

                <label class="col-sm-3 control-label">
                    Адрес
                </label>
                <div class="col-sm-9">
                    <strong>{{ $order->address }} </strong>
                </div>

                <label class="col-sm-3 control-label">
                    Тип оплаты
                </label>
                <div class="col-sm-9">
                    <strong>{{ config("payments.types.".$order->payment_type_id.".name") }} </strong>
                </div>

                <label class="col-sm-3 control-label">
                    Тип доставки
                </label>
                <div class="col-sm-9">
                    <strong>{{ $order->deliveryType["name"] }} </strong>
                </div>

                <label class="col-sm-3 control-label">
                    Продукты
                </label>
                <div class="col-sm-9">
                    @foreach($order->products as $product)
                        <strong>{{ $product->name.'   '.$product->price_discount ?? $product->price}} RUB</strong>
                    @endforeach
                </div>

                <label class="col-sm-3 control-label">
                    Дата создания
                </label>
                <div class="col-sm-9">
                    <strong>{{ $order['created_at']}} </strong>
                </div>
                <label class="col-sm-3 control-label">
                    Дата изменения
                </label>
                <div class="col-sm-9">
                    <strong>{{ $order['updated_at']}} </strong>
                </div>
                <label class="col-sm-3 control-label">
                    Дата завершения
                </label>
                <div class="col-sm-9">
                    <strong>{{ $order['completion_date']}} </strong>
                </div>

                <div>Продукты</div>
                    @foreach($order->products as $product)
                        <label class="col-sm-3 control-label">
                            Название товара
                        </label>
                        <div class="col-sm-9">
                            <strong>{{ $product['name']}} </strong>
                        </div>

                        <label class="col-sm-3 control-label">
                            Цена товара
                        </label>
                        <div class="col-sm-9">
                            <strong>{{ ($product['price_discount'] != null) ? $product['price_discount'] : $product['price'] }} </strong>
                        </div>

                        <div class="col-sm-9">
                            <a href="{{ route("products.page", ["slug" => $product["slug"]]) }}" target="_blank">Страница товара</a>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
@endsection

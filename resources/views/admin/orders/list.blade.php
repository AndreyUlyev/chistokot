@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">Заказы</span>
        </h1>
    </div>
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables-1" class="table table-middle nowrap">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>Идентификатор</th>
                                <th>Продукт</th>
                                <th>Адрес</th>
                                <th>Телефон</th>
                                <th>Оплачен</th>
                                <th>Дата создания</th>
                                <th>ФИО</th>
                                <th>Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                        {{ $loop->iteration + ($orders->currentpage() - 1) * $orders->perpage() }}
                                    </td>
                                    <td>
                                        {{ $order->id }}
                                    </td>
                                    <td>
                                        @foreach($order->products as $product)
                                            <div>
                                                {{ $product->name }}
                                                @if ($product->size ?? false)
                                                    | Размер - {{ $product->size }}
                                                    @if ($product->color ?? false)
                                                        | Цвет - {{ $product->color }}
                                                    @endif
                                                @endif
                                            </div>
                                        @endforeach
                                    </td>
                                    <td>
                                        <strong>{{ $order->address }}</strong>
                                    </td>
                                    <td>
                                        <strong>{{ $order->phone }}</strong>
                                    </td>
                                    <td>
                                        <strong>
                                            @if($order->isPaid() == true)
                                                Да
                                            @endif
                                        </strong>
                                    </td>
                                    <td class="maw-320">
                                        {{ $order->created_at }}
                                    </td>
                                    <td>
                                        <strong>{{ $order->name }} {{ $order->second_name !=0 ? $order->second_name : false }}</strong>
                                    </td>
                                    <td>
                                        {{ $order->total_price }} руб.
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
{{--                                                <li><a href="{{ route("admin.orders.show", ["id_order" => $order["id"]]) }}">Показать</a></li>--}}
                                                <li>
                                                    <a href="" class="delete_btn">
                                                        Удалить
                                                        <form class="hidden_form" method="post" action="{{ route("admin.orders.destroy", ["id_order" => $order["id"]]) }}">
                                                            @csrf
                                                            @method("DELETE")
                                                        </form>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="dataTables_paginate paging_simple_numbers" id="demo-dynamic-tables-2_paginate">
                                {{ $orders->render() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

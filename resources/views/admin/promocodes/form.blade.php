@extends('layouts.admin_app')

@section('content')


    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить промокод
                @else
                    Добавить промокод
                @endif
            </span>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="demo-form-wrapper promocodeForm">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif
                <form class="form form-horizontal" action="{{ $route }}" method="post">
                    @csrf
                    @if($update ?? false)
                        @method('PUT')
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-control">Название</label>
                        <div class="col-sm-9">
                            <input id="name" class="form-control" name="name" value="{{ $promocode["name"] ?? '' }}"
                                   type="text">
                        </div>

                        <label class="col-sm-3 control-label" for="form-control">Размер скидки</label>
                        <div class="col-sm-9">
                            <div class="input-group form_element">
                                <input id="discount" class="form-control" name="price" value="{{ $promocode["discount"] ?? '' }}"
                                       type="number"
                                       min="1"
                                       max="99"
                                       step="0.1">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>

                        <div>
                            <label class="col-sm-3 control-label">
                                Привязать промокод к категории?
                            </label>
                            <input class="col-sm-1" id="allow_category"
                                @if($promocode->category_id ?? false)
                                checked
                                @endif
                            type="checkbox"/>

                            <label class="col-sm-2 control-label" for="form-control">Выберите категорию:</label>
                            <div class="col-sm-6">
                                <select
                                    @if(!($promocode->category_id ?? false))
                                        disabled
                                    @endif
                                    id="category_select" class="form-control" name="category_id">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}"
                                            @if(($promocode->category_id ?? false) == $category->id)
                                                selected="selected"
                                            @endif
                                        >{{ $category["name"] ?? ''}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div>
                            <label class="col-sm-3 control-label">Привязать промокод к продукту?</label>
                            <input class="col-sm-1" id="allow_product" type="checkbox"
                                @if($promocode->product_id ?? false)
                                    checked
                                @endif
                            />
                            <label class="col-sm-2 control-label" for="form-control">Выберите продукт:</label>
                            <div class="col-sm-6">
                                <select
                                    @if(!($promocode->product_id ?? false))
                                        disabled
                                    @endif
                                    id="product_select" class="form-control" name="product_id">
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}"
                                            @if(($promocode->product_id ?? false) == $product->id)
                                                selected="selected"
                                            @endif
                                        >{{ $product->name.'-'.$product->price }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div>
                            <label class="col-sm-3 control-label">Привязать промокод к бренду?</label>
                            <input class="col-sm-1" id="allow_brand"
                                @if($promocode->brand_id ?? false)
                                    checked
                                @endif
                                type="checkbox"/>
                            <label class="col-sm-2 control-label" for="form-control">Выберите бренд:</label>
                            <div class="col-sm-6">
                                <select
                                    @if(!($promocode->brand_id ?? false))
                                        disabled
                                    @endif
                                    id="brand_select" class="form-control" name="brand_id">
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand->id }}"
                                            @if(($promocode->brand_id ?? false) == $brand->id)
                                                selected="selected"
                                            @endif
                                        >{{ $brand->name ?? ''}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9 submitBtn" align="right">
                                <label class="btn btn-success file-upload-btn">
                                    @if($update ?? false)
                                        Изменить
                                    @else
                                        Добавить
                                    @endif
                                    <input class="file-upload-input" type="submit" multiple="">
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

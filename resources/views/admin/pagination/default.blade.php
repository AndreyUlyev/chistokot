@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Переход на первую --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><span>&laquo;</span></li>
        @else
            <li><a href="{{  $elements[0][1] }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Три страницы влево --}}
        @for ($i = ($paginator->currentPage() - 3) < 1 ? 1 : ($paginator->currentPage() - 3); $i < $paginator->currentPage(); $i++)
            <li><a href="{{ $paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i }}">{{ $i }}</a></li>
        @endfor

        {{-- Текущая страница --}}
        <li class="active"><span>{{ $paginator->currentPage() }}</span></li>

        {{-- Три страницы право --}}
        @for ($i = $paginator->currentPage() + 1; $i <= (($paginator->currentPage() + 3) > $paginator->lastPage() ? $paginator->lastPage() : $paginator->currentPage() + 3); $i++)
            <li><a href="{{ $paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i }}">{{ $i }}</a></li>
        @endfor

        {{-- Переход на последнюю--}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ end($elements)[$paginator->lastPage()] }}" rel="next">&raquo;</a></li>
        @else
            <li class="disabled"><span>&raquo;</span></li>
        @endif
    </ul>
@endif

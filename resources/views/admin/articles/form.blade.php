@extends('layouts.admin_app')

@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
            <span class="d-ib">
                @if($update ?? false)
                    Изменить статью
                @else
                    Добавить статью
                @endif
            </span>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="demo-form-wrapper">
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors')->first() }}
                    </div>
                @endif

                <form class="form form-horizontal create_article" action="{{ $route }}" method="post">
                    <input type="hidden" id="update" value="{{ $update ?? false }}">
                    <input type="hidden" id="id_article" value="{{ $article->id ?? false }}">
                    <div class="form-group">

                        <div class="label-for-admin">
                        <p class="col-sm-3 control-label">Добавить на главную*</p>
                        <div class="col-xs-4 col-sm-10 shift-checkbox1 shift-checkbox2">
                            <div class="input-group form_element">
                                <div class="">
                                    <input type="checkbox" value="1" class="form-control" id="is_promotion"
                                           name="is_promotion"
                                           @if($article["is_promotion"] ?? false)
                                           checked="checked"
                                        @endif
                                    >
                                    <label for="is_promotion"></label>
                                </div>
                            </div>
                        </div>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">Название*</label>
                        <div class="col-sm-9">
                            <input id="name" class="form-control" name="name" value="{{ $article["name"] ?? '' }}" type="text" >
                        </div>

                        <label class="col-sm-3 control-label" for="form-control">Содержание*</label>
                        <div class="col-sm-9">
                            <textarea id="content" class="form-control"
                                      name="content">{{ $article["content"] ?? false}}</textarea>
                        </div>

                        <label class="col-sm-3 control-label">Категория*</label>
                        <div class="col-xs-4 col-sm-3">
                            <select class="custom-select" name="category_id" id="category" >
                                @foreach($categories as $category)
                                    <option  @if($update ?? false)
                                                @if (old('category_id', $article->category_id) == $category["id"])
                                                    selected="selected"
                                                @endif
                                             @endif value="{{ $category["id"] }}" >{{ $category["name"] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <label class="col-sm-3 control-label shift-products">Продукты*</label>
                        <div class="col-xs-4 col-sm-3">
                            <select class="custom-select js-example-basic-multiple" multiple="multiple" name="products" id="products" >
                                @foreach($categories as $category)
                                    <optgroup label="{{ $category['name'] }}">
                                        @foreach($category->products as $product)
                                                <option
                                                    @if($update ?? false)
                                                        @if( in_array($product->id, $chosen_products))
                                                            selected="selected"
                                                        @endif
                                                    @endif
                                                        id="product_id" name="product_id" value="{{ $product["id"]}}">{{ $product["name"] }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>

                        <label class="col-sm-3 control-label">Изображения*</label>
                        <div class="col-sm-9 form_element">
                            <label id="images_label" class="drive-uploader-btn btn btn-primary btn-lg"
                                   @if($article['image'] ?? false)
                                   disabled
                                @endif>
                                Выбрать файл
                                <input class="drive-uploader-input" type="file" name="files" id="files"
                                       @if($article['image'] ?? false)
                                       disabled
                                    @endif/>
                            </label>
                        </div>

                        <div class="col-sm-10">
                            <div id="images_list">
                                @if($article['image'] ?? false)
                                    <div class="listImg">
                                        <button id="imgButton" data-id="{{ $article['image'] }}" >Удалить</button>
                                        <img src="{{ asset( $article['image']) }}" width="130px"/>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1">Подпись к картинке</label>
                        <div class="col-sm-9">
                            <input id="image_caption" class="form-control" name="image_caption" value="{{ $article["image_caption"] ?? '' }}" type="text" >
                        </div>

                        <label class="col-sm-3 control-label" for="form-control-1"></label>
                        <div class="col-sm-12" align="right">
                            <label class="btn btn-success file-upload-btn">
                                @if($update ?? false)
                                    Изменить
                                @else
                                    Добавить
                                @endif
                                <input class="file-upload-input" type="submit" multiple="">
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset("js/resources/cropped.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/resources/articles.js") }}"></script>
@endsection

@if ($paginator->hasPages())
    <nav>
        <div class="page_navigation">
            {{-- Переход на первую --}}
            @if (!$paginator->onFirstPage())
                <p class="page_number" aria-label="« Назад">
                    <a class="page-link" href="{{  $elements[0][1] }}">‹</a>
                </p>
            @endif

            {{-- Три страницы влево --}}
            @for ($i = ($paginator->currentPage() - 3) < 1 ? 1 : ($paginator->currentPage() - 3); $i < $paginator->currentPage(); $i++)
                <li><a href="{{ $elements[0][$i] }}">{{ $i }}</a></li>
            @endfor

            {{-- Текущая страница --}}
            <li class="active"><span>{{ $paginator->currentPage() }}</span></li>

            {{-- Три страницы право --}}
            @for ($i = $paginator->currentPage() + 1; $i <= (($paginator->currentPage() + 3) > $paginator->lastPage() ? $paginator->lastPage() : $paginator->currentPage() + 3); $i++)
                <li><a href="{{ $elements[0][$i] }}">{{ $i }}</a></li>
            @endfor

            {{-- Переход на последнюю --}}
            @if ($paginator->hasMorePages())
                <li><a href="{{ $elements[0][$paginator->lastPage()] }}" rel="next">&raquo;</a></li>
            @else
                <li class="disabled"><span>&raquo;</span></li>
            @endif
        </div>
    </nav>
@endif

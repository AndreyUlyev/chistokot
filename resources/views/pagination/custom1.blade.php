@if ($paginator->hasPages())
    <nav>
        <div class="page_navigation">
            {{-- Переход на первую --}}
            @if (!$paginator->onFirstPage())
                <a  href="{{  $elements[0][1] }}">
                    <li class="page_number" aria-label="« Назад">«</li>
                </a>
            @endif

            {{-- Три страницы влево --}}
            @for ($i = ($paginator->currentPage() - 3) < 1 ? 1 : ($paginator->currentPage() - 3); $i < $paginator->currentPage(); $i++)
                <a href="{{ $paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i }}"><li class="page_number">{{ $i }}</li></a>
            @endfor

            {{-- Текущая страница --}}
            <li class="active page_number"><span>{{ $paginator->currentPage() }}</span></li>

            {{-- Три страницы право --}}
            @for ($i = $paginator->currentPage() + 1; $i <= (($paginator->currentPage() + 3) > $paginator->lastPage() ? $paginator->lastPage() : $paginator->currentPage() + 3); $i++)
                <a href="{{ $paginator->getOptions()["path"]."?".$paginator->getOptions()["pageName"]."=".$i  }}"><li class="page_number">{{ $i }}</li></a>
            @endfor

            {{-- Переход на последнюю --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $elements[0][$paginator->lastPage()] }}" rel="next"><li class="page_number">&raquo;</li></a>
            @else
                <li class="disabled page_number"><span>&raquo;</span></li>
            @endif
        </div>
    </nav>
@endif

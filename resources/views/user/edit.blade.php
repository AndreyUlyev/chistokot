@extends('layouts.app')
@section('content')

    <div class="basket profile_editing">
        <div class="basket_first_column">
            <div class="site-path">
                <a href="{{ route('home') }}">Главная</a> / <a
                    href="{{ route('profile_editing')}}">Личый кабинет</a>
            </div>
            <div class="block-head">
                <p class="h2 category__title">редактирование профиля</p>
                <img class="category__wave" src="imgs/wave.png" alt="">
            </div>
            <div class="basket_checkout">
                <div class="checkout_form">
                    <div class="checkout_form_first_column">
                        <div class="checkout_input">
                            <label for="profile_phone_input">Телефон:</label>
                            <input type="text" placeholder="+7 999 888 7766" id="profile_phone_input">
                        </div>
                        <div class="checkout_input password_change">
                            <label for="profile_change_password">Пароль:</label>
                            <a href="#change_password_modal" class="editing_button" id="profile_change_password">Изменить</a>
                        </div>
                        <div class="checkout_input">
                            <label for="profile_date_input">Дата рождения:</label>
                            <input type="text" placeholder="01.01.2000" style="width: 100px" id="profile_date_input">
                        </div>
                    </div>
                    <div class="checkout_form_second_form">
                        <div class="checkout_input">
                            <label for="profile_first_name_input">Имя:</label>
                            <input type="text" placeholder="Анатолий" id="profile_first_name_input">
                        </div>

                        <div class="checkout_input">
                            <label for="profile_second_name_input">Фамилия:</label>
                            <input type="text" placeholder="Иванов" id="profile_second_name_input">
                        </div>

                        <div class="checkout_input">
                            <label for="profile_email_input">Email:</label>
                            <input type="text" placeholder="email@mail.ru" id="profile_email_input">
                        </div>
                    </div>
                </div>
                <div class="button_editing_profile">
                    <a href="">Сохранить</a>
                </div>
            </div>
        </div>
        <div class="basket_second_column">
            <div class="h3">Ваши заказы</div>
            <a href="#useful_product_modal" id="open_useful_product_modal">
                <div class="info_item">
                    <i class="far fa-heart heart_icon"></i>
                    <img src="imgs/for-basket1.png" alt="">
                    <p>Название товара</p>
                </div>
            </a>
            <a href="#useful_product_modal" id="open_useful_product_modal2">
                <div class="info_item">
                    <i class="far fa-heart heart_icon"></i>
                    <img src="imgs/for-basket2.png" alt="">
                    <p>Название товара</p>
                </div>
            </a>
        </div>
    </div>
@stop

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ $meta_title ?? "Белый Кот - Сайт официального дистрибьютора" }}</title>
    <meta content="{{ $meta_keywords ?? "Белый Кот - Сайт официального дистрибьютора" }}" name=keywords>
    <meta content="{{ $meta_description	?? "Белый Кот - Сайт официального дистрибьютора" }}" name=description>
    <link rel="stylesheet" href="{{ asset("css/fontawesome/css/all.css") }}">
    <script  href="{{ asset("/css/fontawesome/js/all.js") }}"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/media.css") }}">
</head>

<body>
<div id="app">
    <!--modal start-->
        <div id="conditions">
            <h3 class="modal-title">Условия акции</h3>
            <form id="f_conditions" name="conditions" action="#" method="post">
                <div class="modal-body">
                    Подпишитесь на наши новости, привязав свою электронную почту и получите промокод на 500 рублей
                    для покупок в нашем магазине в подарок!
                </div>
                <div class="btn-modal-ok">
                    <input id="do_conditions" type='submit' class="btn-modal empty-ok" value='Принять'>
                </div>
            </form>
        </div>
        <div id="news-cell">
            <h3 class="modal-title">Подписка</h3>
            <div class="modal-body">
                Вы успешно оформили подписку на наши новости, ваш промокод был отправлен на электронную почту!
            </div>
            <div class="btn-modal-ok">
                <input id="do_news-cell" type='submit' class="btn-modal empty-ok" value='Принять'>
            </div>
        </div>

        <div id="callback">
            <h3 class="modal-title">Заказать звонок</h3>
            <form id="f_callback" name="callback" action="#" method="post">
                <div class="modal-body">
                    <input class="call-back-form" placeholder="Номер телефона">
                    <textarea class="call-back-form" id="call-form-com" placeholder="Комментарий"></textarea>
                    <p id="com">* укажите в какое время вам удобно принять звонок</p>
                </div>
                <div class="btn-modal-ok">
                    <input id="do_callback" type='submit' class="btn-modal empty-ok" value='Заказать'>
                </div>
            </form>
        </div>
    <!--modal end-->

    @include('partials.header')

    @yield('content')

    @include('partials.footer')

</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(57002299, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57002299" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
<script type="text/javascript" src="{{ asset("js/jquery.min.js") }}"></script>
<script src='{{ asset("js/main.js") }}'></script>
<script type="text/javascript" src="{{ asset("js/app.js?v=1") }}"></script>
</html>

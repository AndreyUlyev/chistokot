<h3 class="modal-title">Регистрация</h3>
<form id="f_registration" name="registration" action="{{ route('register') }}" method="POST">
    @csrf
    <div class="modal-body">
        <input class="call-back-form form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Имя">
        @error('name')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
        <input class="call-back-form form-control" name="second_name" value="{{ old('second_name') }}" required autocomplete="second_name" autofocus placeholder="Фамилия">
        <input id="email" type="email" class="call-back-form form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
        @error('email')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
        <input id="password" type="password" class="call-back-form form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Пароль">
        @error('password')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
        <input id="password-confirm" type="password" class="call-back-form form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Еще раз пароль">
    </div>
    <div>
        <input id="do_registration" type='submit' class="btn-modal reg" value='Регистрация'>
        <a class="login-site" href="{{route('login')}}">Войти на сайт</a>
    </div>
</form>

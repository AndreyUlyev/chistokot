<h3 class="modal-title">Вход</h3>
<form id="f_login" name="login" action="{{ route('login') }}" method="POST">
    @csrf

    <div class="modal-body">
        <input id="email" type="email" class="call-back-form form-control @error('email') is-invalid @enderror"
               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
               placeholder="Email">
        @error('email')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror

        <input id="password" type="password"
               class="call-back-form form-control @error('password') is-invalid @enderror" name="password"
               required autocomplete="current-password" placeholder="Пароль">
        @error('password')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror

    </div>
    <div class="button_area">
        <input id="do_login" type='submit' class="btn-modal login" value='Войти'>
        <a class="forgotpass" href="{{ route('password.request') }}">Забыли пароль?</a>
    </div>
    <div class="registration">
        <a href="#registration" rel="nofollow" class="registration modalbox">Регистрация на сайте</a>
    </div>
</form>

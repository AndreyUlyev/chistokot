<section class="section-info">
    <div class="container numbers">
        В СВЯЗИ С МОДЕРНИЗАЦИЕЙ САЙТА ЗАКАЗЫ ПРИНИМАЮТСЯ ПО НОМЕРУ&nbsp;
        <a class="number1" href="tel:+74955178871">8 (495) 517-88-71</a>
        <a class="number2" href="tel:+74955064952">8 (495)506-49-52</a>
    </div>
</section>

<section class="content-section menu-bar">
    <div class="container header-flexbox header_text">
        <div class="col-md-5 headerLinks">
            <a class="aboutUs" href="{{ route('contact') }}">Контакты</a>
            <a class="delivery" href="{{ route('shipping') }}">Доставка и оплата</a>
            <a class="present" href="{{ route('news.index') }}">Новости</a>
        </div>
        <a class="col-md-2 header-logo" href="{{ route("home") }}">
            <img src="{{ asset("images/logo.png") }}" width="280" height="" class="logo">
        </a>
{{--        <div class="col-md-5 icons header_text">--}}
{{--            @if(Auth::check())--}}
{{--                <form class="icon" method="post" action="{{ route("logout") }}">--}}
{{--                    @csrf--}}

{{--                    @if (Auth::user()->hasRole('admin'))--}}
{{--                        <a href="{{ route("admin.main") }}" class="icon">--}}
{{--                            <img height="16px" src="{{ asset("images/admin.png") }}" alt="">--}}
{{--                        </a>--}}
{{--                    @endif--}}
{{--                    <button type="submit">--}}
{{--                        <img src="{{ asset("images/icon/logout.png") }}">--}}
{{--                    </button>--}}
{{--                </form>--}}
{{--            @else--}}
{{--                <a href="#login" rel="nofollow" class="icon modalbox">--}}
{{--                    <img class="" src="{{ asset("images/icon/login.png") }}">--}}
{{--                </a>--}}
{{--            @endif--}}
{{--            <a href="{{ route("likes.index") }}" class="icon">--}}
{{--                <img class="" src="{{ asset("images/icon/like.png") }}">--}}
{{--            </a>--}}
{{--            <a href="{{ route("baskets.index") }}" class="icon basket-icon">--}}
{{--                <img class="" src="{{ asset("images/icon/basket.png") }}">--}}
{{--                <div id="circle-price">--}}
{{--                    <p class="">{{ $countProductsInBasket ?? '' }}</p>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
        <user-panel-component :products_quantity="{{  $countProductsInBasket }}" :is_auth="{{ (int)Auth::check() }}"></user-panel-component>

    </div>
</section>

<section class="section-catalog">
    <div class="container header-flexbox-catalog header_text">
        <div class="dropdown catalog">
            <i class="fas fa-bars"></i>
            <span><a class="catalog_title" href="{{ route("categories.index") }}">Каталог</a></span>
            <div class="dropdown-child catalog-flexbox">
                @foreach($menu["categories"] as $category)
                    <a href="{{ route("categories.page", ["slug" => $category->slug]) }}"
                       data-typeobject="categories"
                       data-id="{{ $category->id }}"
                    >{{ $category->name }}</a>
                @endforeach
                <a href="#">новинки</a>
                <a href="#">популярное</a>
                <a href="#">акции</a>
                <a href="#">хиты продаж</a>
            </div>
        </div>

        @foreach($menu["popular_categories"] as $category)
            <a class="catalog-all" href="{{ route("categories.page", ["slug" => $category->slug]) }}">{{ $category->name }}</a>
        @endforeach

        <div class="colSearch">
            <form action="{{ route('search') }}" method="GET" role="search">
                @csrf
                <input type="search" name="search" placeholder="Что ищем?" class="search_form">
                <input type="submit" value="Поиск" class="search_btn">
            </form>
        </div>
    </div>
</section>

<section class="mobile-padding"></section>

<section class="mobile-menu">
    <div class="left-nav">
        <div class="menuBtn">
            <div class="mobileWrapper">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>

        </div>
        <div class="mobile-menu-content">

            <div class="menuWrapper">
                <div class="menuDesc">
                    <a href="{{ route('contact') }}">О нас</a>
                    <a href="{{ route('shipping') }}">Доставка и оплата</a>
                    <a href="{{ route('gifts') }}">Подарки</a></div>
                <h2>КАТАЛОГ</h2>
                <div class="catalogList">
                    @foreach($menu["categories"] as $category)
                        <a href="{{ route("categories.page", ["slug" => $category->slug]) }}"
                           data-typeobject="categories"
                           data-id="{{ $category["id"] }}"
                        >{{ $category["name"] }}</a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="searchBtn">
            <img id="m_search" src="{{ asset("images/icon/searchB.png") }}" alt="">
        </div>
        <div class="searchContainer">
            <input class="searchFormInput" type="text" placeholder="Что ищем?">
            <div class="searchSubmit">
                <img id="m_search_btn" src="{{ asset("images/icon/searchB.png") }}" alt="">
            </div>
        </div>
    </div>
    <div class="right-nav">
        <div class="header_text">
            @if(Auth::check())
                <form method="post" action="{{ route("logout") }}">
                    @csrf
                    <input type="submit" value="Выход" id="logout">
                </form>
            @else
                <a href="#login" class="icon">
                    <img id="m_login" class="" src="{{ asset("images/icon/login.png") }}">
                </a>
            @endif
            <a href="{{ route("likes.index") }}" class="icon">
                <img id="m_like" class="" src="{{ asset("images/icon/like.png") }}">
            </a>
            <a href="{{ route("baskets.index") }}" class="icon basket-icon">
                <img id="m_basket" class="" src="{{ asset("images/icon/basket.png") }}">
                <div id="circle-price">
                    <p class="">{{ $countProductsInBasket }}</p>
                </div>
            </a>
        </div>
    </div>
</section>
<!--Header end-->

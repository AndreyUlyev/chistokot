<section class="content-section">
    <div class="text-center category__block description-carousel">
        <p class="h2 category__title">Категории</p>
        <img class="category__wave" src="images/wave.png" alt="">
        <p class="category__description">Текст описания раздела на главной странице. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit. Donec faucibus maximus vehicula.</p>
    </div>
    <div class="owl-carousel owl-theme circle-carousel" id="owl-carousel-circle">
        @foreach($categories as $category)
            <a href="{{ route("categories.page", ["slug" => $category['slug'] ] ) }}" class="item item-circle cat">
                <div class="category__circle-block" >
                    <img class="category__circle" src="{{ $category['image'] }}" alt="">
                </div>
                <div><p>{{ $category['name'] }}</p></div>
            </a>
        @endforeach
    </div>
    <div class="text-center category__link-block follow-to-catalog">
        <a href="{{ route("all_categories") }}">
            <p>
                Перейти к каталогу <img src="{{ asset("images/a-main-page.png") }}" alt="" style="max-width: 5px;">
            </p>
        </a>
    </div>
</section>


<section class="footer content-section"> <!--category__block-->
    <div class="container footer-news category__block">
        <div id="footer-news1">
            <div>Оставайтесь всегда в<br>курсе новостей</div>
            <p>Подпишитесь на последние обновления и узнавайте о новинках и специальных предложениях первым</p>
        </div>
        <div id="footer-news2">
            <div class="footer-input-relative">
                <label class="feedback-label-footer">
                    <input type="text" class="news-form-footer" placeholder="Укажите адрес вашей электронной почты">
                </label>
                <a href="#news-cell" class="modalbox">
                    <input type="submit" class="news-form-btn-footer" value="Подписаться">
                </a>
            </div>
            <div class="checkbox">
                <input type="checkbox" id="checkbox">
                <label for="checkbox">
                    Нажимая на кнопку, я соглашаюсь с
                    <a href="#conditions" class="modalbox" id="stock">
                        условиями подписки
                    </a>
                </label>
            </div>
        </div>
    </div>
</section>

<section class="footer content-section">
    <div class="horizontalStrip-footer"></div>
</section>

<section class="footer content-section">
    <div class="container footer-main">
        <div class='col-md-3 footer_container_cell'>
            <p class='footer_head_text'>
                Белый кот
                <img src="{{ asset("images/arrow.svg") }}" alt="Иконка угла">
            </p>
            <p class='footer_text'>
                Белый кот приветствует Вас и благодарит за то, что Вы проявили инетерс к ашей компании.
                Компания Белый кот основана в 1998 году.
            </p>
        </div>
        <div class='col-md-3 footer_container_cell'>
            <p class='footer_head_text'>
                О нас
                <img src="{{ asset("images/arrow.svg") }}" alt="Иконка угла">
            </p>
            <p class='footer_text'>
                <a href="{{ route('news.index') }}">Новости</a><br>
                <a href="{{ route('shipping') }}">Доставка и оплата</a><br>
                <a href="{{ route('contact') }}">О нас</a><br>
            </p>
        </div>
        <div class='col-md-3 footer_container_cell'>
            <p class='footer_head_text'>
                Каталог
                <img src="{{ asset("images/arrow.svg") }}" alt="Иконка угла">
            </p>
            <p class='footer_text'>
                @foreach($menu["popular_categories"] as $category)
                    <a href="{{ route("categories.page", ["slug" => $category->slug]) }}" class='footer_text_a'>
                        {{ $category->name }}
                    </a>
                @endforeach
            </p>
        </div>
        <div class='col-md-3 footer_container_cell'>
            <p class='footer_head_text'>
                Контакты <img src="{{ asset("images/arrow.svg") }}" alt="Иконка угла"></p>
            <p class='footer_text'>Россия, город Москва, Звенигородское шоссе, д. 4, ТЦ Электроника на Пресне, желтая
                линия, стенд Б-12<br><br>
                <a href="tel:+74955178871">+7 (495) 517-88-71</a><br>
                <a href="tel:+74955064952">+7 (495) 506-49-52</a><br><br>mail@chistokot.ru
            </p>
        </div>
    </div>
</section>

<section class="footer content-section">
    <div class="horizontalStrip-footer"></div>
</section>

<section class="footer content-section">
    <div class="container copyright">
        <div class="col-md-3" id="copyright-col1">Copyright © Chistokot.ru {{ date("Y") }}</div>

        <div class="col-md-5 pay-var">
            <div id="copyright-col2" class="copyright-col2-container pay-var">
                <div class="pay-logo">
                    <img src="{{ asset("images/pay1.png") }}" alt="">
                </div>
                <div class="pay-logo">
                    <img src="{{ asset("images/pay2.png") }}" alt=""></div>

                <div class="pay-logo">
                    <img src="{{ asset("images/pay4.png") }}" alt="">
                </div>
                <div class="pay-logo">
                    <img src="{{ asset("images/jcb.png") }}" alt="">
                </div>

            </div>
        </div>
        <div class="col-md-4 cop">политика конфеденциальности</div>
    </div>
</section>


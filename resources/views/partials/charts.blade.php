<div class="card">
    <div class="card-body">
        <h5 class="card-title">Средняя сумма заказа:</h5>
        <div class="card-text">
            <h3 id="result"></h3>
        </div>
    </div>
</div>
<hr>
<div id="container1" style="width:100%; height:400px;"></div>
<hr>
<script src='{{ asset("js/admin/charts/charts.js") }}'></script>
<script src="/js/admin/charts/highcharts.js"></script>
<script src="/js/admin/charts/data.js"></script>

<div id="container2" style="width:100%; height:400px;"></div>

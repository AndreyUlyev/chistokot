<section>
    <div class="text-center category__block news-block">
        <p class="h2 category__title">Будь в курсе наших акций</p>
        <img class="category__wave" src="{{ asset("images/wavww.png") }}" alt="">
        <p class="category__description">Мы регулярно устраиваем акции, которые позволят Вам покупать наши товары еще выгоднее. Подпишись, чтобы не пропустить!</p>
        <div class="promoSub">
            <input type="text" class="news-form" placeholder="Email" />
            <a href="#news-cell" class="modalbox">
                <input type="submit" class="news-form-btn" value="Готово">
            </a>
        </div>
    </div>
</section>

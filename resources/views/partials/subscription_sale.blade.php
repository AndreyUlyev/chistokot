<div class="text-center category__block news-block">
    <p class="h2 category__title">Скидка 500 рублей</p>
    <img class="category__wave" src="{{ asset("images/wavww.png") }}" alt="">
    <p class="category__description">За подписку на наши новости.</p>
    <a href="#conditions" class="modalbox" id="stock">Условия акции.</a>
    <div class="promoSub">
        <label>
            <input name="email" type="text" class="news-form" placeholder="Email">
        </label>
        <input type="submit" class="news-form-btn" value="Готово">
    </div>
</div>

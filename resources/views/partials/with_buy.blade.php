<section class="content-section category__block">
    <div class="with-buy">
        С этим часто покупают
    </div>
    <div class="product-8">
        @foreach($products as $product)
            <div class="item item-good" id="product1">
                <a class="category__good-block-prod" href="{{ route('products.page', ["slug" => $product->slug]) }}">
                    <img class="category__good-prod"
                         src="{{ asset("/uploads/mini/".$product->productImages()->first()['link']) }}" alt="">
                </a>
                <a class="category__good-title"
                   href="{{ route('products.page', ["slug" => $product->slug]) }}">{{ $product->name }}</a>
                <p class="category__good-price">{{ (int)($product->price_discount ?? $product->price) }} РУБ.</p>
                <by-button-component :id="{{ $product->id }}"></by-button-component>
            </div>
        @endforeach
    </div>
</section>

@extends('layouts.app')

@section('content')
    <section>
        <div class="container justify-content-left">
            <div class="container">
                <h1 class="shop_title">Каталог</h1>

            </div>
            <div class="shop">
                <div class="shop_catalog">
                    <div class="shop_product">
                        @foreach($likes as $like)
                            <div class="shop_item">
                                <a href="{{ route_product($like->product) }}">
                                    <img src="{{ asset("/uploads/mini/" . $like->product->productImages[0]->link) }}" width="250px" alt="">
                                </a>
                                <p class="shop_item_title">
                                    <a href="{{ route_product($like->product) }}">
                                        {{ $like->product->name }}
                                    </a>
                                </p>
                                <p class="shop_item_price">{{ (int)$like->product->price }} руб.</p>
                                <by-button-component :id="{{ $like->product->id }}"></by-button-component>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            {{ $likes->render() }}
        </div>
    </section>
@endsection

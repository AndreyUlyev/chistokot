@extends('layouts.app')
@section('content')

    <section class="content-section">
        <div class="site-path">
            <a href="{{ route('home') }}">Главная</a> / <a href="{{ route('product') }}">Каталог</a>
        </div>

        <div class="prod-flexbox">
            <div class="text-center" id="prod-flexbox1">
                <a href="#img-prod">
                    <div id="prod">
                        <img class="prod" src="imgs/prod1.png">
                    </div>
                </a>
                <div class="owl-carousel carousel" id="carousel1">
                    <img class="prod" src="imgs/prod1min.png">
                    <img class="prod" src="imgs/prod1min.png">
                    <img class="prod" src="imgs/prod1min.png">
                    <img class="prod" src="imgs/prod1min.png">
                    <img class="prod" src="imgs/prod1min.png">
                </div>
            </div>
            <div id="prod-flexbox2">
                <div class="title_prod">
                    <div id="prod-name-sale">
                        <p class="h2 category__title">Заголовок товара</p>
                    </div>
                    <img class="category__wave" src="imgs/wave.png" alt="">
                </div>
                <div class="not-available">
                    <div id="red-circle"></div>
                    <p>Нет в наличии</p>
                </div>
                <p class="about black-color">Текст описания товара. Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit. Donec
                    faucibus maximus vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                <div class="news-not-available_product">
                    <p class="about black-color">Подпишитесь и узнайте о появлении товара</p>
                    <div class="news-not-available">
                        <input type="text" class="news-form-not-available" placeholder="Ваш Email">
                        <a href="#news-cell"><input type="submit" class="news-form-btn-not-available"
                                                    value="Готово"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-section">
        <div class="tabs category__block">
            <input id="tab1" type="radio" name="tabs" checked>
            <label class="title-text-tab" for="tab1">Описание</label>
            <input id="tab2" type="radio" name="tabs">
            <label class="title-text-tab" for="tab2">Отзывы</label>
            <input id="tab3" type="radio" name="tabs">
            <label class="title-text-tab" for="tab3">Характеристики</label>
            <div class="horizontalStrip"></div>
            <section id="content-tab1">
                <p class="review-desc">Коврик у двери – это не пережиток прошлого, а необходимость, особенно в том
                    случае, если
                    вас беспокоит
                    порядок и чистота в доме. Они достаточно прочны, выдерживают даже повышенные механические
                    воздействия,
                    удерживают влагу. Более того, современные материалы способны выдерживать большой диапазон перепада
                    температур, от -20 до +40 градусов, противостоять агрессивному воздействию таких жидкостей, как
                    бензин,
                    керосин и т.п.<br><br>Даже сейчас достаточно трудно представить, что над созданием такой привычной и
                    банальной
                    вещи, как коврик, будут трудиться ученые мужи, тщательно выверяющие состав и сплетение нити по
                    особой
                    уникальной схеме. Но только в таком случае можно утверждать, что коврик будет тщательно очищать,
                    удерживать влагу, создавая естественный барьер перед вашим жилищем. Примером такой инновационной
                    мысли
                    может служить коврик грязезащитный "Спагетти". Как следует из названия, данный коврик имеет особый
                    внешний вид. Ворс, в привычном понимании этого слова, отсутствует. Рабочая поверхность коврика
                    представляет собой сплав синтетических нитей. Верхняя часть коврика очищает подошву обуви, а
                    глубинно
                    расположенные слои нитей «намертво» удерживают пыль, грязь и влагу внутри себя. Такое расположение
                    нитей
                    уникально, и является изобретением компании производителя.<br><br>Способность коврика поглощать и
                    удерживать
                    грязь неимоверна, при этом даже при максимальной загрязненности он сохраняет опрятный внешний вид,
                    ведь
                    грязь остается не на поверхности, а внутри. Периодически следует только стряхивать или промывать
                    коврик
                    – вот и весь уход!<br><br>Подложка ковра изготовлена из высококачественного заменителя каучука - ПВХ
                    (поливинилхлорида). Этот материал прочен и долговечен, устойчив к механическим воздействиям. Основа
                    ковра исполнена в двух вариантах: гладкая и шипованная. Использовать ковры можно как на улице, так и
                    в
                    закрытых помещениях, т.к. они предназначены для использования в широком диапазоне температуры от-20
                    до
                    +40 градусов.<br><br><strong>Применение</strong><br><br>Предназначен для первичного снятия грязи на
                    пороге загородного дома и при входе
                    в квартиру. При этом сам ковер "спагетти" всегда сохраняет эстетичный и опрятный вид, сколько бы
                    пыли
                    и грязи он не впитал.<br><br><strong>Уход</strong><br><br>Чистка ковриков осуществляется путем
                    вытряхивания песка, грязи из структуры
                    ковра. Мыть ковер рекомендуется под струей воды в тазу, ванне, с использованием любых моющих средств
                    или
                    без них.<br><br><strong>Производитель:</strong> Турция. </p>
            </section>
            <section id="content-tab2">
                <div class="review">
                    <div class="review-title-name">
                        <p class="client-name">аня</p>
                        <p class="date-reviev">• 13.05.2019 •</p>
                        <p class="star">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                        </p>
                    </div>
                    <div class="review-title">суперски!</div>
                    <div class="review-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet turpis est. Nullam
                        tincidunt iaculis viverra. Nunc vel lectus neque. Mauris id auctor erat. Vestibulum erat massa,
                        hendrerit eget sapien vel, vestibulum sodales metus. Aenean at erat egestas tellus lobortis
                        sodales.
                        Phasellus finibus mauris non ipsum dapibus, vel vestibulum massa venenatis. Sed sed interdum
                        leo.
                    </div>
                    <a href="#" class="review-answer">Ответить <img src="imgs/ans-prod.png"></a>
                </div>
                <div class="review">
                    <div class="review-title-name">
                        <p class="client-name">тёма</p>
                        <p class="date-reviev">• 13.05.2019 •</p>
                        <p class="star">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                        </p>
                    </div>
                    <div class="review-title">лучшее, что я видел в своей жизни!!!</div>
                    <div class="review-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet turpis est. Nullam
                        tincidunt iaculis viverra. Nunc vel lectus neque. Mauris id auctor erat. Vestibulum erat massa,
                        hendrerit eget sapien vel, vestibulum sodales metus. Aenean at erat egestas tellus lobortis
                        sodales.
                        Phasellus finibus mauris non ipsum dapibus, vel vestibulum massa venenatis. Sed sed interdum
                        leo.
                    </div>
                    <a href="#" class="review-answer">Ответить <img src="imgs/ans-prod.png"></a>
                </div>
                <div class="review">
                    <div class="review-title-name">
                        <p class="client-name">тамара</p>
                        <p class="date-reviev">• 22.8.2019 •</p>
                        <p class="star">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                            <img src="imgs/star.png">
                        </p>
                    </div>
                    <div class="review-title">класс</div>
                    <div class="review-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet turpis est. Nullam
                        tincidunt iaculis viverra. Nunc vel lectus neque. Mauris id auctor erat. Vestibulum erat massa,
                        hendrerit eget sapien vel, vestibulum sodales metus. Aenean at erat egestas tellus lobortis
                        sodales.
                        Phasellus finibus mauris non ipsum dapibus, vel vestibulum massa venenatis. Sed sed interdum
                        leo.
                    </div>
                    <a href="#" class="review-answer">Ответить <img src="imgs/ans-prod.png"></a>
                </div>

                <div class="page_navigation-review">
                    <p class="page_number_go">Первая</p>
                    <p class="page_number ">1</p>
                    <p class="page_number page_number_active">2</p>
                    <p class="page_number">3</p>
                    <p class="page_number">4</p>
                    <p class="page_number ">...</p>
                    <p class="page_number_go">Вперед</p>
                    <p class="page_number_go ">Последняя</p>
                </div>
                <div class="category__block">
                    <div class="my-review">
                        <h2>Оставьте свой отзыв</h2>
                        <div class="my-review-row">
                            <div class="rating my-star">
                                <input type="radio" id="star5" name="rating" value="5"/><label for="star5"
                                                                                               title="5"></label>
                                <input type="radio" id="star4" name="rating" value="4"/><label for="star4"
                                                                                               title="4"></label>
                                <input type="radio" id="star3" name="rating" value="3"/><label for="star3"
                                                                                               title="3"></label>
                                <input type="radio" id="star2" name="rating" value="2"/><label for="star2"
                                                                                               title="2"></label>
                                <input type="radio" id="star1" name="rating" value="1"/><label for="star1"
                                                                                               title="1"></label>
                            </div>
                            <input type="text" placeholder="Заголовок">
                            <input type="text" placeholder="Как вас зовут?">
                        </div>

                        <textarea placeholder="Напишите пару строк о товаре"></textarea>
                        <div id="sent-review">
                            <input type="submit" value="Отправить">
                        </div>
                    </div>
                </div>

            </section>
            <section id="content-tab3">
                <p class="review-desc">Коврик у двери – это не пережиток прошлого, а необходимость, особенно в том
                    случае, если
                    вас беспокоит
                    порядок и чистота в доме. Они достаточно прочны, выдерживают даже повышенные механические
                    воздействия,
                    удерживают влагу. Более того, современные материалы способны выдерживать большой диапазон перепада
                    температур, от -20 до +40 градусов, противостоять агрессивному воздействию таких жидкостей, как
                    бензин,
                    керосин и т.п.<br>Даже сейчас достаточно трудно представить, что над созданием такой привычной и
                    банальной
                    вещи, как коврик, будут трудиться ученые мужи, тщательно выверяющие состав и сплетение нити по
                    особой
                    уникальной схеме. Но только в таком случае можно утверждать, что коврик будет тщательно очищать,
                    удерживать влагу, создавая естественный барьер перед вашим жилищем. Примером такой инновационной
                    мысли
                    может служить коврик грязезащитный "Спагетти". Как следует из названия, данный коврик имеет особый
                    внешний вид. Ворс, в привычном понимании этого слова, отсутствует. Рабочая поверхность коврика
                    представляет собой сплав синтетических нитей. Верхняя часть коврика очищает подошву обуви, а
                    глубинно
                    расположенные слои нитей «намертво» удерживают пыль, грязь и влагу внутри себя. Такое расположение
                    нитей
                    уникально, и является изобретением компании производителя.<br>Способность коврика поглощать и
                    удерживать
                    грязь неимоверна, при этом даже при максимальной загрязненности он сохраняет опрятный внешний вид,
                    ведь
                    грязь остается не на поверхности, а внутри. Периодически следует только стряхивать или промывать
                    коврик
                    – вот и весь уход!<br>Подложка ковра изготовлена из высококачественного заменителя каучука - ПВХ
                    (поливинилхлорида). Этот материал прочен и долговечен, устойчив к механическим воздействиям. Основа
                    ковра исполнена в двух вариантах: гладкая и шипованная. Использовать ковры можно как на улице, так и
                    в
                    закрытых помещениях, т.к. они предназначены для использования в широком диапазоне температуры от-20
                    до
                    +40 градусов.<br>Применение<br>Предназначен для первичного снятия грязи на пороге загородного дома и
                    при
                    входе
                    в квартиру. При этом сам ковер "спагетти" всегда сохраняет эстетичный и опрятный вид, сколько бы
                    пыли
                    и грязи он не впитал.<br>Уход<br>Чистка ковриков осуществляется путем вытряхивания песка, грязи из
                    структуры
                    ковра. Мыть ковер рекомендуется под струей воды в тазу, ванне, с использованием любых моющих средств
                    или
                    без них.<br>Производитель: Турция. </p>
            </section>
        </div>
    </section>

    @include('partials.categories')
    @include('partials.subscription_sale')
    @include('partials.short_about')
@stop

@extends('layouts.app')

@section('content')
    <section class="content-section">
        {{  Breadcrumbs:: render("product", $category, $product)}}

        <div class="container prod-flexbox">
            @if($product->discount != null)
                <div id="square">
                    <p class="black-color"><b>{{ $product->discount }}%</b> скидка</p>
                </div>
            @endif

            <div class="text-center " id="prod-flexbox1">
                <product-slider-component
                    :images="{{ json_encode($product->productImages) }}"></product-slider-component>
            </div>
            <div id="prod-flexbox2">
                <div class="title_prod">
                    <div id="prod-name-sale">
                        <p class="h2 category__title">
                            {{ $product->name }}
                        </p>
                    </div>
                    @if (count($products_group))
                        <img class="category__wave" src="{{ asset("images/wave.png")}}" alt="">
                    @endif

                    @if ($product->size || $product->color)
                        <div>
                            @if ($product->size)
                                <div>Модификация - {{ $product->size }}</div>
                            @endif

                            @if ($product->color)
                                <div>Цвет - {{ $product->color }}</div>
                            @endif
                        </div>
                    @endif

                    <div class="mods_block">
                        @foreach ($products_group as $product_gr)
                            <a href="{{ route_product($product_gr) }}" class="mod_block">
                                <img src="{{ asset("/uploads/mini/".$product_gr->productImages[0]->link ) }}">
                                @if ($product_gr->size)
                                    <div>{{ $product_gr->size }}</div>
                                @endif

                                @if ($product_gr->color)
                                    <div>{{ $product_gr->color }}</div>
                                @endif
                            </a>
                        @endforeach
                    </div>

                    <div>
                        <img class="category__wave" src="{{ asset("images/wave.png")}}" alt="">
                    </div>
                </div>

                @if(Auth::check() && (Auth::user()->hasRole('admin') ?? false))
                    <p>
                        <a target="_blank"
                           href="{{ route("admin.products.edit", ["id_product" => $product->id]) }}"><input
                                type="button" class="inBasket" value="Редактировать"></a>
                    </p>
                @endif
                <div id="price-prod">
                    @if ($product->price_discount != null)
                        <div class="old-price-prod">{{ (int)$product->price }} РУБ.</div>
                        <div class="new-price-prod">{{ (int)$product->price_discount }} РУБ.</div>
                    @else
                        <div class="new-price-prod">{{ (int)$product->price }} РУБ.</div>
                    @endif
                    <div id="square-mobile">
                        <p class="black-color"><b>{{ $product->discount }}%</b> скидка</p>
                    </div>
                </div>

                <p class="about black-color">
                    {{ $product->short_description }}
                </p>


                <div class="btns">
                    <by-one-click-component
                        :product="{{ json_encode($product) }}"
                        :delivery_types="{{ json_encode($delivery_types) }}"
                        :payment_types="{{ json_encode($payment_types) }}"
                    ></by-one-click-component>
                    <by-button-component :id="{{ $product->id }}" type="product_page"></by-button-component>
                    <like-button-component :id="{{ $product->id }}"
                                           :active_like="{{ (int)$active_like }}"></like-button-component>
                </div>
                <div class="about-prod">
                    @if($product->composition)
                        <p class="title-text">Состав</p>
                        <div class="horizontalStrip"></div>
                        <p class="about">
                            {{ $product->composition }}
                        </p>
                    @endif

                    @if($product->about_delivery)
                        <p class="title-text">О доставке</p>
                        <div class="horizontalStrip"></div>
                        <p class="about">
                            {{ $product->about_delivery }}
                        </p>
                    @endif

                    @if($product->guarantees)
                        <p class="title-text">Гарантии</p>
                        <div class="horizontalStrip"></div>
                        <p class="about">
                            {{ $product->guarantees }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="content-section">
        <div class="container tabs category__block">
            <input id="tab1" type="radio" name="tabs" checked>
            <label class="title-text-tab" for="tab1">Описание</label>
            <input id="tab2" type="radio" name="tabs">
            <label class="title-text-tab" for="tab2">Отзывы</label>
            <div class="horizontalStrip"></div>
            <section id="content-tab1">
                <p class="review-desc">
                    {{ $product->description }}
                    @if ($product->where_apply)
                        <br><br><strong>Применение</strong><br><br>
                        {{ $product->where_apply }}
                    @endif

                    @if ($product->maintenance)
                        <br><br><strong>Уход</strong><br><br>
                        {{ $product->maintenance }}
                    @endif
                    {{--                    <br><br><strong>Производитель:</strong>--}}
                    {{--                    {{ $product->country_name  }}--}}
                </p>
            </section>
            <section id="content-tab2">
                <comment-list-component :product_id="{{ $product->id }}"></comment-list-component>
            </section>
        </div>
    </section>

    <section class="container">
        @include('partials.with_buy')
    </section>
    <section>
        @include('partials.be_aware_of_sales')
    </section>
    <section class="container">
        @include('partials.short_about')
    </section>

@endsection

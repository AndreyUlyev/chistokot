@extends('layouts.app')

@section('content')
    <section class="content-section category__block">
        <div class="container justify-content-left">
            <div class="with-buy">
                Результат поиска
            </div>

            {{  Breadcrumbs:: render("pages.shipping")}}

            @if($error ?? false)
                <div class="review-desc alert alert-success"> {{ $error }}</div>
            @else
                <div class="review-desc card-header"><b>{{ $products->count() }} результатов для "{{ request('search') }}
                        "</b></div>
                <div class="product-8 category__block">
                    @foreach($products as $product)
                        <div class="item item-good" id="product1">
                            <a class="category__good-block-prod"
                               href="{{ route_product($product) }}">
                                <img class="category__circle"
                                     src="{{ asset("/uploads/mini/" . ($product->image ?? $product->productImages()->first()->link)) }}" alt="">
                            </a>
                            <a class="category__good-title"
                               href="{{ route_product($product) }}">
                                <p>
                                    {{ $product->name }}<br/>
                                    {{ $product->size }}
                                    {{ $product->color }}
                                </p>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </section>
@endsection

@extends('layouts.app')
@section('content')

    {{  Breadcrumbs:: render("categories.list")}}

    <div class="container">
        <div class="col-md-3">
            <div class="type_filter">
                <div class="filter_title first_filter">
                    <h3>Категории <span class="angle_icon angle_icon_rotate" id="icon_rotate"> <img
                                src="{{ asset("images/icon/angle.png") }}" alt=""> </span>
                    </h3>
                </div>
                <ul class="filter_options">
                    @foreach($all_categories as $category)
                        <li>
                            <div class="filter_options_item ">
                                <hr class="filter_options_item_line">
                                <a href="{{ route("categories.page", ["slug" => $category->slug]) }}"
                                   class="filter_options_item_title">{{ $category->name }}</a>
                                <p class="filter_options_item_number">{{ $category->products()->count() }}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-9">
{{--            <div class="banner_categories">--}}
{{--                <img src="{{ asset("images/banner_categories.png") }}" alt="баннер категорий">--}}
{{--                <div class="banner_categories_text">--}}
{{--                    <h1>Скидка 20%</h1>--}}
{{--                    <p>Текст описания первого слайда на главной странице. Lorem ipsum dolor sit amet, consectetur--}}
{{--                        adipiscing--}}
{{--                        elit. Donec faucibus maximus vehicula.</p>--}}
{{--                    <a href="">Перейти</a>--}}
{{--                </div>--}}
{{--            </div>--}}

            <h3 class="all_categories_title">Все категории</h3>
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-4 categories_item">
                        <a href=" {{ route("categories.page", ["slug" => $category->slug]) }} ">
                            <div class="img-block">
                                <img src="{{ asset("/uploads/mini/".$category->image) }}" alt="Картинка категории">
                            </div>
                            <p class="shop_item_title">{{ $category->name }}</p>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="dataTables_paginate" id="demo-dynamic-tables-2_paginate">
                        {{ $categories->links('pagination.custom1') }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section>
        @include('partials.subscription_sale')
    </section>

    @include('partials.short_about')
@stop


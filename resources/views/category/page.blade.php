@extends('layouts.app')
@section('content')
    <div class="container">
        <h1 class="shop_title">Каталог</h1>
    </div>
    {{  Breadcrumbs:: render("categories.page", $category)}}
    <categories-component slug="{{ $category->slug }}"></categories-component>
@stop
